var config = {
	title : /*[title]*/"小i机器人模板"/*[/title]*/,
	logo_link : /*[logo_link]*/"http://www.xiaoi.com/"/*[/logo_link]*/,
	intro_text : /*[intro_text]*/"小i机器人技术提供"/*[/intro_text]*/,
	intro_link : /*[intro_link]*/"http://www.xiaoi.com/"/*[/intro_link]*/,
	write : function(key) {
		document.write(this[key])
	},
	link : function(hrefKey,textKey, styleCls) {
		document.write('<a href="'+this[hrefKey]+'" target="_blank" class="'+styleCls+'">'+this[textKey]+'</a>')
	}
}