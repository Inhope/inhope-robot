<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="pragma" content="no-cache"/>         
	<meta http-equiv="Cache-Control" content="no-cache,   must-revalidate"/>         
	<meta http-equiv="expires" content="0"/> 
<style>
body {
    margin:0;
    overflow-x:hidden;
	overflow-y:hidden;
    font-size:12px;
    color:#333;
    line-height:18px;
	font-family:'宋体',Verdana, Arial;
	background-color:transparent;
	margin-top: 20px;
}
a {
  text-decoration:none;
}
a:hover {
  text-decoration:underline;
}
a:VISITED{
  text-decoration:none;color:blue;
}

</style>
<script type="text/javascript" src="../js/jquery.js"></script> 
<script type="text/javascript" src="js/login.js"></script>
<script type="text/javascript">var status = ${status};var user = '${username}';</script>
</head>
<body>
<div id ="loginDiv"  style="absolute;z-index: 2;background-color:#fff;">
<form method="post" id="loginForm" action="login.action">
<input type="hidden" name="${ruName}" value="${ruValue}"/>
<h3
	style="color: #333; font-size: 15px; border: 0; margin: 0; padding: 0; line-height: 36px;text-align: center;">
	用户登录
</h3>
<table border="0" align="center" cellpadding="0" cellspacing="2"  >
  <tr valign="top">
	<td>手机号码：</td>
	<td align="left">
	<input tabindex="1" name="username" type="text" style="width:130px; border:1px solid #8fb7da"  value="" ><br/><span style="color: #bbb">请输入11位手机号码</span>
	</td>
	<td width="81" rowspan="2"><a id="submitButton" onclick="return false;" href="#"><img src="images/btn-enter_d.gif" border="0"/></a></td>
  </tr>
  <tr valign="top">
	<td>服务密码：</td>
	<td align="left"><input tabindex="2" name="password" type="password" style="width:130px; border:1px solid #8fb7da"  value=""><br/><span style="color: #bbb">请输入10086服务密码</span></td>
  </tr>
  <tr valign="top">
	<td>验证码：</td>
	<td align="left" colspan="2"><input tabindex="3" name="captcha" type="text" style="width:75px; border:1px solid #8fb7da"  value=""><img id="captchaImg" src="captcha.action?ts=<%=new java.util.Random().nextInt()%>" style="cursor:pointer;vertical-align:bottom"/><br/><span style="color: #bbb">请输入验证码</span></td>
  </tr>
</table>
</form>
</div>
</body>

</html>
