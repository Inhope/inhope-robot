package com.eastrobot.robotdev.servlet;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eastrobot.robotdev.function.utils.StringUtil;
import com.eastrobot.robotdev.util.XiaoiTagUtils;
import com.eastrobot.robotface.RobotServiceEx;
import com.eastrobot.robotface.RobotServiceExProxy;
import com.eastrobot.robotface.domain.RobotRequestEx;
import com.eastrobot.robotface.domain.RobotResponseEx;
import com.eastrobot.robotface.domain.UserAttribute;

/**
 * 提供给客户的http接口,请求用标准http协议接收参数，响应为RobotResponseEx的json格式
 * 
 * @author yale
 * 
 */
public class HttpAskService extends BaseServlet {

	private static final long serialVersionUID = -6433901548830594473L;
	private static Logger log = LoggerFactory.getLogger(HttpAskService.class);

	private RobotServiceEx robotServiceEx = new RobotServiceExProxy();

	@Override
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		RobotResponseEx robotResponseEx = null;
		try {
			// 设置请求参数
			RobotRequestEx robotRequestEx = new RobotRequestEx();

			// 必填参数
			String userId = request.getParameter("userId");
			String question = request.getParameter("question");
			String platform = request.getParameter("platform");

			log.debug("userId:" + userId);
			log.debug("question:" + question);
			log.debug("platform:" + platform);
			

			if (StringUtils.isBlank(userId) || StringUtils.isBlank(question)
					|| StringUtils.isBlank(platform)) {
				throw new Exception("缺少必填参数");
			}

//			String location = request.getParameter("location");
			String location = request.getParameter("scopeId");
			
			String brand = request.getParameter("brand");

			robotRequestEx.setUserId(userId);
			robotRequestEx.setQuestion(question);

			// 设置UserAttribute数组
			List<UserAttribute> attrs = new ArrayList<UserAttribute>();
			attrs.add(new UserAttribute(UserAttribute.PLATFORM, platform));
			if (StringUtils.isNotBlank(location))
				attrs.add(new UserAttribute(UserAttribute.LOCATION, location));
			if (StringUtils.isNotBlank(brand))
				attrs.add(new UserAttribute(UserAttribute.BRAND, brand));

			robotRequestEx.setAttributes(attrs.toArray(new UserAttribute[] {}));
			log.debug(JSONArray.fromObject(robotRequestEx.getAttributes()).toString());
			robotResponseEx = robotServiceEx.deliver(robotRequestEx);

			int type = robotResponseEx.getType();
			String content = robotResponseEx.getContent();
//			String faqVote = content.substring(content.indexOf("[feed]")+6,content.indexOf("[/feed]"));
//			String content1 = content.substring(0,content.indexOf("[feed]"));
//			System.out.println("content："+content);
//			System.out.println("faqVote："+faqVote);
			log.debug("type:" + type);
			log.debug("content:" + content);

		} catch (Exception e) {
			// 如果有异常,type为-1
			log.error("", e);
			robotResponseEx = new RobotResponseEx();
			robotResponseEx.setType(-1);
			robotResponseEx.setContent(e.getMessage());
		} finally {
			// 响应json数据
			String content = robotResponseEx.getContent();
			if(StringUtils.isNotBlank(content)) {
				content = content.replaceAll("\"", "'");
				content = XiaoiTagUtils.tagFilter(content);
				robotResponseEx.setContent(content);
				log.debug("realContent:" + content);
			}
			JSONObject resJson = JSONObject.fromObject(robotResponseEx);
//			System.out.println(content);
//			String faqVote = content.substring(content.indexOf("[feed]")+6,content.indexOf("[/feed]"));
//			String content1 = content.substring(0,content.indexOf("[feed]"));
//			String content2 = content.substring(content.indexOf("[/feed]"),content.indexOf("[feed]"));
//			System.out.println("content2:"+content2);
//			resJson.remove("content");
//			resJson.put("content", content1);
//			resJson.put("faqVote", faqVote);
			if (content != null && content.contains("[feed]")) {
				List<String> faqList = StringUtil.getTag(content, "feed");
				if (faqList.size() == 2) {
					System.out.println(faqList.get(0));
					System.out.println(faqList.get(1));
					System.out.println( URLEncoder.encode(faqList.get(0),"UTF-8"));
				 URLEncoder.encode(faqList.get(1),"UTF-8");
				}
				content = StringUtil.removeTag(content, "feed");
//				System.out.println(content);
				String[] relatedQuestions = robotResponseEx.getRelatedQuestions();
//				System.out.println(robotResponseEx.getRelatedQuestions());
				String solve ="faqvote:"+URLEncoder.encode(faqList.get(0),"UTF-8");
				String unsolved ="faqvote:"+URLEncoder.encode(faqList.get(1),"UTF-8");
				resJson.remove("content");
				resJson.put("content", content);
				resJson.put("normQuestion", relatedQuestions[0]);
				resJson.put("solve", solve);
				resJson.put("unsolved",unsolved);
				}
			
			String[] questions = robotResponseEx.getRelatedQuestions(); // 标准问相关问题
			int type = robotResponseEx.getType();
			if (questions != null) {
				JSONArray qstArr = new JSONArray();
				for (String qst : questions) {
					qstArr.add(qst);
				}
				if (type == 1)
					qstArr.remove(0);
				resJson.remove("relatedQuestions");	
				resJson.put("relatedQuestions", qstArr);
				
				
				
			}

			write(response, String.valueOf(resJson));
		}
	}

}
