function adjustLayout(topdivId, targetdivId, lr) {
	var h = 0, w = 0;
	var targetdiv;
	var topdiv = document.getElementById(topdivId)
	topdiv.topdivId = topdivId
	topdiv.targetdivId = targetdivId
	topdiv.lr = lr
	topdiv.onresize = function() {
		adjustLayout(this.topdivId, this.targetdivId, this.lr)
	}
	for (var i = topdiv.childNodes.length - 1; i >= 0; i--) {
		var node = topdiv.childNodes.item(i);
		if (node.nodeName && node.nodeName.toLowerCase() == "div") {
			if (node.id == targetdivId) {
				targetdiv = node
			} else {
				if (lr == 1 || lr == 2) {
					w = w + node.offsetWidth;
					if (!targetdiv)
						node.style.left = (topdiv.offsetWidth - w) + "px";
				}
				if (lr == 0 || lr == 2) {
					h = h + node.offsetHeight;
					if (!targetdiv)
						node.style.top = (topdiv.offsetHeight - h) + "px";
				}
			}
		}
	}
	if (lr == 1 || lr == 2)
		targetdiv.style.width = (topdiv.offsetWidth - w) + "px";

	if (lr == 0 || lr == 2) {
		targetdiv.style.height = (topdiv.offsetHeight - h) + "px";
		if ("activityPanelDiv" == targetdivId) {
			var o = targetdiv.getElementsByTagName('div');
			if (o.length) {
				for (var j = 0; i < o.length; j++) {
					o[j].style.height = targetdiv.style.height
				}
			}
		}
	}
}

if (navigator.appVersion.indexOf("MSIE 6") > 0) {
	window.attachEvent("onload", function() {
				adjustLayout("mainDiv", "panelDiv", 0);
				adjustLayout("panelDiv", "leftPanelDiv", 1);
				adjustLayout("leftPanelDiv", "outputDiv", 0);
				adjustLayout("inputDiv", "inputAreaDiv", 1);
				adjustLayout("rightPanelDiv", "activityDiv", 0);
				adjustLayout("activityDiv", "activityPanelDiv", 0);
				adjustLayout("outputDiv", "outputArea", 2);
			});
}
