<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@page import="com.eastrobot.app.web.VerifyActionSupport"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	VerifyActionSupport.writeNonce("wap", request, response);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta name="robots" content="index,nofollow" />
<link rel="stylesheet" type="text/css" href="css/iphone_wap.css" />
<title>小i智能机器人WAP展示</title>
</head>
<body>
	<div class="wap_wrap">

		<div class="wap_content left">
			<div class="wap_banner">
				<img src="images/banner1.jpg" width="222" height="40" title="小i机器人"
					alt="小i机器人">
			</div>

			<c:if
				test="${not empty sessionScope.verifyCaptchaState and sessionScope.verifyCaptchaState gt 0}">
				<div class="wap_huanying">您好，我是小i机器人，请直接输入您想咨询的问题，谢谢！</div>
				<div class="wap_search">
					<div class="wap_searchcn1">
						<form id="askForm" action="wap-demo.action" method="post">
							<input type="hidden" name="fromWeb" value="true" />
							<p class="wap_searchp1 left">
								<input id="askInput" name="requestContent" type="text"
									autofocus="autofocus" />
							</p>
							<p class="wap_searchp2 left">
								<input type="submit" value="" />
							</p>
						</form>
					</div>
			</c:if>
			<c:if
				test="${empty sessionScope.verifyCaptchaState or sessionScope.verifyCaptchaState eq 0}">
				<div class="wap_huanying">
					您好，我是小i机器人，请输入验证码： <img
						src="<c:url value='/p4pages/captcha.action'/>" alt="验证码"
						style="cursor: pointer;"
						onclick="this.src='<c:url value='/p4pages/captcha.action'/>?r='+new Date().getTime()" />
				</div>
				<div class="wap_search">
					<div class="wap_searchcn1">
						<form name="verifCodeForm"
							action="<c:url value='wap-demo.action'/>" method="post">
							<input type="hidden" name="fromWeb" value="true" />
							<p class="wap_searchp1 left">
								<input name="captcha" id="ecode" maxlength="6"
									onKeyUp="verifCodeCheck(event);" />
							</p>
							<p class="wap_searchp2 left">
								<input type="submit" value="" />
							</p>
						</form>
					</div>
					<div style="color:#d23802;margin-top:5px;margin-left: 5px"
						id="errorMsg">
						<c:if test="${sessionScope.verifyCaptchaState eq 0}">
							验证码输入错误
						</c:if>
					</div>
			</c:if>
			<ul class="wap_searchul left" style="margin-top:5px;">
				<p class="wap_searchulp1">快速提问：</p>
				<c:if
					test="${not empty sessionScope.verifyCaptchaState and sessionScope.verifyCaptchaState gt 0}">
					<li><a href="#"
						onclick="document.getElementById('askInput').value=this.innerHTML;document.getElementById('askForm').submit();">什么是智能网络机器人？</a>
					</li>
					<li><a href="#"
						onclick="document.getElementById('askInput').value=this.innerHTML;document.getElementById('askForm').submit();">小i机器人可以做什么？</a>
					</li>
				</c:if>
				<c:if
					test="${empty sessionScope.verifyCaptchaState or sessionScope.verifyCaptchaState==0}">
					<li>什么是智能网络机器人？</li>
					<li>小i机器人可以做什么？</li>
				</c:if>
			</ul>


			<div class="wap_content2 left">
				<p class="wap_cn1">
					<c:if test="${not empty requestContent}">
						<b>我:</b>${requestContent }</c:if>
				</p>

				<p class="wap_cn2">
					<c:if test="${not empty responseContent}">
						<span>小i机器人:</span> ${responseContent }</c:if>
				</p>
			</div>
		</div>
	</div>
	</div>
	<script src="../../js/verify.js"></script>
</body>
</html>


