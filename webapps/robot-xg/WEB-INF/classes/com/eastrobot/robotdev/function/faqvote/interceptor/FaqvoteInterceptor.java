package com.eastrobot.robotdev.function.faqvote.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.eastrobot.robotdev.function.FunctionConstants;
import com.eastrobot.robotdev.function.faqvote.processor.FaqvoteProcessor;
import com.eastrobot.robotdev.function.utils.MsgUtil;
import com.eastrobot.robotnerve.Channel;
import com.eastrobot.robotnerve.Event;
import com.eastrobot.robotnerve.Interceptor;
import com.eastrobot.robotnerve.InterceptorChain;
import com.eastrobot.robotnerve.annotations.handler.Priority;
import com.eastrobot.robotnerve.app.HandlerHelper;

@Component
@Priority(Priority.NORMAL_PRIORITY)
public class FaqvoteInterceptor implements Interceptor {

	private static Logger log = LoggerFactory
			.getLogger(FaqvoteInterceptor.class);
	private static final HandlerHelper helper = HandlerHelper.getInstance();
	// 获取FaqvoteProcessor实例
	private FaqvoteProcessor fp = FaqvoteProcessor.getInstance();

	@Override
	public void intercept(Channel channel, Event event, InterceptorChain chain) {

		String platform = helper.client.platform();
		log.debug("platform: " + platform);
		// 对应渠道是否有打开此功能开关
		String faqVoteSwitch = MsgUtil.get(FunctionConstants.FaqVote.SWITCH, platform);
		boolean doProcessor = "true".equalsIgnoreCase(faqVoteSwitch);
		log.debug("faqVoteSwitch: " + faqVoteSwitch + " || doProcessor: " + doProcessor);

		// 如果开关是关闭，只运行doIntercept
		if (!doProcessor) {
			chain.doIntercept(channel, event);
		} else {
			boolean isContinue = Boolean.TRUE;
			
			// 如果返回值为true则需要调用chain.doIntercept(channel, event);
			isContinue = fp.processor(channel, event);
			
			if (isContinue) {
				chain.doIntercept(channel, event);
				// 后置处理
				fp.afterProcessor(channel, event);
			}
		}

	}
}
