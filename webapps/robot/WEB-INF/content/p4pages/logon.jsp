<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="pragma" content="no-cache"/>         
	<meta http-equiv="Cache-Control" content="no-cache,   must-revalidate"/>         
	<meta http-equiv="expires" content="0"/> 
</head>
<body>
<script type="text/javascript">
var user = ${userJson};
var ru = '${ru}';

var channelLogin = window.parent.Channel_Login || window.top.Channel_Login;
if(channelLogin) {
	channelLogin(user)
	if(ru && ru.length>0 && ru.indexOf('logon.action') == -1){
		if (ru.indexOf('?') == -1)
			ru += '?';
		ru += '&ts=' + new Date().getTime();
		window.location.href = ru;
	}
}
else {
	alert('服务器不支持登录！');
}
</script>
</body>
</html>
