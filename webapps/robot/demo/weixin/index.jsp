<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf8">
<meta name="keywords" content="微信网页版,weixin web,wechat,微信 pc,微信电脑版,wechat web,微信 web,微信电脑">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=yes">
<link rel="shortcut icon" href="images/icon.ico" type="image/x-icon">
<title>微信网页版</title>
<link rel="stylesheet" type="text/css" href="css/weixin.css">
<script src="../../js/jquery.js"></script>

<script src="js/weixin.js"></script>
<script src="js/emojimap.js"></script>
</head>
<body ctrl="1">
	<div id="container" style="">
		<div id="chat" class="chatPanel normalPanel" ctrl="1">
			<div class="content">
				<div class="chat lightBorder" style="visibility: visible;">
					<div class="chatContainer" style="height: 621px;">
						<div class="chatMainPanel" id="chatMainPanel">
							<div class="chatTitle">
								<div class="chatNameWrap">
									<p class="chatName" id="messagePanelTitle">小i机器人</p>
								</div>
							</div>
							
							<div class="chatScorll" id="chatScorll" style="height: 522px; overflow-y: auto; position: relative;">
								<div id="chat_chatmsglist" class="chatContent" ctrl="1" style="top: 0px;">
								
								</div>
							</div>

							<div id="chat_editor" class="chatOperator lightBorder" ctrl="1">
								<div class="inputArea">
									<div class="attach">
										<a href="javascript:showEmojiPanel();" id="sendEmojiIcon" class="func expression" click="showEmojiPanel" title="选择表情"></a>
										<!-- 
										<a href="javascript:;" style="visibility:hidden" id="screenSnapIcon" class="func screensnap" click="screenSnap" title="发送截屏" style=""></a>
										
										<form class="left" id="sendFileIcon"
											url="http://file2.wx.qq.com/cgi-bin/mmwebwx-bin/webwxuploadmedia?cgi=sendfile&amp;t=chat&amp;callbackfun=sendFile"
											enctype="multipart/form-data" method="post" target="actionFrame" style="">
											<input type="hidden" name="uploadmediarequest" value="{BaseRequest:{}}"> 
												<a href="javascript:;" class="func file" style="position:relative;display:block;margin:0;" title="文件图片" id="uploadFileContainer">
													<div style="position: absolute;top:0;left:0; width: 100%; height: 100%;overflow:hidden;filter:alpha(opacity=0);opacity:0;cursor:pointer;">
														<div>
															<input change="sendAppMsg@form" type="file" name="filename"
																style="width:100%;height:100%;margin:0;cursor:pointer;font-size:100px;">
														</div>
													</div>
													<div id="swfUploaderContainer" style="z-index: 3; position: absolute; height: 34px; width: 34px; left: 0px; top: 0px;" title="文件图片">
														<span id="swfUploaderWrapper"
															style="top:0;left:0;position:absolute;width:100%;height:34px;margin:0;z-index:2;"><embed
																wmode="transparent" allowscriptaccess="always"
																quality="high" width="100%" height="100%"
																src="https://res.wx.qq.com/zh_CN/htmledition/swf/uploader.swf?_=2.0?r=1412904405349"
																type="application/x-shockwave-flash" name="flashUploader"
																id="flashUploader">
														</span>
													</div> 
												</a>
										</form>
										 -->
									</div>
									<input type="text" id="textInput" class="chatInput lightBorder" style="width:452px"></input>
									<a href="javascript:;" class="chatSend" id="send_btn" click="sendMsg@.inputArea"><b>发送</b> </a>
									<div id="recordInput" class="recordInput chatInput" style="display:none;"></div>
									<div class="clr"></div>

									<textarea type="text" id="textInput" class="chatInput lightBorder" style="visibility: hidden; position: absolute; left: -1000px; padding: 0px 10px; width: 402px; overflow: hidden;"></textarea>
								</div>
									<div class="emojiPanel" id="emojiPanel" style="display: none;"> 
										<div style="position:absolute;"></div>
										<ul class="faceTab">
											<li>
												<a class="chooseFaceTab" href="javascript:chooseEmojiPanel('faceBox');" click="chooseEmojiPanel@.faceTab" un="faceBox">QQ表情</a>
											</li>
											<!-- 
											<li>
												<a href="javascript:chooseEmojiPanel('emojiBox');" click="chooseEmojiPanel@.faceTab" un="emojiBox" class="">符号表情</a>
											</li>
											 -->
										</ul>
										<div class="faceWrap" style="zoom:1;outline:none;" tabindex="0" hidefocus="true">
											<div class="faceBox emojiArea" style="display: block;" click="javascript:chooseEmoji();">
												<a title="微笑" class="f14" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="撇嘴" class="f1" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="色" class="f2" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="发呆" class="f3" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="得意" class="f4" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="流泪" class="f5" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="害羞" class="f6" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="闭嘴" class="f7" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="睡" class="f8" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="大哭" class="f9" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="尴尬" class="f10" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="发怒" class="f11" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="调皮" class="f12" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a>
												<a title="呲牙" class="f13" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="惊讶" class="f0 borderRightNone" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="难过" class="f15" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="酷" class="f16" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="冷汗" class="f96" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="抓狂" class="f18" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="吐" class="f19" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="偷笑" class="f20" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="愉快" class="f21" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="白眼" class="f22" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="傲慢" class="f23" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="饥饿" class="f24" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="困" class="f25" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a>
												<a title="惊恐" class="f26" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="流汗" class="f27" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="憨笑" class="f28" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="悠闲" class="f29 borderRightNone" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="奋斗" class="f30" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="咒骂" class="f31" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="疑问" class="f32" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="嘘" class="f33" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="晕" class="f34" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="疯了" class="f35" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="衰" class="f36" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="骷髅" class="f37" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="敲打" class="f38" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="再见" class="f39" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a>
												<a title="擦汗" class="f97" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="抠鼻" class="f98" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="鼓掌" class="f99" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="糗大了" class="f100" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="坏笑" class="f101 borderRightNone" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="左哼哼" class="f102" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="右哼哼" class="f103" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="哈欠" class="f104" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="鄙视" class="f105" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="委屈" class="f106" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="快哭了" class="f107" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="阴险" class="f108" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="亲亲" class="f109" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="吓" class="f110" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="可怜" class="f111" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a>
												<a title="菜刀" class="f112" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="西瓜" class="f89" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="啤酒" class="f113" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="篮球" class="f114" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="乒乓" class="f115 borderRightNone" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="咖啡" class="f60" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="饭" class="f61" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="猪头" class="f46" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="玫瑰" class="f63" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="凋谢" class="f64" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="嘴唇" class="f116" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="爱心" class="f66" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a>
												<a title="心碎" class="f67" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="蛋糕" class="f53" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="闪电" class="f54" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="炸弹" class="f55" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="刀" class="f56" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="足球" class="f57" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="瓢虫" class="f117" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="便便" class="f59 borderRightNone" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="月亮" class="f75" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="太阳" class="f74" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="礼物" class="f69" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="拥抱" class="f49" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a>
												<a title="强" class="f76" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="弱" class="f77" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="握手" class="f78" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="胜利" class="f79" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="抱拳" class="f118" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="勾引" class="f119" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="拳头" class="f120" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="差劲" class="f121" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="爱你" class="f122" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="NO" class="f123" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="OK" class="f124 borderRightNone" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="爱情" class="f42 borderBottomNone" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a>
												<a title="飞吻" class="f85 borderBottomNone" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="跳跳" class="f43 borderBottomNone" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="发抖" class="f41 borderBottomNone" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="怄火" class="f86 borderBottomNone" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="转圈" class="f125 borderBottomNone" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="磕头" class="f126 borderBottomNone" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="回头" class="f127 borderBottomNone" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="跳绳" class="f128 borderBottomNone" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="挥手" class="f129 borderBottomNone" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="激动" class="f130 borderBottomNone" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="街舞" class="f131 borderBottomNone" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="献吻" class="f132 borderBottomNone" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a>
												<a title="左太极" class="f133 borderBottomNone" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a><a title="右太极" class="f134 borderBottomNone borderRightNone" href="javascript:;" onclick="javascript:chooseEmoji(this);"></a>
												<div style="display: none;" class="facePreview">
													<div>
														<p class="faceImg"><img alt="" src="https://res.wx.qq.com/zh_CN/htmledition/images/spacer17ced3.gif"></p><p class="faceName"></p>
													</div>
												</div>
											</div> 
											<!-- 
											<div style="display: none;" class="emojiBox">
												<div class="emojiContent">
													<div class="emojiFacePanel" click="chooseSysEmoji">
														<a title="笑脸" class="" href="javascript:;"></a><a title="开心" class="" href="javascript:;"></a><a title="大笑" class="" href="javascript:;"></a><a title="热情" class="" href="javascript:;"></a><a title="眨眼" class="" href="javascript:;"></a><a title="色" class="" href="javascript:;"></a><a title="接吻" class="" href="javascript:;"></a><a title="亲吻" class="" href="javascript:;"></a><a title="脸红" class="" href="javascript:;"></a><a title="露齿笑" class="" href="javascript:;"></a><a title="满意" class="" href="javascript:;"></a><a title="戏弄" class="" href="javascript:;"></a><a title="吐舌" class="" href="javascript:;"></a><a title="无语" class="" href="javascript:;"></a><a title="得意" class="borderRightNone" href="javascript:;"></a><a title="汗" class="" href="javascript:;"></a><a title="失望" class="" href="javascript:;"></a><a title="低落" class="" href="javascript:;"></a><a title="呸" class="" href="javascript:;"></a><a title="焦虑" class="" href="javascript:;"></a><a title="担心" class="" href="javascript:;"></a><a title="震惊" class="" href="javascript:;"></a><a title="悔恨" class="" href="javascript:;"></a><a title="眼泪" class="" href="javascript:;"></a><a title="哭" class="" href="javascript:;"></a><a title="破涕为笑" class="" href="javascript:;"></a>
														<a title="晕" class="" href="javascript:;"></a><a title="恐惧" class="" href="javascript:;"></a><a title="心烦" class="" href="javascript:;"></a><a title="生气" class="borderRightNone" href="javascript:;"></a><a title="睡觉" class="" href="javascript:;"></a><a title="生病" class="" href="javascript:;"></a><a title="恶魔" class="" href="javascript:;"></a><a title="外星人" class="" href="javascript:;"></a><a title="心" class="" href="javascript:;"></a><a title="心碎" class="" href="javascript:;"></a><a title="丘比特" class="" href="javascript:;"></a><a title="闪烁" class="" href="javascript:;"></a><a title="星星" class="" href="javascript:;"></a><a title="叹号" class="" href="javascript:;"></a><a title="问号" class="" href="javascript:;"></a><a title="睡着" class="" href="javascript:;"></a><a title="水滴" class="" href="javascript:;"></a><a title="音乐" class="" href="javascript:;"></a><a title="火" class="borderRightNone" href="javascript:;"></a><a title="便便" class="" href="javascript:;"></a><a title="强" class="" href="javascript:;"></a><a title="弱" class="" href="javascript:;"></a><a title="拳头" class="" href="javascript:;"></a><a title="胜利" class="" href="javascript:;"></a><a title="上" class="" href="javascript:;"></a><a title="下" class="" href="javascript:;"></a><a title="右" class="" href="javascript:;"></a><a title="左" class="" href="javascript:;"></a><a title="第一" class="" href="javascript:;"></a><a title="强壮" class="" href="javascript:;"></a><a title="吻" class="" href="javascript:;"></a><a title="热恋" class="" href="javascript:;"></a><a title="男孩" class="" href="javascript:;"></a><a title="女孩" class="borderRightNone" href="javascript:;"></a><a title="女士" class="" href="javascript:;"></a><a title="男士" class="" href="javascript:;"></a><a title="天使" class="" href="javascript:;"></a>
														<a title="骷髅" class="" href="javascript:;"></a><a title="红唇" class="" href="javascript:;"></a><a title="太阳" class="" href="javascript:;"></a><a title="下雨" class="" href="javascript:;"></a><a title="多云" class="" href="javascript:;"></a><a title="雪人" class="" href="javascript:;"></a><a title="月亮" class="" href="javascript:;"></a><a title="闪电" class="" href="javascript:;"></a><a title="海浪" class="" href="javascript:;"></a><a title="猫" class="" href="javascript:;"></a><a title="小狗" class="" href="javascript:;"></a><a title="老鼠" class="borderRightNone" href="javascript:;"></a><a title="仓鼠" class="" href="javascript:;"></a><a title="兔子" class="" href="javascript:;"></a><a title="狗" class="" href="javascript:;"></a><a title="青蛙" class="" href="javascript:;"></a><a title="老虎" class="" href="javascript:;"></a><a title="考拉" class="" href="javascript:;"></a><a title="熊" class="" href="javascript:;"></a><a title="猪" class="" href="javascript:;"></a><a title="牛" class="" href="javascript:;"></a><a title="野猪" class="" href="javascript:;"></a><a title="猴子" class="" href="javascript:;"></a><a title="马" class="" href="javascript:;"></a><a title="蛇" class="" href="javascript:;"></a><a title="鸽子" class="" href="javascript:;"></a><a title="鸡" class="borderRightNone" href="javascript:;"></a><a title="企鹅" class="" href="javascript:;"></a><a title="毛虫" class="" href="javascript:;"></a><a title="章鱼" class="" href="javascript:;"></a><a title="鱼" class="" href="javascript:;"></a><a title="鲸鱼" class="" href="javascript:;"></a><a title="海豚" class="" href="javascript:;"></a><a title="玫瑰" class="" href="javascript:;"></a><a title="花" class="" href="javascript:;"></a><a title="棕榈树" class="" href="javascript:;"></a><a title="仙人掌" class="" href="javascript:;"></a><a title="礼盒" class="" href="javascript:;"></a><a title="南瓜灯" class="" href="javascript:;"></a><a title="鬼魂" class="" href="javascript:;"></a><a title="圣诞老人" class="" href="javascript:;"></a><a title="圣诞树" class="borderRightNone" href="javascript:;"></a><a title="礼物" class="" href="javascript:;"></a><a title="铃" class="" href="javascript:;"></a>
														<a title="庆祝" class="" href="javascript:;"></a><a title="气球" class="" href="javascript:;"></a><a title="CD" class="" href="javascript:;"></a><a title="相机" class="" href="javascript:;"></a><a title="录像机" class="" href="javascript:;"></a><a title="电脑" class="" href="javascript:;"></a><a title="电视" class="" href="javascript:;"></a><a title="电话" class="" href="javascript:;"></a><a title="解锁" class="" href="javascript:;"></a><a title="锁" class="" href="javascript:;"></a><a title="钥匙" class="" href="javascript:;"></a><a title="成交" class="" href="javascript:;"></a><a title="灯泡" class="borderRightNone" href="javascript:;"></a><a title="邮箱" class="" href="javascript:;"></a><a title="浴缸" class="" href="javascript:;"></a><a title="钱" class="" href="javascript:;"></a><a title="炸弹" class="" href="javascript:;"></a><a title="手枪" class="" href="javascript:;"></a><a title="药丸" class="" href="javascript:;"></a><a title="橄榄球" class="" href="javascript:;"></a><a title="篮球" class="" href="javascript:;"></a><a title="足球" class="" href="javascript:;"></a><a title="棒球" class="" href="javascript:;"></a><a title="高尔夫" class="" href="javascript:;"></a><a title="奖杯" class="" href="javascript:;"></a><a title="入侵者" class="" href="javascript:;"></a><a title="唱歌" class="" href="javascript:;"></a><a title="吉他" class="borderRightNone" href="javascript:;"></a><a title="比基尼" class="" href="javascript:;"></a><a title="皇冠" class="" href="javascript:;"></a><a title="雨伞" class="" href="javascript:;"></a><a title="手提包" class="" href="javascript:;"></a><a title="口红" class="" href="javascript:;"></a><a title="戒指" class="" href="javascript:;"></a><a title="钻石" class="" href="javascript:;"></a><a title="咖啡" class="" href="javascript:;"></a><a title="啤酒" class="" href="javascript:;"></a><a title="干杯" class="" href="javascript:;"></a><a title="鸡尾酒" class="" href="javascript:;"></a><a title="汉堡" class="" href="javascript:;"></a><a title="薯条" class="" href="javascript:;"></a><a title="意面" class="" href="javascript:;"></a>
														<a title="寿司" class="borderRightNone" href="javascript:;"></a><a title="面条" class="" href="javascript:;"></a><a title="煎蛋" class="" href="javascript:;"></a><a title="冰激凌" class="" href="javascript:;"></a><a title="蛋糕" class="" href="javascript:;"></a><a title="苹果" class="" href="javascript:;"></a><a title="飞机" class="" href="javascript:;"></a><a title="火箭" class="" href="javascript:;"></a><a title="自行车" class="" href="javascript:;"></a><a title="高铁" class="" href="javascript:;"></a><a title="警告" class="" href="javascript:;"></a><a title="旗" class="" href="javascript:;"></a><a title="男人" class="" href="javascript:;"></a><a title="女人" class="" href="javascript:;"></a><a title="O" class="" href="javascript:;"></a><a title="X" class="borderRightNone" href="javascript:;"></a><a title="版权" class=" borderBottomNone" href="javascript:;"></a><a title="注册商标" class=" borderBottomNone" href="javascript:;"></a><a title="商标" class=" borderBottomNone" href="javascript:;"></a>
													</div>
												</div>
											</div>
											<div style="display:none;" class="rabbitBox" click="chooseCustomEmoji">
												<div class="rabbitContent">
													<div class="rabbitPanel">
														<a class="r11" href="javascript:;" un="icon_002.gif"></a><a class="r12" href="javascript:;" un="icon_007.gif"></a> <a class="r13" href="javascript:;" un="icon_010.gif"></a> <a class="r14" href="javascript:;" un="icon_012.gif"></a> <a class="r15 borderRightNone" href="javascript:;" un="icon_013.gif"></a><a class="r21" href="javascript:;" un="icon_018.gif"></a><a class="r22" href="javascript:;" un="icon_019.gif"></a> <a class="r23" href="javascript:;" un="icon_021.gif"></a> <a class="r24" href="javascript:;" un="icon_022.gif"></a> <a class="r25 borderRightNone" href="javascript:;" un="icon_024.gif"></a> <a class="r31" href="javascript:;" un="icon_027.gif"></a><a class="r32" href="javascript:;" un="icon_029.gif"></a>
														<a class="r33" href="javascript:;" un="icon_030.gif"></a> <a class="r34" href="javascript:;" un="icon_035.gif"></a> <a class="r35 borderRightNone" href="javascript:;" un="icon_040.gif"></a> <a class="r41" href="javascript:;" un="icon_020.gif"></a>
													</div>
												</div>
											</div>
											 -->
										</div><!--<a title="关闭" class="faceCloseIcon" href="javascript:;" click="closeEmojiPanel"><img src="https://res.wx.qq.com/zh_CN/htmledition/images/spacer17ced3.gif" /></a>-->
										<div class="faceTriangle"><div class="faceTrianglePanel"><div class="faceTriangle1"></div><div class="faceTriangle2"></div></div></div> 
									</div>
							</div>
						</div>
						<!--change main panel end-->
					</div>
				</div>
				<div style="clear: both; visibility: visible;"></div>
			</div>
		</div>
	
	<div id="mediaPlayer" style="width: 0px; height: 0px;">
		<img id="jp_poster_1" style="width: 0px; height: 0px; display: none;">
			<param name="flashvars" value="jQuery=jQuery&amp;id=mediaPlayer&amp;vol=0.8&amp;muted=false">
			<param name="allowscriptaccess" value="always">
			<param name="bgcolor" value="#000000"><param name="wmode" value="window"></object></div>
	
	
	</div>
	<script type="text/javascript">
		//显示表情面板
		function showEmojiPanel(){
			 $("#emojiPanel").toggle();
		}
		//选择表情tab
		function chooseEmojiPanel(f) {
			var e = $(".faceWrap");
		    $(".faceTab").find("a").each(function() {
			  var a = $(this), c = a.attr("un");
		      c != f ? (a.removeClass("chooseFaceTab"), e.find("." + c).hide())
		    		  : (a.addClass("chooseFaceTab"), e.find("." + c).show())
		    })
	 	}
		
		function closeEmojiPanel() {
		    $("#emojiPanel").fadeOut("fast")
	  	}
		
		function chooseEmoji(self) {
		    closeEmojiPanel();
		    setTimeout(function() {
		       $(".chatInput").insertTextToInput("[" + self.title + "]");
		       $("#textInput").focus();
		     })
		}
	</script>
</body>
</html>



