<?xml version="1.0" encoding="utf-8"?>
<%@page import="java.util.Enumeration"%>
<%@page import="com.eastrobot.app.web.VerifyActionSupport"%>
<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	VerifyActionSupport.writeNonce("wap", request, response);
%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta name="viewport"
	content="width=device-width;initial-scale=1.4;minimum-scale=1.0;maximum-scale=2.0" />
<meta name="MobileOptimized" content="240" />
<meta name="robots" content="index,nofollow" />
<link rel="stylesheet" type="text/css" href="css/iphone_wap_.css" />
<title>小i智能机器人WAP展示</title>
</head>
<body>
	<div class="wap_wrap"
		style="background : none; padding : 0; margin: 0;">
		<div class="wap_content left">
			<div class="wap_banner">
				<img src="images/banner1.jpg" width="222" height="40" title="小i机器人"
					alt="小i机器人">
			</div>

			<div class="wap_search">
				<c:if
					test="${not empty sessionScope.verifyCaptchaState and sessionScope.verifyCaptchaState gt 0}">
					<div class="wap_huanying">您好，我是小i机器人，请直接输入您想咨询的问题，谢谢！</div>
					<div class="wap_searchcn1">
						<form id="askForm" action="wap-demo.action" method="post">
							<p class="wap_searchp1 left">
								<input style="border : 1px solid #F48102;" id="askInput"
									name="requestContent" type="text" autofocus="autofocus" />
							</p>
							<p class="wap_searchp2 left">
								<input value="提问" type="submit" />
							</p>
						</form>
					</div>
				</c:if>
				<c:if
					test="${empty sessionScope.verifyCaptchaState or sessionScope.verifyCaptchaState eq 0}">
					<div class="wap_huanying">
						您好，我是小i机器人，请输入验证码： <img
							src="<c:url value='/p4pages/captcha.action'/>" alt="验证码"
							style="cursor: pointer;"
							onclick="this.src='<c:url value='/p4pages/captcha.action'/>?r='+new Date().getTime()" />
					</div>
					<form name="verifCodeForm"
						action="<c:url value='wap-demo.action'/>" method="post">
						<p class="wap_searchp1 left">
							<input style="border : 1px solid #F48102;" name="captcha"
								id="ecode" maxlength="6" />
						</p>
						<p class="wap_searchp2 left">
							<input value="确定" type="submit" autofocus="autofocus" />
						</p>
					</form>
					<div style="color:#d23802;margin-top:25px;margin-left: 15px"
						id="errorMsg">
						<c:if test="${sessionScope.verifyCaptchaState eq 0}">
							验证码输入错误
						</c:if>
					</div>
				</c:if>
				<ul class="wap_searchul left" style="padding:0;">
					<p class="wap_searchulp1">快速提问：</p>
					<c:if
						test="${not empty sessionScope.verifyCaptchaState and sessionScope.verifyCaptchaState gt 0}">
						<li><a href="#"
							onclick="document.getElementById('askInput').value=this.innerHTML;document.getElementById('askForm').submit();">什么是智能网络机器人？</a>
						</li>
						<li><a href="#"
							onclick="document.getElementById('askInput').value=this.innerHTML;document.getElementById('askForm').submit();">小i机器人可以做什么？</a>
						</li>
					</c:if>
					<c:if
						test="${empty sessionScope.verifyCaptchaState or sessionScope.verifyCaptchaState==0}">
						<li>什么是智能网络机器人？</li>
						<li>小i机器人可以做什么？</li>
					</c:if>
				</ul>

				<div class="wap_content2 left">
					<p class="wap_cn1">
						<c:if test="${not empty requestContent}">
							<b>我:</b>${requestContent }</c:if>
					</p>

					<p class="wap_cn2">
						<c:if test="${not empty responseContent}">
							<span>小i机器人:</span> ${responseContent }</c:if>
					</p>
				</div>
			</div>
		</div>
	</div>
	<script src="../../js/verify.js"></script>
</body>
</html>


