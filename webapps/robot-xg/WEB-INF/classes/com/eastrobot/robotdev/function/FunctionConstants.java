package com.eastrobot.robotdev.function;

public interface FunctionConstants {

	// 转人工队列功能常量
	static interface AcsSeq {
		public static final String ACS_SEQ_VALUE = "acsSeqValue";

		public static final String SWITCH = "dev.acsSeqSwitch"; // 是否开启此功能
		public static final String DEV_ACS_SEQ_CONTENT_HEADER = "dev.acsSeqContentHeader"; // 队列列表头部内容
		public static final String DEV_ACS_SEQ_CONTENT = "dev.acsSeqContent"; // 队列列表内容
	}

	/**
	 * 多次默认回复转人工功能常量
	 */
	static interface DefaultReplay {
		//服务参数配置参数
		public static final String SWITCH = "dev.defaultReplyToacs.switch"; // 是否开启此功能
		public static final String DEFAULT_REPLY_NUM = "dev.defaultReplyToacs.defaultReplyNum"; // 多少次默认回复之后转接人工客服
		public static final String TOACSMODE = "dev.defaultReplyToacs.toacsMode"; // toacsMode=1为指令转人工（默认），toacsMode=2为进入ACSHandler
		public static final String TOACSCOMMAND = "dev.defaultReplyToacs.toacsCommand"; // 当toacsMode为1时的转人工指令内容，此时type为101
		//缓存参数
		public static final String CACHE_NUM = "defaultReplyToacs.num"; // 缓存的默认回复出现的次数
	}

	// 解决、未解决功能常量
	static interface FaqVote {
		public static final String SWITCH = "dev.faqVote.switch"; // 是否开启此功能
	}

	// 序号递增功能常量
	static interface SeqNoIncrease {

		public static final String INCREASE_FLAG = "increaseFlag";

		public static final String SWITCH = "dev.seqNoIncreaseSwitch"; // 是否开启此功能
		public static final String DEV_SEQ_NO_SIZE = "dev.seqNoSize"; // 每次允许递增的个数
		public static final String DEV_SEQ_NO_LIMIT = "dev.seqNoLimit"; // 序号递增上限
		public static final String DEV_SEQ_NO_START = "dev.seqNoStart"; // 序号递增起始数字
		public static final String DEV_SEQ_NO_RENDER_STYLE = "dev.seqNoRenderStyle"; // 渲染后序号的样式

	}

}
