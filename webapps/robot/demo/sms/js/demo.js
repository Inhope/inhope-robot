String.prototype.trim = function() {
	return this.replace(/(^\s*)|(\s*$)/g, "");
}
var inputArea;
var contentDiv;
var scroller;
var showReceiveMsg = function(content) {
	var p = document.createElement("p");
	p.className = "gray_contentp1";
	var date = new Date();
	p.innerHTML = "小i说: " + date.pattern("yyyy-MM-dd hh:mm:ss");
	var d1 = document.createElement("div");
	d1.className = "gray_content left";
	var d2 = document.createElement("div");
	d2.className = "gray_cn1";
	var d3 = document.createElement("div");
	d3.className = "gray_cn2";
	var d4 = document.createElement("div");
	d4.className = "gray_cn3";
	contentDiv.appendChild(d1);
	d1.appendChild(p);
	d1.appendChild(d2);
	d2.appendChild(d3);
	content = content.replace(/\r\n/g, '<br>');
	d4.innerHTML = content.replace(/\n/g, '<br>');
	d3.appendChild(d4);
	scrollFn();
	if (contentDiv.scrollHeight > 312)
		scroller.parentNode.style.display = '';
};
var showSendMsg = function(data) {
	var d1 = document.createElement("div");
	d1.className = "green_content right";
	var p = document.createElement("p");
	p.className = "green_contentp1";
	var date = new Date();
	p.innerHTML = "我说: " + date.pattern("yyyy-MM-dd hh:mm:ss");
	var d2 = document.createElement("div");
	d2.className = "green_cn1";
	var d3 = document.createElement("div");
	d3.className = "green_cn2";
	var d4 = document.createElement("div");
	d4.className = "green_cn3";
	d4.innerHTML = data;
	contentDiv.appendChild(d1);
	d1.appendChild(p);
	d1.appendChild(d2);
	d2.appendChild(d3);
	d3.appendChild(d4);
	scrollFn();
};
Date.prototype.pattern = function(fmt) {
	var o = {
		"M+" : this.getMonth() + 1, // 月份
		"d+" : this.getDate(), // 日
		"h+" : this.getHours() == 0 ? 12 : this.getHours(), // 小时
		"H+" : this.getHours(), // 小时
		"m+" : this.getMinutes(), // 分
		"s+" : this.getSeconds(), // 秒
		"q+" : Math.floor((this.getMonth() + 3) / 3), // 季度
		"S" : this.getMilliseconds()
		// 毫秒
	};
	var week = {
		"0" : "\u65e5",
		"1" : "\u4e00",
		"2" : "\u4e8c",
		"3" : "\u4e09",
		"4" : "\u56db",
		"5" : "\u4e94",
		"6" : "\u516d"
	};
	if (/(y+)/.test(fmt)) {
		fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4
						- RegExp.$1.length));
	}
	if (/(E+)/.test(fmt)) {
		fmt = fmt.replace(RegExp.$1, ((RegExp.$1.length > 1)
						? (RegExp.$1.length > 2 ? "\u661f\u671f" : "\u5468")
						: "")
						+ week[this.getDay() + ""]);
	}
	for (var k in o) {
		if (new RegExp("(" + k + ")").test(fmt)) {
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1)
							? (o[k])
							: (("00" + o[k]).substr(("" + o[k]).length)));
		}
	}
	return fmt;
}
var scrollFn = function() {
	contentDiv.scrollTop += 999;
};

var _sendSms = function(content) {
	showSendMsg(content);
	$.ajax({
				url : "sms-demo.action",
				type : "POST",
				dataType : "text",
				data : {
					"content" : content
				},
				success : function(data, textStatus, xhr) {
					window._verify();
					if(xhr.getResponseHeader("X-VERIFY-CAPTCHA-STATE")=="0"){
						showReceiveMsg("验证失败，请重新输入验证码。");
						inputArea.value = '';
						inputArea.focus();
					}else if(xhr.getResponseHeader("X-VERIFY-CAPTCHA-STATE")=="1") {
						showReceiveMsg("验证成功，请直接输入您想咨询的问题。");
						inputArea.value = '';
						inputArea.focus();
					}else {
						setTimeout(function() {
							showReceiveMsg(data);
							inputArea.value = '';
							inputArea.focus();
						}, 500);
					}
				},
				error : function() {
					alert("服务器内部错误");
				}
			});
};

//重载验证码  
var reloadCaptcha =  function reloadVerifyCode(obj){
    var timenow = new Date().getTime();
    var pathName = window.location.pathname.substring(1);
	var webName = pathName == '' ? '' : pathName.substring(0, pathName.indexOf('/'));
    obj.setAttribute("src", window.location.protocol + '//' + window.location.host + '/'+ webName+"/p4pages/captcha.action?d="+timenow);
};
window.onload = function() {
	$.ajax({
				url : "sms-demo.action",
				type : "GET",
				headers : {
					"X-INIT-SESSION" : "1"
				},
				success : function(data, textStatus, xhr) {
					window._verify();
					if(xhr.getResponseHeader("X-VERIFY-CAPTCHA-STATE")=="0") {
						var pathName = window.location.pathname.substring(1);
						var webName = pathName == '' ? '' : pathName.substring(0, pathName.indexOf('/'));
						showReceiveMsg("<div style=\"height:20px;line-height:20px;margin:0;padding:0;float:left\">请输入验证码：</div><img src=\""+window.location.protocol + '//' + window.location.host + '/'+ webName+"/p4pages/captcha.action\" alt=\"验证码\" style=\"margin:0;cursor: pointer;float:left\" onclick=\"reloadCaptcha(this)\"/>");
					}
				},
				error : function() {
					
				}
			});
	
	var btn = document.getElementById("send_btn");
	inputArea = document.getElementById("input_area");
	contentDiv = document.getElementById("phoneContent");
	btn.onclick = function() {
		if (inputArea.value.trim())
			_sendSms(inputArea.value);
	};
	inputArea.onkeyup = function(e) {
		e = e || window.event;
		if (e.keyCode == 13 && inputArea.value.trim()) {
			_sendSms(inputArea.value);
		}
	}
	scroller = document.getElementById('scroller');
	Drag.init(scroller, null, 0, 0, 0, 222);
	scroller.onDrag = function(x, y) {
		var h = (y / 222) * (contentDiv.scrollHeight - 312);
		contentDiv.scrollTop = h;
	}
};
