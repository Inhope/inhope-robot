package com.eastrobot.robotdev.servlet;

import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BaseServlet extends HttpServlet {
	
	private static final long serialVersionUID = -2905544461830144416L;
	private static Logger log = LoggerFactory.getLogger(BaseServlet.class);

	protected void write(HttpServletResponse response, String msg) {
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(msg);
			out.flush();
		} catch (Exception e) {
			log.error("", e);
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

}
