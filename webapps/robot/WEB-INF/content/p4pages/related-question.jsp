<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
	.title {
		color: #38608E;
		font: 13px 宋体;
	}
	.faq {
		width:99%;
		padding : 1px;
	}
	.question {
		color: #38608E;
		font: 13px 宋体;
		cursor : pointer;
	}
	.answer {
		color: #666666;
		padding:4px 0 0 12px;
		display:none;
		font: 13px 宋体;
		word-break: break-word;
	}
	.question_content {
		float:left;height:auto;line-height:20px;width:92%;
	}
	.question_leftimg {
		float:left;height:12px;width:12px;margin-top:3px;
		background-image: url(images/arrow_l_right.gif);
	}
	.question_rightimg {
		float:right;height:18px;width:18px;
		background-image: url(images/arrow_r_down.gif);
	}
</style>
<script type="text/javascript" src="../js/jquery.js"></script> 
<script type="text/javascript" src="js/related-question.js"></script>

<title>相关问题</title> 
</head>
<body>
	<div class="title">您可能还关注下列相关问题：</div>
	<c:forEach var="suggestedQA" varStatus="status" items="${response.suggestedQAs}">
	<div class="faq">
		<div class="question">
			<div class="question_rightimg"></div>
			<div class="question_leftimg"></div>
			<div class="question_content">${suggestedQA.question}</div>
			<div style="clear:both;line-height:0;height:0;"></div>
		</div>
		<div class="answer">${suggestedQA.answer}</div>
	</div>
	</c:forEach>
</body>
</html>
