<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.Map"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
	.title {
		color: #38608E;
		font: 13px 宋体;
	}
	.question_list {
		margin:5px 20px;
		padding:0;
	}
	.question {
		color: #38608E;
		font: 13px 宋体;
		cursor : pointer;
		height:auto;
		line-height:20px;
	}
</style>
<title>营销活动</title> 
</head>
<body>
	<c:if test="${empty emptyResult}">
		<div class="title">近期活动，点击标题查看详情：</div>
		<ul class="question_list">
		<c:forEach var="act" items="${activities}">
			<li class="question"><a target="_blank" href="${act.value}">${act.key}</a></li>
		</c:forEach>
		</ul>
	</c:if>
	<c:if test="${not empty emptyResult}">
		<div class="title">近期无相关活动。</div>
	</c:if>
</body>
</html>
