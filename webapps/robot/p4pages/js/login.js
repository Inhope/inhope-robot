$(function(){
	$('#submitButton').click(function(){
		var username = $('input[name=username]');
		if(username.val().length==0){
			alert('用户名不能为空！');
			return false;
		}
		var password = $('input[name=password]');
		if(password.val().length==0){
			alert('密码不能为空！');
			return false;
		}
		var captcha = $('input[name=captcha]');
		if(captcha.val().length==0){
			alert('图片验证码不能为空！');
			return false;
		}
		$('#loginForm').submit();
	});
	$('#captchaImg').click(function(){
		this.src = "captcha.action?"+new Date().getTime();
	});
	
	
	if(status == -1) {
		alert('图片验证码错误！');
	}
	else if(status == -2) {
		alert('用户名或密码错误！');
	}
	else if(status == 1) {
		alert('已登录！');
	}
});