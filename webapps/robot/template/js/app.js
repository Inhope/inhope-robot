var _PREFERRED_TRANSPORT = 'jsonp';
// var _USER_INFO_EXPIRE = 30;
// var _USER_PLATFORM = 'wap';
// var _LOGIN_TRANSPORT = 'flash';
var _FLASHSERVER_PORT = 8443;
var _ROOT_PATH = '../../../';
var _ROOT_IMAGE_PATH = _ROOT_PATH + 'images/';
var _ROOT_SCRIPT_PATH = _ROOT_PATH + 'js/';
var _APP_PATH = '../../';
var _APP_IMAGE_PATH = _APP_PATH + 'images/';
var _APP_SCRIPT_PATH = _APP_PATH + 'js/';
var __packages = [ _ROOT_SCRIPT_PATH + 'jquery.js', _ROOT_SCRIPT_PATH + 'jquery.cookie.js', _ROOT_SCRIPT_PATH + 'jquery-ex.js',
		_APP_SCRIPT_PATH + "transport.js", _APP_SCRIPT_PATH + "client-dep.js", _APP_SCRIPT_PATH + "client.js" ];
// _DEFAULT_P4_WIDTH = 745;
// _MIN_P4_WIDTH = 300;

var getSwfHtml = function(swfsrc) {
	var html = '<a href="#">' + '<embed id="_mic_flash" style="width:220px;height:140px;" type="application/x-shockwave-flash" src="' + swfsrc
			+ '?ts=' + new Date().getTime() + '" allowScriptAccess="always"></embed>' + '</a>';
	if (document.all)
		html = '<a href="#">'
				+ '<object id="_mic_flash" style="width:220px;height:140px;" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" allowScriptAccess="always" ><param name="movie" value="'
				+ swfsrc + '?ts=' + new Date().getTime() + '"/><param name="allowScriptAccess" value="always" /></object>' + '</a>';
	return html;
};

_CLIENT_INIT = function() {
	$client.on('initconfigreceived', function(cfg) {
		if (cfg.sessionPersistent) {
			if (uinfoStore) {
				var val = uinfoStore.get('latestQA');
				if (val)
					document.getElementById('outputArea').innerHTML += val;
				val = uinfoStore.get('robotName');
				if (val)
					$client.config.robotName = val;
			}
		}
		if ('true' != cfg.speechEnabled)
			return;
		var grammarId = cfg.speechGrammarId;
		var recogAddr = null, synthAddr = null;
		if (cfg.speechAddr) {
			var addrs = cfg.speechAddr.split(',');
			if (addrs.length == 1) {
				recogAddr = synthAddr = addrs[0];
			} else {
				recogAddr = addrs[0];
				synthAddr = addrs[1];
			}
		} else {
			recogAddr = synthAddr = 'vcloud.xiaoi.com';
		}
		if (recogAddr.indexOf(':') != -1)
			recogAddr = recogAddr.substring(0, recogAddr.indexOf(':'));
		recogAddr = 'rtmp://' + recogAddr + '/app';
		synthAddr = 'http://' + synthAddr + '/synthflv';
		$('#enableMic').attr('checked', false);
		window.onMicReady = function() {
			var el = $('#enableMic');
			el.parent().show();
			if (el.get(0).checked)
				micReady();
		};
		$('#micButtonDiv').css('right', 1500);
		$('#micButtonDiv').html(getSwfHtml(_APP_SCRIPT_PATH + 'mic.swf'));
		var $mic = $('#_mic_flash');
		var mic0 = $mic.get(0);
		if ($client.config.isIE6)
			$mic.hide();
		function micReady() {
			if ($client.config.isIE6)
				$mic.show();
			$('#micButtonDiv').css('right', 350);
		}
		var firstTime = true;
		function checkMic() {
			if (!firstTime) {
				micReady();
				$mic.css('width', 64).css('height', 64);
				return;
			}
			var ret = mic0.initMic();
			if (ret == 'oldVersion')
				alert('你的Flash版本过低，请先更新到最新版本再尝试语音功能。');
			else if (ret == 'noMic')
				alert('没有在您的机器上找到麦克风设备');
			else if (ret == 'muted') {
				setTimeout(function() {
					micReady();
				}, 250);
			} else
				micReady();
			firstTime = false;
		}
		$('#enableMic').bind('change', function() {
			if (this.checked) {
				checkMic();
				$('#micButtonDiv').css('right', 150);
			} else {
				$('#micButtonDiv').css('right', 1500);
				if ($client.config.isIE6)
					$mic.hide();
				mic0.reset();
			}
		});
		window.startRecord = function() {
			setTimeout(function() {
				mic0.startRecord(recogAddr, grammarId);
			}, 350);
		};
		window.stopRecord = function() {
			mic0.stopRecord();
		};
		window.dataDetect = function(info) {
			if (console)
				console.log(info);
		};
		window.onQuestionReceived = function(question) {
			if (!$('#enableMic').get(0).checked)
				return;
			if (!question) {
				var dftReply = '对不起，我没有听清楚，请您再说一遍。';
				$client.showMessage($client.config.robotName, dftReply);
				mic0.playSound(dftReply, synthAddr);
			} else {
				$client.els.inputBox.val(question);
				var ret = $client.sendText();
				if (ret == false)
					mic0.reset();
			}
		};
		window.playbackComplete = function() {
			mic0.reset();
		};
		window.onSilenceDetected = function() {
			mic0.loading();
			mic0.stopRecord();
		};
		window.muteStateChange = function(muted) {
			if (muted) {
				$mic.css('width', 220).css('height', 140);
			} else {
				$mic.css('width', 64).css('height', 64);
				$('#micButtonDiv').css('top', 0);
				$('#micButtonDiv').css('right', 150);
			}
		};
		window.onunload = function() {
			$('#micButtonDiv').html('');
		};
		$client.on('messagereceived', function(msg) {
			var micEnabled = $('#enableMic').get(0).checked;
			if (!micEnabled)
				return;
			var content = msg.content.replace(/&nbsp;/g, '');
			content = content.replace(/<br>/g, '\n');
			content = content.replace(/\[link[^\]]*\]/g, '').replace(/\[\/link\]/g, '');
			content = content.replace(/<a[^>]*>/g, '').replace(/<\/a>/g, '');
			content = content.replace(/<speak[^>]*>/g, '').replace(/<\/speak>/g, '').replace(/<audio[^>]*>/g, '');
			if (content.indexOf('　 　') == -1)
				content += '　 　';
			if (content.length) {
				mic0.playSound(content, synthAddr);
				return;
			}
			var nrIdx = 0;
			var rIdx = content.indexOf('\r');
			var nIdx = content.indexOf('\n');
			if (rIdx > 0 && nIdx > 0) {
				nrIdx = Math.min(rIdx, nIdx);
			} else if (rIdx > 0) {
				nrIdx = rIdx;
			} else if (nIdx > 0) {
				nrIdx = nIdx;
			}
			content = content.substr(0, nrIdx);
			if (content.length > 0) {
				if (content.indexOf('归属地') == -1) {
					if (content.length > 35) {
						content = content.replace(/[\\.！!？\\?;；]/, '。');
						var i = content.indexOf('。');
						if (i > 0)
							content = content.substr(0, i);
						else {
							content = content.replace(/[,、]/, '，');
							i = content.indexOf('，');
							if (i > 0)
								content = content.substr(0, i);
						}
					}
					mic0.playSound(content, synthAddr);
				}
			}
		});
	});

	$client.on('messagerendered', function(msg) {
		if (uinfoStore) {
			var divs = $('div .chat-segment');
			if (divs.length >= 1) {
				var val = divs[divs.length - 1].outerHTML;
				if (divs.length > 1)
					val = divs[divs.length - 2].outerHTML + val;
				uinfoStore.set('latestQA', val, new Date().getTime() + 3600 * 1000 * 6);
			}
			uinfoStore.set('robotName', $client.config.robotName, new Date().getTime() + 3600 * 1000 * 6)
		}
	});
};

for ( var i = 0; i < __packages.length; i++) {
	document.write('<script type="text/javascript" src="' + __packages[i] + '"></script>');
}

function getxhr() {
	var _xhr = false;
	try {
		_xhr = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e) {
		try {
			_xhr = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (e2) {
			_xhr = false;
		}
	}
	if (!_xhr && window.XMLHttpRequest)
		_xhr = new XMLHttpRequest();
	return _xhr;
}

function http_get(url) {
	var xhr = getxhr();
	xhr.open('GET', url, false);
	xhr.send();
	return xhr.responseText;
}

window.onload = function() {
	var ret = http_get('../../../ask?question=testSimp2Tran');
	var userTradVer = 'true' == ret;
	var imgs = document.getElementsByTagName('img');
	if (imgs && imgs.length) {
		for ( var i = 0; i < imgs.length; i++) {
			if (imgs[i].className && 'has_traditional' == imgs[i].className) {
				var imgSrc = imgs[i].getAttribute('src0');
				if (userTradVer) {
					var idx = imgSrc.lastIndexOf('.');
					imgs[i].src = imgSrc.substring(0, idx) + '_traditional' + imgSrc.substring(idx);
				} else {
					imgs[i].src = imgSrc;
				}
			}
		}
	}
	
	var _script = document.createElement('script');
	_script.src = '../../../ask?question=robotWebJslib';
	document.getElementsByTagName('head')[0].appendChild(_script);
};