$(function() {
	$('#submitButton').click(function() {
		var typeRadios = $('input[name=type]:checked');
		var reasonRadios = $('input[name=reason]:checked');
		if (typeRadios.length == 0) {
			alert('请选择您对本次服务的评价！');
			return false;
		}
		if ($('#reasonDiv').css('display') == 'block') {
			if (reasonRadios.length == 0) {
				alert('请选择您觉得不满意的原因！');
				return false;
			}
		}
		var contentEle = $('textarea[name=content]');
		var params = {
			content : contentEle.val(),
			type : typeRadios.val(),
			reason : reasonRadios.val(),
			platform : 'web',
			sessionId : parent.$client.getSession().getSessionId(),
			async : true
		};
		var dims = parent.$client.config.dims;
		if (dims) {
			for ( var key in dims) {
				params[key] = dims[key];
			}
		}
		$.post('feedback.action', params, function() {
			$("#dialog").dialog({
				height : 100,
				resizable : false,
				modal : true
			});
			contentEle.val('');
		});
	});

	$('input[name=type]').click(function() {
		if (this.value == '4' || this.value == '5') {
			$('#reasonDiv').css('display', 'block');
		} else {
			$('#reasonDiv').css('display', 'none');
		}
	});
});