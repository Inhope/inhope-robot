<?xml version="1.0" encoding="utf-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta name="viewport" content="width=device-width;initial-scale=1.4;minimum-scale=1.0;maximum-scale=2.0" />
<meta name="MobileOptimized" content="240" />
<title>WAP DEMO</title>
<style type="text/css">
html,body,p,form,img {
	margin: 0;
	padding: 0;
	border: 0;
}
select,input,img {
	vertical-align: middle;
}
body {
	background: #fff;
	color: #000;
	font-size: 12px;
}
* {
	line-height: 21px;
}
a {
	color: #039;
	text-decoration: none;
}
a:hover,a:active,a:focus {
	color: #f00;
	text-decoration: none;
}
.wrap {
	width: 240px;
	margin: 0 auto;
	border-right: 10px solid #ecf5fc;
	border-left: 10px solid #ecf5fc;
	border-top: 2px solid #ecf5fc;
	border-bottom: 2px solid #ecf5fc;
}
</style>
</head>
<body>
<div class="wrap" style="height: 255px; overflow-y: scroll;">
	<h3 style="background: #ecf5fc; color: blue; margin: 0;">小i机器人智能问答</h3>
	<c:if test="${not empty responseContent}">
	<p>
	<span style="color:purple;font-weight:bolder;">我: </span>${requestContent }
	</p>
	<p>
	<span style="color:green;font-weight:bolder;">小i机器人: </span>${responseContent }
	</p>
	</c:if>
</div>
<form action="wap-demo.action" method="post">
<div class="wrap" style="background: #ecf5fc;">
	<div>请在下方输入框提问:</div>
	<textarea name="requestContent" rows="5" cols="27" style="width: 98%;" ></textarea><br/>
	<input type="submit" value="提交" />
</div>
</form>
</body>
</html>

