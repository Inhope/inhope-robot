<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />
<link type="text/css" rel="stylesheet" href="css/material.css" />
<link type="text/css" href="css/jplayer.blue.monday.css"
	rel="stylesheet" />
<title></title>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/jquery.jplayer.min.js"></script>
<%
	String videoId = request.getParameter("videoId");
%>
<script language="javascript">
	var url = '${param.url}';
	window.onload = function() {
		$("#jp_container_1").css("display", "block");
		// $("#jp_container_1").offset({"left":$(obj).offset().left,"top":$(obj).offset().top});
		if ($("#jquery_jplayer").attr("ready")) {
			setTimeout(function() {
				$("#jquery_jplayer").jPlayer("setMedia", {
					m4v : "video-msg!showVideo.action?url=" + url
				}).jPlayer("play");
			}, 500);
		} else {
			$("#jquery_jplayer").jPlayer({
				ready : function() {
					$("#jquery_jplayer").attr("ready", "ok");
					$(this).jPlayer("setMedia", {
						m4v : "video-msg!showVideo.action?url=" + url
					}).jPlayer("play");
				},
				ended : function() {
				},
				swfPath : "../js",
				wmode : "window",
				supplied : "m4v",
				solution : "flash,html"
			});
		}
	}
	function hidePlayer() {
		$("#jquery_jplayer").jPlayer("stop");
		$("#jp_container_1").css("display", "none");
	}
</script>
</head>
<body style="border:0;padding:0;margin:0;">
	<div id="jp_container_1" class="jp-video "
		style="display:none;position:absolute">
		<div class="jp-type-single">
			<div id="jquery_jplayer" class="jp-jplayer"></div>
			<div class="jp-gui">
				<div class="jp-video-play">
					<a href="javascript:;" class="jp-video-play-icon" tabindex="1">play</a>
				</div>
				<div class="jp-interface">
					<div class="jp-progress">
						<div class="jp-seek-bar">
							<div class="jp-play-bar"></div>
						</div>
					</div>
					<div class="jp-current-time"></div>
					<div class="jp-duration"></div>
					<div class="jp-controls-holder">
						<ul class="jp-controls">
							<li><a href="javascript:;" class="jp-play" tabindex="1">play</a>
							</li>
							<li><a href="javascript:;" class="jp-pause" tabindex="1">pause</a>
							</li>
							<li><a href="javascript:;" class="jp-stop" tabindex="1">stop</a>
							</li>
							<li><a href="javascript:;" class="jp-mute" tabindex="1"
								title="mute">mute</a>
							</li>
							<li><a href="javascript:;" class="jp-unmute" tabindex="1"
								title="unmute">unmute</a>
							</li>
							<li><a href="javascript:;" class="jp-volume-max"
								tabindex="1" title="max volume">max volume</a>
							</li>
						</ul>
						<div class="jp-volume-bar">
							<div class="jp-volume-bar-value"></div>
						</div>
						<ul class="jp-toggles">
							<li><a href="javascript:;" class="jp-full-screen"
								tabindex="1" title="full screen">full screen</a>
							</li>
							<li><a href="javascript:;" class="jp-restore-screen"
								tabindex="1" title="restore screen">restore screen</a>
							</li>
							<li><a href="javascript:;" class="jp-repeat" tabindex="1"
								title="repeat">repeat</a>
							</li>
							<li><a href="javascript:;" class="jp-repeat-off"
								tabindex="1" title="repeat off">repeat off</a>
							</li>
						</ul>
					</div>
					<!-- <div class="jp-title">
            <ul>
              <li>Big Buck Bunny Trailer</li>
            </ul>
          </div>
           -->
				</div>
			</div>
			<div class="jp-no-solution">
				<span>Update Required</span> To play the media you will need to
				either update your browser to a recent version or update your <a
					href="http://get.adobe.com/flashplayer/" target="_blank">Flash
					plugin</a>.
			</div>
		</div>
	</div>
</body>
</html>