function MessageFormat(font, size, styleCode, color, randomColor) {
	this.font = font;
	this.size = size;
	if (!color && randomColor)
		color = this.getRandomColor();
	this.styleCode = (styleCode ? this.conventStyleToCode(styleCode) : "")
	this.color = color
	this.ToggleStyle = function(styleCodeToggle) {
		var result = false
		if (styleCodeToggle.length == 1) {
			if (this.styleCode != null) {
				var index = this.styleCode.indexOf(styleCodeToggle)
				if (-1 == index) {
					result = true
					this.styleCode += styleCodeToggle
				} else
					this.styleCode = this.styleCode.substring(0, index)
							+ this.styleCode.substring(index + 1,
									this.styleCode.length)
			} else {
				result = true
				this.styleCode = styleCodeToggle
			}
		}
		return result
	}
	this.IsBold = function() {
		return (-1 != this.styleCode.indexOf("B"))
	}
	this.IsItalic = function() {
		return (-1 != this.styleCode.indexOf("I"))
	}
	this.IsStrikethrough = function() {
		return (-1 != this.styleCode.indexOf("S"))
	}
	this.IsUnderline = function() {
		return (-1 != this.styleCode.indexOf("U"))
	}
	this.ApplyFormat = function(str) {
		var f = "<span style=\""
		if (this.font != null)
			f += "font-family:" + MessageFormat.EntityReplace(this.font) + ";"
		if (this.color != null && this.color != 0)
			f += "color:" + MessageFormat.EntityReplace("#" + this.color) + ";"
		if (this.IsBold())
			f += "font-weight:bold;"
		if (this.IsItalic())
			f += "font-style:italic;"
		if (this.IsUnderline() || this.IsStrikethrough()) {
			f += "text-decoration:"
			if (this.IsUnderline())
				f += "underline "
			if (this.IsStrikethrough())
				f += "line-through"
			f += ";"
		}
		str = f + "\">" + str + "</span>"
		return str
	}
}

MessageFormat.randomColors = ["808080", "1d1b11", "17365d", "365f91", "943634",
		"76923c", "5f497a", "31849b", "e36c0a", "c00000", "ff0000", "00b050",
		"0070c0", "002060", "7030a0", "c0504d", "4f81bd", "8064a2", "404040"]
MessageFormat.prototype.getRandomColor = function() {
	return MessageFormat.randomColors[Math.floor(Math.random()
			* MessageFormat.randomColors.length)]
}
MessageFormat.entityList = {
	"<" : "&lt;",
	"\uff1c" : "&lt;",
	">" : "&gt;",
	"\uff1e" : "&gt;",
	"&" : "&amp;",
	"\uff06" : "&amp;",
	"\"" : "&quot;",
	"\uff02" : "&quot;",
	";" : "&#59;",
	" " : "&nbsp;"
}
MessageFormat.EntityReplace = function(str) {
	var result = ""
	if (str != null) {
		var len = str.length
		var i = 0
		while (i < len) {
			var j = i
			var e = this.entityList[str.charAt(j)]
			while (j < len && null == e) {
				j++
				e = this.entityList[str.charAt(j)]
			}
			result += str.substr(i, j - i)
			if (e != null) {
				result += e
				j++
			}
			i = j
		}
	}
	return result;
}
MessageFormat.prototype.conventStyleToCode = function(style) {
	var styleCode = "";
	switch (style) {
		case 1 :
			styleCode = "B";
			break
		case 2 :
			styleCode = "I";
			break
		case 4 :
			styleCode = "U";
			break
		case 8 :
			styleCode = "S";
			break
		case 3 :
			styleCode = "BI";
			break
		case 15 :
			styleCode = "BIUS";
			break
		default :
			styleCode = "";
	}
	return styleCode;
}

function Emoticons() {
	var keysArray = new Array();
	var emoticonsArray = new Array();
	var mapLength = 0;
	var EmoticonInfo = function(image, keys, length, description) {
		this.IsLeaf = true
		this.EmoticonToHtml = function() {
			return "<img style='display:inline;' src=\"" + image + "\">"
		}
		this.Length = function() {
			return length
		}
	}
	this.Insert = function(image, keys) {
		var htmlKeys = MessageFormat.EntityReplace(keys)
		var e = new EmoticonInfo(image, keys, htmlKeys.length, null)
		keysArray[mapLength] = htmlKeys
		emoticonsArray[mapLength] = e
		mapLength++
	}
	this.Replace = function(str) {
		var result = str
		var emotReg = /<[^<>]+>/g;
		var ret, buf = '', pos = 0;
		while ((ret = emotReg.exec(result)) != null) {
			var _b = result.substring(pos, ret.index);
			for (var i = 0; i < mapLength; i++) {
				var oldr = ""
				while (oldr != _b) {
					oldr = _b;
					_b = _b.replace(keysArray[i], "$emoticon" + i + "$");
				}
			}
			buf += _b;
			buf += ret[0];
			pos = ret.index + ret[0].length;
		}
		if (pos < result.length) {
			var _b = result.substr(pos);
			for (var i = 0; i < mapLength; i++) {
				var oldr = ""
				while (oldr != _b) {
					oldr = _b;
					_b = _b.replace(keysArray[i], "$emoticon" + i + "$");
				}
			}
			buf += _b;
		}
		result = buf;
		for (var i = 0; i < mapLength; i++) {
			var oldr = ""
			while (oldr != result) {
				oldr = result
				result = result.replace("$emoticon" + i + "$",
						emoticonsArray[i].EmoticonToHtml())
			}
		}
		return result
	}
	this.Contains = function(key) {
		for (var i = 0; i < keysArray.length; i++) {
			if (keysArray[i] == key) {
				return true
			}
		}
		return false
	}
	this.Size = function() {
		return mapLength
	}
}

var dateFormat = function() {
	var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g, timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g, timezoneClip = /[^-+\dA-Z]/g, pad = function(
			val, len) {
		val = String(val);
		len = len || 2;
		while (val.length < len)
			val = "0" + val;
		return val;
	};

	// Regexes and supporting functions are cached through closure
	return function(date, mask, utc) {
		var dF = dateFormat;

		// You can't provide utc if you skip other args (use the "UTC:" mask
		// prefix)
		if (arguments.length == 1
				&& Object.prototype.toString.call(date) == "[object String]"
				&& !/\d/.test(date)) {
			mask = date;
			date = undefined;
		}

		// Passing date through Date applies Date.parse, if necessary
		date = date ? new Date(date) : new Date;
		if (isNaN(date))
			throw SyntaxError("invalid date");

		mask = String(dF.masks[mask] || mask || dF.masks["default"]);

		// Allow setting the utc argument via the mask
		if (mask.slice(0, 4) == "UTC:") {
			mask = mask.slice(4);
			utc = true;
		}

		var _ = utc ? "getUTC" : "get", d = date[_ + "Date"](), D = date[_
				+ "Day"](), m = date[_ + "Month"](), y = date[_ + "FullYear"](), H = date[_
				+ "Hours"](), M = date[_ + "Minutes"](), s = date[_ + "Seconds"](), L = date[_
				+ "Milliseconds"](), o = utc ? 0 : date.getTimezoneOffset(), flags = {
			d : d,
			dd : pad(d),
			ddd : dF.i18n.dayNames[D],
			dddd : dF.i18n.dayNames[D + 7],
			m : m + 1,
			mm : pad(m + 1),
			mmm : dF.i18n.monthNames[m],
			mmmm : dF.i18n.monthNames[m + 12],
			yy : String(y).slice(2),
			yyyy : y,
			h : H % 12 || 12,
			hh : pad(H % 12 || 12),
			H : H,
			HH : pad(H),
			M : M,
			MM : pad(M),
			s : s,
			ss : pad(s),
			l : pad(L, 3),
			L : pad(L > 99 ? Math.round(L / 10) : L),
			t : H < 12 ? "a" : "p",
			tt : H < 12 ? "am" : "pm",
			T : H < 12 ? "A" : "P",
			TT : H < 12 ? "AM" : "PM",
			Z : utc ? "UTC" : (String(date).match(timezone) || [""]).pop()
					.replace(timezoneClip, ""),
			o : (o > 0 ? "-" : "+")
					+ pad(
							Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o)
									% 60, 4),
			S : ["th", "st", "nd", "rd"][d % 10 > 3
					? 0
					: (d % 100 - d % 10 != 10) * d % 10]
		};

		return mask.replace(token, function($0) {
					return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
				});
	};
}();

// Some common format strings
dateFormat.masks = {
	"default" : "ddd mmm dd yyyy HH:MM:ss",
	shortDate : "m/d/yy",
	mediumDate : "mmm d, yyyy",
	longDate : "mmmm d, yyyy",
	fullDate : "dddd, mmmm d, yyyy",
	shortTime : "h:MM TT",
	mediumTime : "h:MM:ss TT",
	longTime : "h:MM:ss TT Z",
	isoDate : "yyyy-mm-dd",
	isoTime : "HH:MM:ss",
	isoDateTime : "yyyy-mm-dd'T'HH:MM:ss",
	isoUtcDateTime : "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
	dayNames : ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sunday",
			"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
	monthNames : ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
			"Sep", "Oct", "Nov", "Dec", "January", "February", "March",
			"April", "May", "June", "July", "August", "September", "October",
			"November", "December"]
};

// For convenience...
Date.prototype.format = function(mask, utc) {
	return dateFormat(this, mask, utc);
};

var jscolor = {

	dir : _APP_IMAGE_PATH + 'jscolor/', // location of jscolor directory (leave
	// empty to autodetect)
	bindClass : 'color', // class name
	binding : false, // automatic binding via <input class="...">
	preloading : true, // use image preloading?

	install : function() {
		jscolor.addEvent(window, 'load', jscolor.init);
	},

	init : function() {
		if (jscolor.binding) {
			jscolor.bind();
		}
		if (jscolor.preloading) {
			jscolor.preload();
		}
	},

	getDir : function() {
		if (!jscolor.dir) {
			var detected = jscolor.detectDir();
			jscolor.dir = detected !== false ? detected : 'jscolor/';
		}
		return jscolor.dir;
	},

	detectDir : function() {
		var base = location.href;

		var e = document.getElementsByTagName('base');
		for (var i = 0; i < e.length; i += 1) {
			if (e[i].href) {
				base = e[i].href;
			}
		}

		var e = document.getElementsByTagName('script');
		for (var i = 0; i < e.length; i += 1) {
			if (e[i].src && /(^|\/)jscolor\.js([?#].*)?$/i.test(e[i].src)) {
				var src = new jscolor.URI(e[i].src);
				var srcAbs = src.toAbsolute(base);
				srcAbs.path = srcAbs.path.replace(/[^\/]+$/, ''); // remove
				// filename
				srcAbs.query = null;
				srcAbs.fragment = null;
				return srcAbs.toString();
			}
		}
		return false;
	},

	bind : function() {
		var matchClass = new RegExp('(^|\\s)(' + jscolor.bindClass
						+ ')\\s*(\\{[^}]*\\})?', 'i');
		var e = document.getElementsByTagName('input');
		for (var i = 0; i < e.length; i += 1) {
			var m;
			if (!e[i].color && e[i].className
					&& (m = e[i].className.match(matchClass))) {
				var prop = {};
				if (m[3]) {
					try {
						eval('prop=' + m[3]);
					} catch (eInvalidProp) {
					}
				}
				e[i].color = new jscolor.color(e[i], prop);
			}
		}
	},

	preload : function() {
		for (var fn in jscolor.imgRequire) {
			if (jscolor.imgRequire.hasOwnProperty(fn)) {
				jscolor.loadImage(fn);
			}
		}
	},

	images : {
		pad : [181, 101],
		sld : [16, 101],
		cross : [15, 15],
		arrow : [7, 11]
	},

	imgRequire : {},
	imgLoaded : {},

	requireImage : function(filename) {
		jscolor.imgRequire[filename] = true;
	},

	loadImage : function(filename) {
		if (!jscolor.imgLoaded[filename]) {
			jscolor.imgLoaded[filename] = new Image();
			jscolor.imgLoaded[filename].src = jscolor.getDir() + filename;
		}
	},

	fetchElement : function(mixed) {
		return typeof mixed === 'string'
				? document.getElementById(mixed)
				: mixed;
	},

	addEvent : function(el, evnt, func) {
		if (el.addEventListener) {
			el.addEventListener(evnt, func, false);
		} else if (el.attachEvent) {
			el.attachEvent('on' + evnt, func);
		}
	},

	fireEvent : function(el, evnt) {
		if (evnt == 'change') {
			if (el.color.onchange)
				el.color.onchange.call(el, el.value);
		}
		// if(!el) {
		// return;
		// }
		// if(document.createEvent) {
		// var ev = document.createEvent('HTMLEvents');
		// ev.initEvent(evnt, true, true);
		// el.dispatchEvent(ev);
		// } else if(document.createEventObject) {
		// var ev = document.createEventObject();
		// el.fireEvent('on'+evnt, ev);
		// } else if(el['on'+evnt]) { // alternatively use the traditional event
		// model (IE5)
		// el['on'+evnt]();
	},

	getElementPos : function(e) {
		var e1 = e, e2 = e;
		var x = 0, y = 0;
		if (e1.offsetParent) {
			do {
				x += e1.offsetLeft;
				y += e1.offsetTop;
			} while (e1 = e1.offsetParent);
		}
		while ((e2 = e2.parentNode) && e2.nodeName.toUpperCase() !== 'BODY') {
			x -= e2.scrollLeft;
			y -= e2.scrollTop;
		}
		return [x, y];
	},

	getElementSize : function(e) {
		return [e.offsetWidth, e.offsetHeight];
	},

	getRelMousePos : function(e) {
		var x = 0, y = 0;
		if (!e) {
			e = window.event;
		}
		if (typeof e.offsetX === 'number') {
			x = e.offsetX;
			y = e.offsetY;
		} else if (typeof e.layerX === 'number') {
			x = e.layerX;
			y = e.layerY;
		}
		return {
			x : x,
			y : y
		};
	},

	getViewPos : function() {
		if (typeof window.pageYOffset === 'number') {
			return [window.pageXOffset, window.pageYOffset];
		} else if (document.body
				&& (document.body.scrollLeft || document.body.scrollTop)) {
			return [document.body.scrollLeft, document.body.scrollTop];
		} else if (document.documentElement
				&& (document.documentElement.scrollLeft || document.documentElement.scrollTop)) {
			return [document.documentElement.scrollLeft,
					document.documentElement.scrollTop];
		} else {
			return [0, 0];
		}
	},

	getViewSize : function() {
		if (typeof window.innerWidth === 'number') {
			return [window.innerWidth, window.innerHeight];
		} else if (document.body
				&& (document.body.clientWidth || document.body.clientHeight)) {
			return [document.body.clientWidth, document.body.clientHeight];
		} else if (document.documentElement
				&& (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
			return [document.documentElement.clientWidth,
					document.documentElement.clientHeight];
		} else {
			return [0, 0];
		}
	},

	URI : function(uri) { // See RFC3986

		this.scheme = null;
		this.authority = null;
		this.path = '';
		this.query = null;
		this.fragment = null;

		this.parse = function(uri) {
			var m = uri
					.match(/^(([A-Za-z][0-9A-Za-z+.-]*)(:))?((\/\/)([^\/?#]*))?([^?#]*)((\?)([^#]*))?((#)(.*))?/);
			this.scheme = m[3] ? m[2] : null;
			this.authority = m[5] ? m[6] : null;
			this.path = m[7];
			this.query = m[9] ? m[10] : null;
			this.fragment = m[12] ? m[13] : null;
			return this;
		};

		this.toString = function() {
			var result = '';
			if (this.scheme !== null) {
				result = result + this.scheme + ':';
			}
			if (this.authority !== null) {
				result = result + '//' + this.authority;
			}
			if (this.path !== null) {
				result = result + this.path;
			}
			if (this.query !== null) {
				result = result + '?' + this.query;
			}
			if (this.fragment !== null) {
				result = result + '#' + this.fragment;
			}
			return result;
		};

		this.toAbsolute = function(base) {
			var base = new jscolor.URI(base);
			var r = this;
			var t = new jscolor.URI;

			if (base.scheme === null) {
				return false;
			}

			if (r.scheme !== null
					&& r.scheme.toLowerCase() === base.scheme.toLowerCase()) {
				r.scheme = null;
			}

			if (r.scheme !== null) {
				t.scheme = r.scheme;
				t.authority = r.authority;
				t.path = removeDotSegments(r.path);
				t.query = r.query;
			} else {
				if (r.authority !== null) {
					t.authority = r.authority;
					t.path = removeDotSegments(r.path);
					t.query = r.query;
				} else {
					if (r.path === '') { // TODO: == or === ?
						t.path = base.path;
						if (r.query !== null) {
							t.query = r.query;
						} else {
							t.query = base.query;
						}
					} else {
						if (r.path.substr(0, 1) === '/') {
							t.path = removeDotSegments(r.path);
						} else {
							if (base.authority !== null && base.path === '') { // TODO:
								// ==
								// or
								// ===
								// ?
								t.path = '/' + r.path;
							} else {
								t.path = base.path.replace(/[^\/]+$/, '')
										+ r.path;
							}
							t.path = removeDotSegments(t.path);
						}
						t.query = r.query;
					}
					t.authority = base.authority;
				}
				t.scheme = base.scheme;
			}
			t.fragment = r.fragment;

			return t;
		};

		function removeDotSegments(path) {
			var out = '';
			while (path) {
				if (path.substr(0, 3) === '../' || path.substr(0, 2) === './') {
					path = path.replace(/^\.+/, '').substr(1);
				} else if (path.substr(0, 3) === '/./' || path === '/.') {
					path = '/' + path.substr(3);
				} else if (path.substr(0, 4) === '/../' || path === '/..') {
					path = '/' + path.substr(4);
					out = out.replace(/\/?[^\/]*$/, '');
				} else if (path === '.' || path === '..') {
					path = '';
				} else {
					var rm = path.match(/^\/?[^\/]*/)[0];
					path = path.substr(rm.length);
					out = out + rm;
				}
			}
			return out;
		}

		if (uri) {
			this.parse(uri);
		}

	},

	/*
	 * Usage example: var myColor = new jscolor.color(myInputElement)
	 */

	color : function(target, prop) {

		this.required = true; // refuse empty values?
		this.adjust = true; // adjust value to uniform notation?
		this.hash = false; // prefix color with # symbol?
		this.caps = true; // uppercase?
		this.slider = true; // show the value/saturation slider?
		this.valueElement = target; // value holder
		this.styleElement = target; // where to reflect current color
		this.hsv = [0, 0, 1]; // read-only 0-6, 0-1, 0-1
		this.rgb = [1, 1, 1]; // read-only 0-1, 0-1, 0-1

		this.pickerOnfocus = true; // display picker on focus?
		this.pickerMode = 'HSV'; // HSV | HVS
		this.pickerPosition = 'bottom'; // left | right | top | bottom
		this.pickerButtonHeight = 20; // px
		this.pickerClosable = false;
		this.pickerCloseText = 'Close';
		this.pickerButtonColor = 'ButtonText'; // px
		this.pickerFace = 10; // px
		this.pickerFaceColor = 'ThreeDFace'; // CSS color
		this.pickerBorder = 1; // px
		this.pickerBorderColor = 'ThreeDHighlight ThreeDShadow ThreeDShadow ThreeDHighlight'; // CSS
		// color
		this.pickerInset = 1; // px
		this.pickerInsetColor = 'ThreeDShadow ThreeDHighlight ThreeDHighlight ThreeDShadow'; // CSS
		// color
		this.pickerZIndex = 10000;

		for (var p in prop) {
			if (prop.hasOwnProperty(p)) {
				this[p] = prop[p];
			}
		}

		this.hidePicker = function() {
			if (isPickerOwner()) {
				removePicker();
			}
		};

		this.showPicker = function() {
			if (!isPickerOwner()) {

				drawPicker(135, -110);
				return;

				var tp = jscolor.getElementPos(target); // target pos
				var ts = jscolor.getElementSize(target); // target size
				var vp = jscolor.getViewPos(); // view pos
				var vs = jscolor.getViewSize(); // view size
				var ps = getPickerDims(this); // picker size
				var a, b, c;
				switch (this.pickerPosition.toLowerCase()) {
					case 'left' :
						a = 1;
						b = 0;
						c = -1;
						break;
					case 'right' :
						a = 1;
						b = 0;
						c = 1;
						break;
					case 'top' :
						a = 0;
						b = 1;
						c = -1;
						break;
					default :
						a = 0;
						b = 1;
						c = 1;
						break;
				}
				var l = (ts[b] + ps[b]) / 2;
				var pp = [ // picker pos
						-vp[a] + tp[a] + ps[a] > vs[a] ? (-vp[a] + tp[a]
								+ ts[a] / 2 > vs[a] / 2
								&& tp[a] + ts[a] - ps[a] >= 0 ? tp[a] + ts[a]
								- ps[a] : tp[a]) : tp[a],
						-vp[b] + tp[b] + ts[b] + ps[b] - l + l * c > vs[b]
								? (-vp[b] + tp[b] + ts[b] / 2 > vs[b] / 2
										&& tp[b] + ts[b] - l - l * c >= 0
										? tp[b] + ts[b] - l - l * c
										: tp[b] + ts[b] - l + l * c)
								: (tp[b] + ts[b] - l + l * c >= 0 ? tp[b]
										+ ts[b] - l + l * c : tp[b] + ts[b] - l
										- l * c)];
				drawPicker(pp[a], pp[b]);
			}
		};

		this.importColor = function() {
			if (!valueElement) {
				this.exportColor();
			} else {
				if (!this.adjust) {
					if (!this.fromString(valueElement.value, leaveValue)) {
						styleElement.style.backgroundColor = styleElement.jscStyle.backgroundColor;
						styleElement.style.color = styleElement.jscStyle.color;
						this.exportColor(leaveValue | leaveStyle);
					}
				} else if (!this.required && /^\s*$/.test(valueElement.value)) {
					valueElement.value = '';
					styleElement.style.backgroundColor = styleElement.jscStyle.backgroundColor;
					styleElement.style.color = styleElement.jscStyle.color;
					this.exportColor(leaveValue | leaveStyle);

				} else if (this.fromString(valueElement.value)) {
					// OK
				} else {
					this.exportColor();
				}
			}
		};

		this.exportColor = function(flags) {
			if (!(flags & leaveValue) && valueElement) {
				var value = this.toString();
				if (this.caps) {
					value = value.toUpperCase();
				}
				if (this.hash) {
					value = '#' + value;
				}
				valueElement.value = value;
			}
			if (!(flags & leaveStyle) && styleElement) {
				// styleElement.style.backgroundColor =
				// '#'+this.toString();
				// styleElement.style.color =
				// 0.213 * this.rgb[0] +
				// 0.715 * this.rgb[1] +
				// 0.072 * this.rgb[2]
				// < 0.5 ? '#FFF' : '#000';

				styleElement.style.color = '#' + this.toString();
			}
			if (!(flags & leavePad) && isPickerOwner()) {
				redrawPad();
			}
			if (!(flags & leaveSld) && isPickerOwner()) {
				redrawSld();
			}
		};

		this.fromHSV = function(h, s, v, flags) { // null = don't change
			h < 0 && (h = 0) || h > 6 && (h = 6);
			s < 0 && (s = 0) || s > 1 && (s = 1);
			v < 0 && (v = 0) || v > 1 && (v = 1);
			this.rgb = HSV_RGB(h === null ? this.hsv[0] : (this.hsv[0] = h),
					s === null ? this.hsv[1] : (this.hsv[1] = s), v === null
							? this.hsv[2]
							: (this.hsv[2] = v));
			this.exportColor(flags);
		};

		this.fromRGB = function(r, g, b, flags) { // null = don't change
			r < 0 && (r = 0) || r > 1 && (r = 1);
			g < 0 && (g = 0) || g > 1 && (g = 1);
			b < 0 && (b = 0) || b > 1 && (b = 1);
			var hsv = RGB_HSV(r === null ? this.rgb[0] : (this.rgb[0] = r),
					g === null ? this.rgb[1] : (this.rgb[1] = g), b === null
							? this.rgb[2]
							: (this.rgb[2] = b));
			if (hsv[0] !== null) {
				this.hsv[0] = hsv[0];
			}
			if (hsv[2] !== 0) {
				this.hsv[1] = hsv[1];
			}
			this.hsv[2] = hsv[2];
			this.exportColor(flags);
		};

		this.fromString = function(hex, flags) {
			var m = hex.match(/^\W*([0-9A-F]{3}([0-9A-F]{3})?)\W*$/i);
			if (!m) {
				return false;
			} else {
				if (m[1].length === 6) { // 6-char notation
					this.fromRGB(parseInt(m[1].substr(0, 2), 16) / 255,
							parseInt(m[1].substr(2, 2), 16) / 255, parseInt(
									m[1].substr(4, 2), 16)
									/ 255, flags);
				} else { // 3-char notation
					this.fromRGB(parseInt(m[1].charAt(0) + m[1].charAt(0), 16)
									/ 255, parseInt(m[1].charAt(1)
											+ m[1].charAt(1), 16)
									/ 255, parseInt(m[1].charAt(2)
											+ m[1].charAt(2), 16)
									/ 255, flags);
				}
				return true;
			}
		};

		this.toString = function() {
			return ((0x100 | Math.round(255 * this.rgb[0])).toString(16)
					.substr(1)
					+ (0x100 | Math.round(255 * this.rgb[1])).toString(16)
							.substr(1) + (0x100 | Math.round(255 * this.rgb[2]))
					.toString(16).substr(1));
		};

		function RGB_HSV(r, g, b) {
			var n = Math.min(Math.min(r, g), b);
			var v = Math.max(Math.max(r, g), b);
			var m = v - n;
			if (m === 0) {
				return [null, 0, v];
			}
			var h = r === n ? 3 + (b - g) / m : (g === n ? 5 + (r - b) / m : 1
					+ (g - r) / m);
			return [h === 6 ? 0 : h, m / v, v];
		}

		function HSV_RGB(h, s, v) {
			if (h === null) {
				return [v, v, v];
			}
			var i = Math.floor(h);
			var f = i % 2 ? h - i : 1 - (h - i);
			var m = v * (1 - s);
			var n = v * (1 - s * f);
			switch (i) {
				case 6 :
				case 0 :
					return [v, n, m];
				case 1 :
					return [n, v, m];
				case 2 :
					return [m, v, n];
				case 3 :
					return [m, n, v];
				case 4 :
					return [n, m, v];
				case 5 :
					return [v, m, n];
			}
		}

		function removePicker() {
			delete jscolor.picker.owner;
			// document.getElementsByTagName('body')[0].removeChild(jscolor.picker.boxB);
			target.removeChild(jscolor.picker.boxB);
		}

		function drawPicker(x, y) {
			if (!jscolor.picker) {
				jscolor.picker = {
					box : document.createElement('div'),
					boxB : document.createElement('div'),
					pad : document.createElement('div'),
					padB : document.createElement('div'),
					padM : document.createElement('div'),
					sld : document.createElement('div'),
					sldB : document.createElement('div'),
					sldM : document.createElement('div'),
					btn : document.createElement('div'),
					btnS : document.createElement('span'),
					btnT : document.createTextNode(THIS.pickerCloseText)
				};
				for (var i = 0, segSize = 4; i < jscolor.images.sld[1]; i += segSize) {
					var seg = document.createElement('div');
					seg.style.height = segSize + 'px';
					seg.style.fontSize = '1px';
					seg.style.lineHeight = '0';
					jscolor.picker.sld.appendChild(seg);
				}
				jscolor.picker.sldB.appendChild(jscolor.picker.sld);
				jscolor.picker.box.appendChild(jscolor.picker.sldB);
				jscolor.picker.box.appendChild(jscolor.picker.sldM);
				jscolor.picker.padB.appendChild(jscolor.picker.pad);
				jscolor.picker.box.appendChild(jscolor.picker.padB);
				jscolor.picker.box.appendChild(jscolor.picker.padM);
				jscolor.picker.btnS.appendChild(jscolor.picker.btnT);
				jscolor.picker.btn.appendChild(jscolor.picker.btnS);
				jscolor.picker.box.appendChild(jscolor.picker.btn);
				jscolor.picker.boxB.appendChild(jscolor.picker.box);
			}

			var p = jscolor.picker;

			p.box.style.margin = '0';
			p.boxB.style.margin = '0';
			p.pad.style.margin = '0';
			p.padB.style.margin = '0';
			p.padM.style.margin = '0';
			p.sld.style.margin = '0';
			p.sldB.style.margin = '0';
			p.sldM.style.margin = '0';

			// controls interaction
			p.box.onmouseup = p.box.onmouseout = function() {
				target.focus();
			};
			p.box.onmousedown = function() {
				abortBlur = true;
			};
			p.box.onmousemove = function(e) {
				if (holdPad || holdSld) {
					holdPad && setPad(e);
					holdSld && setSld(e);
					if (document.selection) {
						document.selection.empty();
					} else if (window.getSelection) {
						window.getSelection().removeAllRanges();
					}
				}
			};
			p.padM.onmouseup = p.padM.onmouseout = function() {
				if (holdPad) {
					holdPad = false;
					jscolor.fireEvent(valueElement, 'change');
				}
			};
			p.padM.onmousedown = function(e) {
				holdPad = true;
				setPad(e);
			};
			p.sldM.onmouseup = p.sldM.onmouseout = function() {
				if (holdSld) {
					holdSld = false;
					jscolor.fireEvent(valueElement, 'change');
				}
			};
			p.sldM.onmousedown = function(e) {
				holdSld = true;
				setSld(e);
			};

			// picker
			var dims = getPickerDims(THIS);
			p.box.style.width = dims[0] + 'px';
			p.box.style.height = dims[1] + 'px';

			// picker border
			p.boxB.style.position = 'absolute';
			p.boxB.style.clear = 'both';
			p.boxB.style.left = x + 'px';
			p.boxB.style.top = y + 'px';
			p.boxB.style.zIndex = THIS.pickerZIndex;
			p.boxB.style.border = THIS.pickerBorder + 'px solid';
			p.boxB.style.borderColor = THIS.pickerBorderColor;
			p.boxB.style.background = THIS.pickerFaceColor;

			// pad image
			p.pad.style.width = jscolor.images.pad[0] + 'px';
			p.pad.style.height = jscolor.images.pad[1] + 'px';

			// pad border
			p.padB.style.position = 'absolute';
			p.padB.style.left = THIS.pickerFace + 'px';
			p.padB.style.top = THIS.pickerFace + 'px';
			p.padB.style.border = THIS.pickerInset + 'px solid';
			p.padB.style.borderColor = THIS.pickerInsetColor;

			// pad mouse area
			p.padM.style.position = 'absolute';
			p.padM.style.left = '0';
			p.padM.style.top = '0';
			p.padM.style.width = THIS.pickerFace + 2 * THIS.pickerInset
					+ jscolor.images.pad[0] + jscolor.images.arrow[0] + 'px';
			p.padM.style.height = p.box.style.height;
			p.padM.style.cursor = 'crosshair';

			// slider image
			p.sld.style.overflow = 'hidden';
			p.sld.style.width = jscolor.images.sld[0] + 'px';
			p.sld.style.height = jscolor.images.sld[1] + 'px';

			// slider border
			p.sldB.style.display = THIS.slider ? 'block' : 'none';
			p.sldB.style.position = 'absolute';
			p.sldB.style.right = THIS.pickerFace + 'px';
			p.sldB.style.top = THIS.pickerFace + 'px';
			p.sldB.style.border = THIS.pickerInset + 'px solid';
			p.sldB.style.borderColor = THIS.pickerInsetColor;

			// slider mouse area
			p.sldM.style.display = THIS.slider ? 'block' : 'none';
			p.sldM.style.position = 'absolute';
			p.sldM.style.right = '0';
			p.sldM.style.top = '0';
			p.sldM.style.width = jscolor.images.sld[0]
					+ jscolor.images.arrow[0] + THIS.pickerFace + 2
					* THIS.pickerInset + 'px';
			p.sldM.style.height = p.box.style.height;
			try {
				p.sldM.style.cursor = 'pointer';
			} catch (eOldIE) {
				p.sldM.style.cursor = 'hand';
			}

			// "close" button
			function setBtnBorder() {
				var insetColors = THIS.pickerInsetColor.split(/\s+/);
				var pickerOutsetColor = insetColors.length < 2
						? insetColors[0]
						: insetColors[1] + ' ' + insetColors[0] + ' '
								+ insetColors[0] + ' ' + insetColors[1];
				p.btn.style.borderColor = pickerOutsetColor;
			}
			p.btn.style.display = THIS.pickerClosable ? 'block' : 'none';
			p.btn.style.position = 'absolute';
			p.btn.style.left = THIS.pickerFace + 'px';
			p.btn.style.bottom = THIS.pickerFace + 'px';
			p.btn.style.padding = '0 15px';
			p.btn.style.height = '18px';
			p.btn.style.border = THIS.pickerInset + 'px solid';
			setBtnBorder();
			p.btn.style.color = THIS.pickerButtonColor;
			p.btn.style.font = '12px sans-serif';
			p.btn.style.textAlign = 'center';
			try {
				p.btn.style.cursor = 'pointer';
			} catch (eOldIE) {
				p.btn.style.cursor = 'hand';
			}
			p.btn.onmousedown = function() {
				THIS.hidePicker();
			};
			p.btnS.style.lineHeight = p.btn.style.height;

			// load images in optimal order
			switch (modeID) {
				case 0 :
					var padImg = 'hs.png';
					break;
				case 1 :
					var padImg = 'hv.png';
					break;
			}
			p.padM.style.backgroundImage = "url('" + jscolor.getDir()
					+ "cross.gif')";
			p.padM.style.backgroundRepeat = "no-repeat";
			p.sldM.style.backgroundImage = "url('" + jscolor.getDir()
					+ "arrow.gif')";
			p.sldM.style.backgroundRepeat = "no-repeat";
			p.pad.style.backgroundImage = "url('" + jscolor.getDir() + padImg
					+ "')";
			p.pad.style.backgroundRepeat = "no-repeat";
			p.pad.style.backgroundPosition = "0 0";

			// place pointers
			redrawPad();
			redrawSld();

			jscolor.picker.owner = THIS;
			// document.getElementsByTagName('body')[0].appendChild(p.boxB);
			target.appendChild(p.boxB);
		}

		function getPickerDims(o) {
			var dims = [
					2
							* o.pickerInset
							+ 2
							* o.pickerFace
							+ jscolor.images.pad[0]
							+ (o.slider ? 2 * o.pickerInset + 2
									* jscolor.images.arrow[0]
									+ jscolor.images.sld[0] : 0),
					o.pickerClosable ? 4 * o.pickerInset + 3 * o.pickerFace
							+ jscolor.images.pad[1] + o.pickerButtonHeight : 2
							* o.pickerInset + 2 * o.pickerFace
							+ jscolor.images.pad[1]];
			return dims;
		}

		function redrawPad() {
			// redraw the pad pointer
			switch (modeID) {
				case 0 :
					var yComponent = 1;
					break;
				case 1 :
					var yComponent = 2;
					break;
			}
			var x = Math.round((THIS.hsv[0] / 6) * (jscolor.images.pad[0] - 1));
			var y = Math.round((1 - THIS.hsv[yComponent])
					* (jscolor.images.pad[1] - 1));
			jscolor.picker.padM.style.backgroundPosition = (THIS.pickerFace
					+ THIS.pickerInset + x - Math.floor(jscolor.images.cross[0]
					/ 2))
					+ 'px '
					+ (THIS.pickerFace + THIS.pickerInset + y - Math
							.floor(jscolor.images.cross[1] / 2)) + 'px';

			// redraw the slider image
			var seg = jscolor.picker.sld.childNodes;

			switch (modeID) {
				case 0 :
					var rgb = HSV_RGB(THIS.hsv[0], THIS.hsv[1], 1);
					for (var i = 0; i < seg.length; i += 1) {
						seg[i].style.backgroundColor = 'rgb('
								+ (rgb[0] * (1 - i / seg.length) * 100) + '%,'
								+ (rgb[1] * (1 - i / seg.length) * 100) + '%,'
								+ (rgb[2] * (1 - i / seg.length) * 100) + '%)';
					}
					break;
				case 1 :
					var rgb, s, c = [THIS.hsv[2], 0, 0];
					var i = Math.floor(THIS.hsv[0]);
					var f = i % 2 ? THIS.hsv[0] - i : 1 - (THIS.hsv[0] - i);
					switch (i) {
						case 6 :
						case 0 :
							rgb = [0, 1, 2];
							break;
						case 1 :
							rgb = [1, 0, 2];
							break;
						case 2 :
							rgb = [2, 0, 1];
							break;
						case 3 :
							rgb = [2, 1, 0];
							break;
						case 4 :
							rgb = [1, 2, 0];
							break;
						case 5 :
							rgb = [0, 2, 1];
							break;
					}
					for (var i = 0; i < seg.length; i += 1) {
						s = 1 - 1 / (seg.length - 1) * i;
						c[1] = c[0] * (1 - s * f);
						c[2] = c[0] * (1 - s);
						seg[i].style.backgroundColor = 'rgb('
								+ (c[rgb[0]] * 100) + '%,' + (c[rgb[1]] * 100)
								+ '%,' + (c[rgb[2]] * 100) + '%)';
					}
					break;
			}
		}

		function redrawSld() {
			// redraw the slider pointer
			switch (modeID) {
				case 0 :
					var yComponent = 2;
					break;
				case 1 :
					var yComponent = 1;
					break;
			}
			var y = Math.round((1 - THIS.hsv[yComponent])
					* (jscolor.images.sld[1] - 1));
			jscolor.picker.sldM.style.backgroundPosition = '0 '
					+ (THIS.pickerFace + THIS.pickerInset + y - Math
							.floor(jscolor.images.arrow[1] / 2)) + 'px';
		}

		function isPickerOwner() {
			return jscolor.picker && jscolor.picker.owner === THIS;
		}

		function blurTarget() {
			if (valueElement === target) {
				THIS.importColor();
			}
			if (THIS.pickerOnfocus) {
				THIS.hidePicker();
			}
		}

		function blurValue() {
			if (valueElement !== target) {
				THIS.importColor();
			}
		}

		function setPad(e) {
			var mpos = jscolor.getRelMousePos(e);
			var x = mpos.x - THIS.pickerFace - THIS.pickerInset;
			var y = mpos.y - THIS.pickerFace - THIS.pickerInset;
			switch (modeID) {
				case 0 :
					THIS.fromHSV(x * (6 / (jscolor.images.pad[0] - 1)), 1 - y
									/ (jscolor.images.pad[1] - 1), null,
							leaveSld);
					break;
				case 1 :
					THIS
							.fromHSV(x * (6 / (jscolor.images.pad[0] - 1)),
									null, 1 - y / (jscolor.images.pad[1] - 1),
									leaveSld);
					break;
			}
		}

		function setSld(e) {
			var mpos = jscolor.getRelMousePos(e);
			var y = mpos.y - THIS.pickerFace - THIS.pickerInset;
			switch (modeID) {
				case 0 :
					THIS.fromHSV(null, null, 1 - y
									/ (jscolor.images.sld[1] - 1), leavePad);
					break;
				case 1 :
					THIS.fromHSV(null, 1 - y / (jscolor.images.sld[1] - 1),
							null, leavePad);
					break;
			}
		}

		var THIS = this;
		var modeID = this.pickerMode.toLowerCase() === 'hvs' ? 1 : 0;
		var abortBlur = false;
		var valueElement = jscolor.fetchElement(this.valueElement), styleElement = jscolor
				.fetchElement(this.styleElement);
		var holdPad = false, holdSld = false;
		var leaveValue = 1 << 0, leaveStyle = 1 << 1, leavePad = 1 << 2, leaveSld = 1 << 3;

		// target
		jscolor.addEvent(target, 'focus', function() {
					if (THIS.pickerOnfocus) {
						THIS.showPicker();
					}
				});
		jscolor.addEvent(target, 'blur', function() {
					if (!abortBlur) {
						window.setTimeout(function() {
									abortBlur || blurTarget();
									abortBlur = false;
								}, 0);
					} else {
						abortBlur = false;
					}
				});

		// valueElement
		if (valueElement) {
			var updateField = function() {
				THIS.fromString(valueElement.value, leaveValue);
			};
			jscolor.addEvent(valueElement, 'keyup', updateField);
			jscolor.addEvent(valueElement, 'input', updateField);
			jscolor.addEvent(valueElement, 'blur', blurValue);
			valueElement.setAttribute('autocomplete', 'off');
		}

		// styleElement
		if (styleElement) {
			styleElement.jscStyle = {
				backgroundColor : styleElement.style.backgroundColor,
				color : styleElement.style.color
			};
		}

		// require images
		switch (modeID) {
			case 0 :
				jscolor.requireImage('hs.png');
				break;
			case 1 :
				jscolor.requireImage('hv.png');
				break;
		}
		jscolor.requireImage('cross.gif');
		jscolor.requireImage('arrow.gif');

		this.importColor();
	}

};
jscolor.install();

(function($) {
	var daysInWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday",
			"Friday", "Saturday"];
	var shortMonthsInYear = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
			"Aug", "Sep", "Oct", "Nov", "Dec"];
	var longMonthsInYear = ["January", "February", "March", "April", "May",
			"June", "July", "August", "September", "October", "November",
			"December"];
	var shortMonthsToNumber = [];
	shortMonthsToNumber["Jan"] = "01";
	shortMonthsToNumber["Feb"] = "02";
	shortMonthsToNumber["Mar"] = "03";
	shortMonthsToNumber["Apr"] = "04";
	shortMonthsToNumber["May"] = "05";
	shortMonthsToNumber["Jun"] = "06";
	shortMonthsToNumber["Jul"] = "07";
	shortMonthsToNumber["Aug"] = "08";
	shortMonthsToNumber["Sep"] = "09";
	shortMonthsToNumber["Oct"] = "10";
	shortMonthsToNumber["Nov"] = "11";
	shortMonthsToNumber["Dec"] = "12";

	$.format = (function() {
		function strDay(value) {
			return daysInWeek[parseInt(value, 10)] || value;
		}

		function strMonth(value) {
			var monthArrayIndex = parseInt(value, 10) - 1;
			return shortMonthsInYear[monthArrayIndex] || value;
		}

		function strLongMonth(value) {
			var monthArrayIndex = parseInt(value, 10) - 1;
			return longMonthsInYear[monthArrayIndex] || value;
		}

		var parseMonth = function(value) {
			return shortMonthsToNumber[value] || value;
		};

		var parseTime = function(value) {
			var retValue = value;
			var millis = "";
			if (retValue.indexOf(".") !== -1) {
				var delimited = retValue.split('.');
				retValue = delimited[0];
				millis = delimited[1];
			}

			var values3 = retValue.split(":");

			if (values3.length === 3) {
				hour = values3[0];
				minute = values3[1];
				second = values3[2];

				return {
					time : retValue,
					hour : hour,
					minute : minute,
					second : second,
					millis : millis
				};
			} else {
				return {
					time : "",
					hour : "",
					minute : "",
					second : "",
					millis : ""
				};
			}
		};

		return {
			date : function(value, format) {
				/*
				 * value = new java.util.Date() 2009-12-18 10:54:50.546
				 */
				try {
					var date = null;
					var year = null;
					var month = null;
					var dayOfMonth = null;
					var dayOfWeek = null;
					var time = null;
					if (typeof value.getFullYear === "function") {
						year = value.getFullYear();
						month = value.getMonth() + 1;
						dayOfMonth = value.getDate();
						dayOfWeek = value.getDay();
						time = parseTime(value.toTimeString());
					} else if (value
							.search(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.?\d{0,3}[-+]?\d{2}:?\d{2}/) != -1) { /* 2009-04-19T16:11:05+02:00 */
						var values = value.split(/[T\+-]/);
						year = values[0];
						month = values[1];
						dayOfMonth = values[2];
						time = parseTime(values[3].split(".")[0]);
						date = new Date(year, month - 1, dayOfMonth);
						dayOfWeek = date.getDay();
					} else {
						var values = value.split(" ");
						switch (values.length) {
							case 6 :
								/* Wed Jan 13 10:43:41 CET 2010 */
								year = values[5];
								month = parseMonth(values[1]);
								dayOfMonth = values[2];
								time = parseTime(values[3]);
								date = new Date(year, month - 1, dayOfMonth);
								dayOfWeek = date.getDay();
								break;
							case 2 :
								/* 2009-12-18 10:54:50.546 */
								var values2 = values[0].split("-");
								year = values2[0];
								month = values2[1];
								dayOfMonth = values2[2];
								time = parseTime(values[1]);
								date = new Date(year, month - 1, dayOfMonth);
								dayOfWeek = date.getDay();
								break;
							case 7 :
								/* Tue Mar 01 2011 12:01:42 GMT-0800 (PST) */
							case 9 :
								/*
								 * added by Larry, for Fri Apr 08 2011 00:00:00
								 * GMT+0800 (China Standard Time)
								 */
							case 10 :
								/*
								 * added by Larry, for Fri Apr 08 2011 00:00:00
								 * GMT+0200 (W. Europe Daylight Time)
								 */
								year = values[3];
								month = parseMonth(values[1]);
								dayOfMonth = values[2];
								time = parseTime(values[4]);
								date = new Date(year, month - 1, dayOfMonth);
								dayOfWeek = date.getDay();
								break;
							default :
								return value;
						}
					}

					var pattern = "";
					var retValue = "";
					/*
					 * Issue 1 - variable scope issue in format.date Thanks
					 * jakemonO
					 */
					for (var i = 0; i < format.length; i++) {
						var currentPattern = format.charAt(i);
						pattern += currentPattern;
						switch (pattern) {
							case "ddd" :
								retValue += strDay(dayOfWeek);
								pattern = "";
								break;
							case "dd" :
								if (format.charAt(i + 1) == "d") {
									break;
								}
								if (String(dayOfMonth).length === 1) {
									dayOfMonth = '0' + dayOfMonth;
								}
								retValue += dayOfMonth;
								pattern = "";
								break;
							case "MMMM" :
								retValue += strLongMonth(month);
								pattern = "";
								break;
							case "MMM" :
								if (format.charAt(i + 1) === "M") {
									break;
								}
								retValue += strMonth(month);
								pattern = "";
								break;
							case "MM" :
								if (format.charAt(i + 1) == "M") {
									break;
								}
								if (String(month).length === 1) {
									month = '0' + month;
								}
								retValue += month;
								pattern = "";
								break;
							case "yyyy" :
								retValue += year;
								pattern = "";
								break;
							case "yy" :
								if (format.charAt(i + 1) == "y"
										&& format.charAt(i + 2) == "y") {
									break;
								}
								retValue += String(year).slice(-2);
								pattern = "";
								break;
							case "HH" :
								retValue += time.hour;
								pattern = "";
								break;
							case "hh" :
								/*
								 * time.hour is "00" as string == is used
								 * instead of ===
								 */
								var hour = (time.hour == 0
										? 12
										: time.hour < 13
												? time.hour
												: time.hour - 12);
								hour = String(hour).length == 1
										? '0' + hour
										: hour;
								retValue += hour;
								pattern = "";
								break;
							case "h" :
								if (format.charAt(i + 1) == "h") {
									break;
								}
								var hour = (time.hour == 0
										? 12
										: time.hour < 13
												? time.hour
												: time.hour - 12);
								retValue += hour;
								pattern = "";
								break;
							case "mm" :
								retValue += time.minute;
								pattern = "";
								break;
							case "ss" :
								/*
								 * ensure only seconds are added to the return
								 * string
								 */
								retValue += time.second.substring(0, 2);
								pattern = "";
								break;
							case "SSS" :
								retValue += time.millis.substring(0, 3);
								pattern = "";
								break;
							case "a" :
								retValue += time.hour >= 12 ? "PM" : "AM";
								pattern = "";
								break;
							case " " :
								retValue += currentPattern;
								pattern = "";
								break;
							case "/" :
								retValue += currentPattern;
								pattern = "";
								break;
							case ":" :
								retValue += currentPattern;
								pattern = "";
								break;
							default :
								if (pattern.length === 2
										&& pattern.indexOf("y") !== 0
										&& pattern != "SS") {
									retValue += pattern.substring(0, 1);
									pattern = pattern.substring(1, 2);
								} else if ((pattern.length === 3 && pattern
										.indexOf("yyy") === -1)) {
									pattern = "";
								}
						}
					}
					return retValue;
				} catch (e) {
					return value;
				}
			}
		};
	}());
}(jQuery));

$(document).ready(function() {
	$(".shortDateFormat").each(function(idx, elem) {
				if ($(elem).is(":input")) {
					$(elem).val($.format.date($(elem).val(), "dd/MM/yyyy"));
				} else {
					$(elem).text($.format.date($(elem).text(), "dd/MM/yyyy"));
				}
			});
	$(".longDateFormat").each(function(idx, elem) {
				if ($(elem).is(":input")) {
					$(elem).val($.format.date($(elem).val(),
							"dd/MM/yyyy hh:mm:ss"));
				} else {
					$(elem).text($.format.date($(elem).text(),
							"dd/MM/yyyy hh:mm:ss"));
				}
			});
});