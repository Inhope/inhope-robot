<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="Cache-Control" content="no-cache,   must-revalidate" />
	<meta http-equiv="expires" content="0" />
	<style>
	body {
	    background-color: transparent;
	    color: #333333;
	    font-family: Verdana,Arial,'宋体';
	    font-size: 11px;
	    line-height: 18px;
	    margin: 20px 0 0;
	    overflow-y: auto;
	}
	</style>
	<link rel="stylesheet" type="text/css" href="../js/jquery-ui/jquery-ui.css" />
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script type="text/javascript" src="../js/jquery-ui/jquery-ui.js"></script>
	<script type="text/javascript">
		$(function(){
			$("#exitBtn").click(function(){
				var cli = window.top.$client;
				if(cli){
					cli.external.acsBye();
					$("#dialog").dialog({height:100, resizable:false, modal:true});
					$(this).hide();
					$('#hint').html('您已退出人工客服系统，欢迎再次使用！');
				}
			});
		})
	</script>
</head>
<body>
	<div style="color: #333333;font-size: 12px;margin: 20px;">
		<h3
			style="color: #333; font-size: 15px; border: 0; margin: 0; padding: 0; line-height: 36px;text-align: center;">
			人工客服
		</h3><center>
			<p id="hint" style="margin:1 0">
				Hi~ 欢迎使用人工客服系统！
			</p></center>
			
			<center>
				<input id="exitBtn" type="button" value="点此退出人工客服" />
			</center>
		<!-- </form> -->
	</div>
	<div style="display: none;" id="dialog" title="退出成功">
		<p>感谢您的使用人工客服系统，祝您愉快！</p>
	</div>
</body>
</html>