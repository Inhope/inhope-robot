<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.eastrobot.robotbase.util.FileServiceImpl"%>
<link type="text/css" href="../../css/p4fold.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/mootools-1.2.4-core.js"></script>
<script type="text/javascript" src="../../js/mootools-1.2.4.2-more.js"></script>
<script type="text/javascript" src="../../js/p4fold.js"></script>
<%
request.setCharacterEncoding("UTF-8");

//当前P4设置信息
String p4id = request.getParameter("p4id") != null ? request.getParameter("p4id") : "";
String p4content = null;
String p4name = null;

String savePath = pageContext.getServletContext().getRealPath("/") + "/p4data/"+p4id+"/";
String fileName = "index.html";
String p4Content = FileServiceImpl.readFile(savePath+fileName);
if(p4Content.indexOf("<!--")==0){
	String paramContent = p4Content.substring(0,p4Content.indexOf("-->"));
	
	paramContent = paramContent.replaceAll("<!--","");
	paramContent = paramContent.replaceAll("-->","");
	String[] paramContents = null;
	if(paramContent!=null&&paramContent.trim().length()>0){
		paramContents = paramContent.split("\\|");
		p4name = paramContents[0];
	}
	p4content = p4Content.substring(p4Content.indexOf(">")+1);
}
%>


<html>
<head>
	<meta charset="utf-8" />
	<!--用户自定义CSS引入-->
	<style type="text/css">
		body{margin:0; background:#fff; overflow:auto;}
		*{margin:0; padding:0; font-family:Tahoma, Verdana, Georgia; font-size:12px;}
	</style>
</head>
<body>

<div style="padding: 2px 2px 2px 2px; height: 100%; width:100%;">
	<%
		int j =0;
		int fromIndex = 0;
		while(true){
			int indexof = p4content.indexOf("<p><strong>",fromIndex);
			if(indexof>-1){
				j++;
				fromIndex = indexof+"<p><strong>".length();
			}else{
				break;
			}
		}
	%>
	<%
		if(j>1){%>
			<div id="selectAll" onclick="P4Fold.selectAllTopic();" style="cursor: pointer;float: right;padding: 0px 20px 0px 0px;">全部打开</div>
			<br>
			<%
			p4content = p4content.replaceAll("<p.*?>(.*?)<strong.*?>","<p><strong>");
			p4content = p4content.replaceAll("</.*?strong.*?>(.*?)</.*?p>","</strong></p>");
			StringBuilder sb = new StringBuilder();
			int i=0;
			while(true){
				i++;
				if(p4content.indexOf("<p><strong>")>-1){
					int headLen = p4content.indexOf("</strong></p>");
					int strongplen = "</strong></p>".length();
					String head = p4content.substring(0,headLen)+"</strong></p>";
					p4content = p4content.substring(headLen+strongplen);
					
					int headlineContnetLen = p4content.indexOf("<p><strong>");
					String content = "";
					if(headlineContnetLen>-1){
						content = p4content.substring(0,headlineContnetLen);
						p4content = p4content.substring(headlineContnetLen);
					}else{
						content = p4content;
					}
					
					
				%>
				<div id="headline<%=i %>" class="headline" onclick="P4Fold.selectTopic('headlineContnet<%=i %>')">
					<%=head %>
				</div>
				<div id="headlineContnet<%=i %>" class="headlineContnet"><%=content %></div>
				<%
				}else{
					break;
				}
			}
			%>
	<%
		}else{
	%>
	<%=p4content%>
	<% }%>
</div>
	
</body>
</html>
<%!
private String htmlspecialchars(String str) {
	str = str.replaceAll("&", "&amp;");
	str = str.replaceAll("<", "&lt;");
	str = str.replaceAll(">", "&gt;");
	str = str.replaceAll("\"", "&quot;");
	return str;
}
%>