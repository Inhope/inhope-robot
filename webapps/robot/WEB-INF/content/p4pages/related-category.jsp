<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.Map"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
	.title {
		color: #38608E;
		font: 14px 宋体;
		font-weight: bold;
	}
	.question_list {
		margin:5px 20px;
		padding:0;
	}
	.question {
		color: #38608E;
		font: 13px 宋体;
		cursor : pointer;
		height:auto;
		line-height:20px;
	}
</style>
<title>相关政策</title> 
</head>
<body>
	<div>
		<form action="related-category!search.action" method="post">
			<input type="text" name="name" value="${name}"/> 
			<input type="submit" value="搜索 " />
		</form>
	</div>
	<br />
	<div class="title">
		<c:if test="${empty pathNodes}">
			全部 
		</c:if>
		<c:if test="${not empty pathNodes}">
			<a href="related-category.action">全部 </a>
			<c:forEach varStatus="vs" var="node" items="${pathNodes}">
				 > 
				 <c:choose>
				 <c:when test="${vs.last}">
				 	${node.name}
				 </c:when>
				 <c:otherwise>
				 	<a href="related-category.action?nodetype=${node.type}&nodeid=${node.id}">${node.name}</a>
				 </c:otherwise>
				 </c:choose>
			</c:forEach>
		</c:if>
	</div>
	<ul class="question_list">
	<c:forEach var="cnode" items="${childNodes}">
		<li class="question">
			<c:choose>
			<c:when test="${cnode.type == 3}">
				<a id="${cnode.id}" href="#" onclick="window.parent.$client.sendTextEx('${cnode.name}')">${cnode.name}</a>
			</c:when>
			<c:when test="${cnode.type == 1 && fn:contains(cnode.bh, 'L')}">
				<a id="${cnode.id}" href="related-category.action?listByCategory=true&nodetype=${cnode.type}&nodeid=${cnode.id}">${cnode.name}</a>
			</c:when>
			<c:otherwise>
				<a id="${cnode.id}" href="related-category.action?nodetype=${cnode.type}&nodeid=${cnode.id}">${cnode.name}</a>
			</c:otherwise>
			</c:choose>
		</li>
	</c:forEach>
	</ul>
</body>
</html>
