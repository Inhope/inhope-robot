$(function() {
	function isEmail(s) {
		var pattern = /^[a-zA-Z0-9_\-\.]{1,}@[a-zA-Z0-9_\-]{1,}\.[a-zA-Z0-9_\-.]{1,}$/;
		if (!pattern.exec(s))
			return false;
		return true;
	}
	function isMobile(s) {
		var reg0 = /^13\d{5,9}$/;
		var reg1 = /^153\d{4,8}$/;
		var reg2 = /^159\d{4,8}$/;
		var reg3 = /^1\d{10}$/;
		var ret = false;
		if (reg0.test(s))
			ret = true;
		if (reg1.test(s))
			ret = true;
		if (reg2.test(s))
			ret = true;
		if (reg3.test(s))
			ret = true;
		return ret;
	}
	$('select[name=type]').change(function(v) {
				if (this.value == '1') {
					$('#labelAddr').html('您的Email')
				} else if (this.value == '2') {
					$('#labelAddr').html('您的手机号码')
				}
			});
	$('#submitButton').click(function() {
				var addrEle = $('input[name=addr]');
				var contentEle = $('textarea[name=content]');
				var nameEle = $('input[name=username]');
				var type = $('select').val();
				var addr = addrEle.val();
				var content = contentEle.val();
				if ($('input[name=addr]').val().length == 0) {
					alert("请填写回复地址")
					return false;
				}
				if (type == 1 && !isEmail(addr)) {
					alert('请正确填写Email');
					return false;
				}
				if (type == 2 && !isMobile(addr)) {
					alert('请正确填写手机号码');
					return false;
				}
				if ($('textarea[name=content]').val().length == 0) {
					alert("请填写留言内容")
					return false;
				}
				$.post('leaveword.action', {
							content : content,
							addr : addr,
							type : type,
							async : true
						}, function() {
							$("#dialog").dialog({
										height : 100,
										resizable : false,
										modal : true
									});
							addrEle.val('');
							nameEle.val('');
							contentEle.val('');
						});
			});
});