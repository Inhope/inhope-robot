package com.eastrobot.robotdev.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.Map.Entry;

import org.jasig.cas.client.authentication.AttributePrincipal;
import org.jasig.cas.client.util.AbstractCasFilter;
import org.jasig.cas.client.validation.Assertion;

import com.eastrobot.robotdev.utils.ManagerService;
import com.eastrobot.robotdev.utils.PropertiesUtil;

@Controller
@RequestMapping("/")
public class HomeController {
	private String managerUrl = PropertiesUtil.getValueByKey("manager.url");

	/**
	 * cas 参考
	 * http://blog.csdn.net/zrk1000/article/details/51168550
	 * http://www.micmiu.com/enterprise-app/sso/sso-cas-sample/
	 *
	 * 更多attribute
	 *
	 * http://zxs19861202.iteye.com/blog/890965
     */

	// 获取用户信息
	public Object getUser(HttpServletRequest request) {
		Assertion assertion = (Assertion) request.getSession().getAttribute(AbstractCasFilter.CONST_CAS_ASSERTION);  
		HashMap<String, Object> m = new HashMap<String, Object>();
//		m.put("Life", "haha");
		System.out.println("request:"+request);
		m.put("sessionId", request.getSession().getId());
		System.out.println("**"+request.getSession().getId()+"**"+request.getRemoteUser());
		// 获取登录后的用户信
		m.put("getRemoteUser", request.getRemoteUser());
		// 获取更多的信息
		/*
		{"name":"admin",
		"attributes":{"id":"6","remark":"用户备注","status":"1","email":"33977621232@qq.com","roles":"admin,premium","userName":"admin","realName":"小张","mobile":"18817350422"}}

		 roles是用户的角色, 一个用户有多个角色,用英文逗号分隔, admin表示是管理员
		 */
		//AttributePrincipal principal = assertion.getPrincipal();  
		AttributePrincipal principal = (AttributePrincipal) request.getUserPrincipal();
		Map attributes = principal.getAttributes();

		String userId = (String)attributes.get("id");
		String userName = (String)attributes.get("userName");
		String email = (String)attributes.get("email");
		String mobile = (String)attributes.get("mobile");
//		String roles = (String)attributes.get("roles");
		String roles = "2123*&saaadca";
		System.out.println("userId"+userId+"userName"+userName+"email"+email+"mobile"+mobile+"roles"+roles);
		
		if (roles.contains("admin")) {
			System.out.println("是管理员啊");
		} else {
			System.out.println("我不是管理员");
			
		}

		m.put("getUserPrincipal", principal );

		return m;
	}
	
	/**
	 * 测试 CAS
	 * @param request
	 * @return
	 */
	@RequestMapping("/login")
	//@ResponseBody
	public Object test(HttpServletRequest request) {
		//HashMap<String, Object> m = new HashMap<String, Object>();
		HashMap<String, Object> map=(HashMap<String, Object>)this.getUser(request);
		System.out.println(map.get("userName"));
		//return this.getUser(request);
		String userName=(String)map.get("userName");
		System.out.println("传入的username:"+userName);
		//String url=ManagerService.getManagerUrl(userName,"",managerUrl);
//		String url=ManagerService.getManagerUrl("paul","管理员","http://172.16.1.196:8280/manager/");
		String url=ManagerService.getManagerUrl(request.getRemoteUser(),"管理员",managerUrl);
//		System.out.println("跳转的url:"+url);
	return "redirect:"+url;
	}

	/**
	 * 注销
	 * @param request
	 * @return
	 */
	@RequestMapping("/logout")
	public String home(HttpServletRequest request) {
		// 先本地session清除
		// 清除session
		System.out.println("logout页面");
		Enumeration<String> em = request.getSession().getAttributeNames();
		while (em.hasMoreElements()) {
			request.getSession().removeAttribute(em.nextElement().toString());
		}
		// 重定向到sso的注销页, 并返回到本地主页
		//return "redirect:http://inhope.leanote.top:8080/sso/logout?service=http://localhost:8081";
		return "redirect:http://inhope.leanote.top:8080/sso/logout";
	}

}
