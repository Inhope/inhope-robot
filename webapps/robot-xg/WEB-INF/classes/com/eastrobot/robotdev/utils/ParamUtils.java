package com.eastrobot.robotdev.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.eastrobot.robotface.domain.UserAttribute;

public class ParamUtils {

	public static Map<String, String> list2Map(UserAttribute[] attrs) {
		Map<String, String> map = new HashMap<String, String>();
		for (UserAttribute attr : attrs) {
			map.put(attr.getName(), attr.getValue());
		}
		return map;
	}

	public static UserAttribute[] map2List(Map<String, String> map) {
		List<UserAttribute> list = new ArrayList<UserAttribute>();
		Set<String> set = map.keySet();
		for (String key : set) {
			UserAttribute attr = new UserAttribute(key, map.get(key));
			list.add(attr);
		}
		return list.toArray(new UserAttribute[] {});
	}

}
