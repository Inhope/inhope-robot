<%@ page contentType="text/html;charset=UTF-8"%>
<%!static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger("com.incesoft");%>
<%
Throwable t = (Throwable) request.getAttribute("exception");
logger.error("", t);
response.setStatus(500);
response.getOutputStream().write((t.getMessage()+"").getBytes("UTF-8"));
%>
