document.addEventListener("WeixinJSBridgeReady", function() {
			if (typeof(WeixinJSBridge) !== "undefined") {
				WeixinJSBridge.invoke('getNetworkType', {}, function(e) {
							WeixinJSBridge.log(e.err_msg);
							var network_type = e.err_msg.split(":")[1];
							if ("edge" == network_type) {
								var imagesSrc = [];
								var imgs = document.images;
								for (var index in imgs) {
									var img = imgs[index];
									if (img.src) {
										img.onclick = (function(x, img) {
											return function() {
												img.src = imagesSrc[x];
											}
										})(index, img);
										imagesSrc[index] = img.src;
										img.src = "../../images/defaultTitleHtml.jpg";
										img.style.display = "block";
									}
								}
							} else {
								var imags = document.images;
								for (var index in imags) {
									var img = imags[index];
									if (img.src) {
										img.style.display = "block";
									}
								}
							}
						});
			}
		}, false);