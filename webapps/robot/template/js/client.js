var $client = {
	config : {
		myName : '我',
		defaultContent : 'default',
		randomFontColor : true,
		isIE6 : navigator.appVersion.indexOf("MSIE 6") > 0
	},
	user : null,
	override : function(funcs, receiver) {
		var _receiver = receiver ? receiver : this;
		for (var funcName in funcs) {
			var originFunc = _receiver[funcName];
			_receiver[funcName] = funcs[funcName];
			if (!_receiver.raw)
				_receiver.raw = {};
			_receiver.raw[funcName] = originFunc;
		}
	},
	addDim : function(k, v) {
		if (!this.config.dims)
			this.config.dims = {};
		this.config.dims[k] = v;
	},
	getDims : function() {
		return this.config.dims;
	},
	registEl : function(elName, el) {
		if (typeof el == 'string')
			this.els[elName] = $('#' + el);
		else if (el instanceof jQuery)
			this.els[elName] = el;
		else
			this.els[elName] = $(el);
	},
	on : function(events, callback, scope) {
		if (!this.listeners)
			this.listeners = {};
		var eventNames = events.split(' ');
		for (var i = 0; i < eventNames.length; i++) {
			this.listeners[eventNames[i]] = {
				fn : callback,
				scope : (scope ? scope : this)
			}
		}
		return this;
	},
	fireEvent : function(name) {
		var r;
		var cb = this.listeners[name];
		if (cb && typeof cb.fn == 'function')
			r = cb.fn.apply(cb.scope, Array.prototype.slice.call(arguments, 1));
		return r;
	},
	disableRandomColor : function() {
		this.config.randomFontColor = false;
	},
	setResizeLimit : function(l, r) {
		this.resizeLeft = l;
		this.resizeRight = r;
	},
	disableResize : function() {
		this.config.resizable = false;
	},
	disableCollapse : function() {
		this.config.collapsable = false;
	},
	disableLocation : function() {
		delete this.Location;
	},
	disableActivity : function() {
		delete this.Activity;
	},
	disableFace : function() {
		delete this.Face;
	},
	disableSuggest : function() {
		delete this.Suggest;
	},
	disableFont : function() {
		delete this.Font;
	},
	els : {
		// 对话内容div
		dialogDiv : $('#outputArea'),
		// 文本输入框
		inputBox : $('#inputArea'),
		// 发送按钮
		sendButton : $('#inputButtonDiv'),
		// p4活动窗口frame
		activityFrame : $('#activityFrame'),
		// 归属地选择按钮
		locationSelBtn : $('#cityBtn'),
		// 归属地选择div
		locationSelDiv : $('#citySelDiv'),
		// 已选择归属地span
		locationVal : $('#citySpan'),
		// 归属地标签名称
		locationLabel : $('#locationLabel'),
		// 关闭归属地选择框按钮
		closeLocationBtn : $('#closeCityBtn'),
		// 归属地选择外层div
		locationWrapper : $('#userarea'),
		// 工具栏归属地部分
		locationBar : $('#locationBar'),
		// 热点问题按钮
		hotquestionBtn : $('#hotquestionBtn'),
		// 留言按钮
		leavewordBtn : $('#leavewordBtn'),
		// 反馈按钮
		feedbackBtn : $('#feedbackBtn'),
		// 帮助按钮
		helpBtn : $('#helpBtn'),
		// 归属地选择上方提示元素
		locationPromptHeader : $('#locationPromptHeader'),
		// 归属地选择下方提示元素
		locationPromptFooter : $('#locationPromptFooter'),
		// 表情图片img元素,
		faceImg : $('#emoImg'),
		// 表情按钮,
		faceBtn : $('#emoticonBtn'),
		// 表情关闭按钮
		faceCloseBtn : $('#emoCloseBtn'),
		// 表情外层div
		faceDiv : $('#emoDiv'),
		// 工具栏
		toolBar : $('#toolbar'),
		// 用户登录按钮
		userLoginBtn : $('#loginBtn'),
		// 用户登录图片
		userLoginLogo : $('#loginLogo'),
		// 字体设置div
		fontCfgDiv : $('#fontDiv'),
		// 字体按钮
		fontBtn : $('#fontBtn'),
		// 字体设置按钮
		fontSizeSel : $('#font_size_sel'),
		fontWeightBtn : $('#font_bold'),
		fontStyleBtn : $('#font_style'),
		fontDecorateBtn : $('#font_underline'),
		fontColorBtn : $('#font_colorPick'),
		appDiv : $('#activityDiv'),
		appPanelDiv : $('#activityPanelDiv'),
		appTabDiv : $('#activityTabDiv'),
		appTabDivMask : $('#activityTabDivMask'),
		// 工具栏定位div
		toolbarPosDiv : $('<div/>').attr('id', 'postionDiv').css({
					'position' : 'relative',
					'height' : 0,
					'width' : 0,
					'float' : 'left'
				}),
		panelSpacer : $('#centerSpacer'),
		rightSpacer : $('#rightSpacer'),
		leftPanel : $('#leftPanelDiv'),
		rightPanel : $('#rightPanelDiv'),
		introDiv : $('#introDiv'),
		tabLeftBtn : $('#tab_left'),
		tabRightBtn : $('#tab_right')
	},
	getSession : function() {
		return this.session;
	},
	closeSession : function() {
		if (this.session)
			this.session.close();
		if(this.sessionIdleReplys){
			for (var key in this.sessionIdleReplys) {
				if (this.sessionIdleTimeouts[key])
					clearTimeout(this.sessionIdleTimeouts[key]);
			}
		}
	},
	init : function() {
		var ret = this.fireEvent('init');
		if (ret == false)
			return false;
		if (this.Location)
			this.Location.init();
		if (this.Activity)
			this.Activity.init();
		if (this.Face)
			this.Face.init();
		if (this.Suggest)
			this.Suggest.init();
		if (this.Font)
			this.Font.init();
		var self = this;
		var tabDiv0 = self.els.appTabDivMask.get(0);
		this.els.tabLeftBtn.bind('click', function(){
			var _t = 0;
			var _f = function() {
				if	(79 - _t < 5)
					tabDiv0.scrollLeft -= 79 - _t;
				else {
					tabDiv0.scrollLeft -= 5;
					_t += 5;
					if(_t < 79)
						setTimeout(function(){_f();}, 10);
				}
			};
			_f();
		});
		this.els.tabRightBtn.bind('click', function(){
			var _t = 0;
			var _f = function() {
				if	(79 - _t < 5)
					tabDiv0.scrollLeft += 79 - _t;
				else {
					tabDiv0.scrollLeft += 5;
					_t += 5;
					if(_t < 79)
						setTimeout(function(){_f();}, 10);
				}
			};
			_f();
		});
		this.els.locationSelBtn.bind('click', function() {
					self.Location.openLocationSelectBox();
				});
		this.els.hotquestionBtn.bind('click', function() {
					// var _url = _ROOT_PATH +
					// 'p4pages/hot-question.action?platform=web';
					// var _location = self.config.userLocation;
					// if (_location)
						// _url += '&location=' + _location;
					// self.Activity.openP4(_url, '热点问题');
					if (self.session)
						self.session.sendMessage("hot")
				});
		this.els.leavewordBtn.bind('click', function() {
					if (self.session)
						self.session.sendMessage("leaveword")
				});
		this.els.feedbackBtn.bind('click', function() {
					if (self.session)
						self.session.sendMessage("feedback")
				});
		if (typeof this.config.collapsable == 'undefined'
				|| this.config.collapsable) {
			this.Collapse.init();
		}
		if (typeof this.config.resizable == 'undefined'
				|| this.config.resizable) {
			new this.Resize(this.els.panelSpacer.get(0));
		}
		if (window._DEFAULT_P4_WIDTH) {
			var _dr = parseInt($client.els.panelSpacer.css('right'));
			var lr = parseInt($client.els.leftPanel.css('right'));
			var rr = parseInt($client.els.rightPanel.css('right'));
			var rw = parseInt($client.els.rightPanel.css('width'));
			var _offset = _DEFAULT_P4_WIDTH - _dr;
			if ($client.config.isIE6) {
				$client.els.rightPanel.css('width', rw + _offset);
				$($client.els.panelSpacer).css('right', _DEFAULT_P4_WIDTH + $client.els.panelSpacer.get(0).offsetWidth);
				try {
					adjustLayout("mainDiv", "panelDiv", 0);
					adjustLayout("panelDiv", "leftPanelDiv", 1);
					adjustLayout("leftPanelDiv", "outputDiv", 0);
					adjustLayout("inputDiv", "inputAreaDiv", 1);
					adjustLayout("rightPanelDiv", "activityDiv", 0);
					adjustLayout("activityDiv", "activityPanelDiv", 0);
					adjustLayout("outputDiv", "outputArea", 2);
				} catch (e) {
				}
			} else {
				$client.els.leftPanel.css('right', lr + _offset);
				$client.els.rightPanel.css('width', rw + _offset);
				$client.els.panelSpacer.css('right', _DEFAULT_P4_WIDTH);
				$client.els.rightPanel.css('right', rr);
			}
		}
		
		var connector = WebRobot.getInstance(_ROOT_PATH + 'webrobot');
		connector.on('connectfailed', function() {
					self.fireEvent('connectfailed');
				});
		connector.on('sessionclosed', function(sess) {
					self.fireEvent('sessionclosed');
				});
		connector.on('sessionopened', function(sess) {
					self.session = sess;
					var send = function(){
						var el = self.els.inputBox;
						var userInput = $.trim(el.val());
							// if (userInput == el.attr('lastContent')) {
							var s = $client.lastSuggestChoice;
							if (s  && s == userInput)
								self.sendText('_inType_fuzzy');
							else
								self.sendText();
							self.Suggest.visibilityHidden();
							$client.lastSuggestChoice = null;
							// }
					};
					if (self.els.inputBox) {
						self.els.inputBox.attr("readOnly", false);
						self.els.inputBox.bind('keyup', function(e) {
									e.stopPropagation();
									if (e.keyCode == 13 && !e.shiftKey) {
										send();
									}
								});
						self.els.inputBox.bind('keypress', function(e) {
									e.stopPropagation();
									if (e.keyCode == 13 && !e.shiftKey) {
										$(this).attr('lastContent',
												$.trim($(this).val()));
										return false;
									}
								});
						self.els.inputBox.bind('focus', function() {
									self.fireEvent('inputfocus');
								});
						self.els.inputBox.bind('blur', function() {
									self.fireEvent('inputblur');
								});
						self.els.sendButton.bind('click', function() {
									send();
								});
					}
					var user = self.user;
					if (user && user.userId && user.userId.length > 0)
						sess.login(user.userId + " " + JSON.toJson(user));
					self.fireEvent('sessionopened');
				});
		connector.on('nudgereceived', function(sess) {
					self.fireEvent('nudgereceived');
				});
		connector.on('actionreceived', function(sess, actionMsg) {
					self.fireEvent('actionreceived', actionMsg);
				});
		connector.on('messagereceived', function(sess, msg) {
					if (msg && msg.content && (msg.content.indexOf("SYS::") == 0 || msg.content.indexOf('::EmptyResponse')  == 0))
						return false;
					var ret = self.fireEvent('messagereceived', msg);
					if (ret == false)
						return false;
					var format = new MessageFormat(msg.fontName, "",
							msg.fontStyle, msg.fontColor,
							self.config.randomFontColor);
					var sig = msg.signature
							? msg.signature
							: self.robotDisplayName
									? self.robotDisplayName
									: sess.getRobotId();
					self.showMessage(sig, msg.content.replace(/(\r)?\n/g,
									"<br>"), msg.emoticons, format);
					self.config.robotName = sig;
					self.fireEvent('messagerendered', msg);
				});
		connector.on('appreceived', function(sess, appmsg) {
					var ret = self.fireEvent('appreceived', appmsg.data,
							appmsg.name);
					if (ret == false)
						return false;
					if (appmsg.data.indexOf('script://') != -1)
						return false;
					self.Activity.openP4(appmsg.data, appmsg.name);
				});
		connector.on('logincompleted', function(sess, user) {
					if (user) {
						if (!$client.user)
							$client.user = user;
					}
					self.fireEvent('logincompleted', user);
				});
		connector.on('exreceived', function(sess, exMsg) {
					self.fireEx(exMsg);
				});
		connector.on('dnupdated', function(displayName) {
					self.robotDisplayName = displayName;
					self.fireEvent('dnupdated', displayName);
				});
		connector.on('dpupdated', function(displayPicture) {
					self.fireEvent('dpupdated', displayPicture);
				});
		connector.on('pmupdated', function(personalMessage) {
					self.fireEvent('pmupdated', personalMessage);
				});
		connector.on('flashconnected', function(sess) {
					self.fireEvent('flashconnected', sess);
				});
		connector.connect();
		this.fireEvent('initcompleted');
	},
	sendText : function() {
		var content = $.trim(this.els.inputBox.val());
		var ret = this.fireEvent('beforemessagesend', content);
		if (ret == false)
			return false;
		if (!content || content == this.config.defaultContent)
			return false;
		if (this.config.locationForcePrompt && !this.config.locationSelected) {
			this.showMessage(this.config.robotName,
					this.config.locationForcePrompt);
			return false;
		}
		var _content = content.replace(/</gi, "&lt;");
		_content = _content.replace(/>/gi, "&gt;");
		_content = _content.replace(/(\r)?\n/g, "<br/>");
		this.showMessage(this.config.myName, _content, null, null, false);
		this.sendMessage((arguments.length > 0 ? arguments[0] + ':' : '') + content);
		this.els.inputBox.val('');
		this.els.inputBox.focus();
		this.fireEvent('messagesent', content);
	},
	sendTextEx : function(content) {
		this.els.inputBox.val(content);
		this.sendText();
	},
	sendMessage : function(msg, linknode, inputType) {
		if (inputType)
			msg = '_inType_' + inputType + ":" + msg;
		this.session.sendMessage(msg);
		this.processSessionIdle();
		if (linknode) {
			var removeLink = function(node) {
				if ($client.disableSubmitLink) {
					$client.disableSubmitLink(node);
				} else {
					node.onclick = function() {
					}
					$(node).css('cursor', 'auto').css('color', '#999999')
				}
			}
			removeLink(linknode);
			$('span', $(linknode).parent()).each(function(){
				if (this.innerHTML.indexOf('faqvote') != -1 || /[0-9]+/.test(this.innerHTML)) {
					var node = this.previousSibling;
					while (node != null) {
						if (node.tagName == 'a' || node.tagName == 'A') {
							removeLink(node);
							break;
						}
						node = node.previousSibling;
					}
				}
			});
		}
	},
	fireEx : function(exMsg) {
		var self = this;
		this.fireEvent('exreceived', exMsg);
		if (exMsg.name == 'suggest' && exMsg.data instanceof Array) {
			var ret = this.fireEvent('suggestreceived', exMsg.data[0]);
			if (ret == false)
				return false;
			this.Suggest.aiTipCallback(exMsg.data[0]);
		} else if (exMsg.name == 'initconfig') {
			// to be optimized
			var url = _ROOT_PATH + "check-login.action";
			var qs = window.location.search;
			if (qs && qs.length > 0) {
				url = url + qs;
			}
			if (url.indexOf('?') == -1)
				url += '?';
			$.getJSON(url + '&jsoncallback=?', {}, function(u) {
						if (u && u.error) {
							$(document).mask('登录后才可以访问！');
						} else {
							if (u)
								Channel_Login(u);
						}
					});

			var initConObj = exMsg.data;
			var ret = this.fireEvent('initconfigreceived', initConObj);
			if (ret == false)
				return false;
			if (initConObj) {
				if (initConObj.robotReplyStyle) {
					this.config.robotReplyStyle = initConObj.robotReplyStyle;
					$('.robotContent', this.els.dialogDiv).each(function() {
								$(this).attr('style',
										self.config.robotReplyStyle);
							})
				}
				if (initConObj.inputPrompt) {
					this.config.defaultContent = initConObj.inputPrompt;
					this.els.inputBox.attr('class', 'inputbox_default_style');
					this.els.inputBox.val(initConObj.inputPrompt)
				}
				if (initConObj.locationLabel)
					this.els.locationLabel.html(initConObj.locationLabel);
				if (initConObj.locationPromptHeader)
					this.els.locationPromptHeader
							.html(initConObj.locationPromptHeader);
				if (initConObj.locationPromptFooter)
					this.els.locationPromptFooter
							.html(initConObj.locationPromptFooter);
				if (initConObj.locationForcePrompt)
					this.config.locationForcePrompt = initConObj.locationForcePrompt;
				if (initConObj.sessionIdlePrompt) {
					var replyArr = initConObj.sessionIdlePrompt.split('|');
					if (replyArr.length) {
						this.sessionIdleReplys = {};
						for (var i = 0; i < replyArr.length; i++) {
							var replyData = replyArr[i].split(/:|：/);
							if (replyData.length == 2) {
								this.sessionIdleReplys[parseInt(replyData[0])] = replyData[1];
							}
						}
						this.processSessionIdle();
					}
				}
				if (initConObj.messageDateFormat) {
					this.messageDateFormat = initConObj.messageDateFormat;
				}
				var locations = initConObj.locations;
				if (locations && this.Location) {
					this.els.locationBar.show();
					this.Location.initLocation(locations);
					this.Location.openLocationSelectBox();
				}
				var homeP4Title = '首页';
				var homeP4Path = _ROOT_PATH + 'p4pages/home.html';
				var helpP4Title = '使用帮助';
				var helpP4Path = _ROOT_PATH + 'p4pages/help.html';
				if (initConObj.homeP4Title)
					homeP4Title = initConObj.homeP4Title;
				if (initConObj.homeP4Path)
					homeP4Path = initConObj.homeP4Path;
				if (initConObj.helpP4Title)
					helpP4Title = initConObj.helpP4Title;
				if (initConObj.helpP4Path)
					helpP4Path = initConObj.helpP4Path;
				this.Activity.openP4(homeP4Path, homeP4Title);
				var self = this;
				this.els.helpBtn.bind('click', function() {
							self.Activity.openP4(helpP4Path, helpP4Title);
						});
				if (!initConObj.robotNeedLogin)
					this.els.userLoginBtn.parent().show();
			}
		} else if (exMsg.name == 'locationselected') {
			this.config.locationSelected = true;
			this.config.userLocation = exMsg.data;
			if ($('li[title=热点问题]', this.els.appTabDiv).length > 0)
				this.els.hotquestionBtn.trigger('click');
			this.fireEvent('locationselected', exMsg.data);
		} else if (exMsg.name == 'imgtxtmsg') {
			var imgtxtmsg = null;
			eval('imgtxtmsg = ' + exMsg.data);
			if (imgtxtmsg.length == 1) {
				imgtxtmsg = imgtxtmsg[0];
				var tpl = '<div class="show-img-msg-left"><div class="msg-item-wrapper"><div class="msg-item multi-msg"><div class="appmsgItem"><a href="${url}" style="text-decoration:none;" target="_blank"><div class="appmsg_info appmsg_info0">'+
					'<h4 class="appmsg_title appmsg_title0"><span>${title}</span></h4><em class="appmsg_date appmsg_date0">${date}</em></div><div class="cover"><img class="i-img" src="${image}" /></div>'+
					'<div class="rel sub-msg-item appmsgItem sub-msg-opr-show sub-msg-opr-show0"><h4 class="msg-t msg-t0"><span class="i-title">${description}</span></h4></div></a></div></div></div></div>';
				var date = new Date();
				var dateStr = date.getFullYear() + '年' + (date.getMonth() + 1) + '月' + date.getDate() + '日';
				imgtxtmsg = tpl.replace('${title}', imgtxtmsg.title).replace('${description}', imgtxtmsg.description).replace('${image}', imgtxtmsg.image).replace('${date}', dateStr).replace("${url}", imgtxtmsg.url);
			} else if (imgtxtmsg.length > 1) {
				var tpl = '<div class="show-img-msg-left"><div class="msg-item-wrapper"><div class="msg-item multi-msg"><div class="appmsgItem"><div class="appmsg_info appmsg_info0"></div>'+
					'<div class="cover"><a href="${url}" target="_blank"><h4 class="msg-t"><span class="i-title" style="color: #fff;" id="appmsgItem1_title">${title}</span></h4><img class="i-img" src="${image}" /></a></div>${subItems}</div></div></div></div>';
				var firstItem = imgtxtmsg[0];
				var imgtxtmsg0 = tpl.replace('${title}', firstItem.title).replace('${image}', firstItem.image).replace('${url}', firstItem.url);
				var subTpl = '<div class="rel sub-msg-item appmsgItem sub-msg-opr-show"><a href="${url}" target="_blank"><span class="thumb"><img class="i-img" src="${image}"></span><h4 class="msg-t"><span class="i-title">${title}</span></h4></a></div>';
				var subItems = '';
				for (var i = 1; i < imgtxtmsg.length; i++) {
					subItems += subTpl.replace('${title}', imgtxtmsg[i].title).replace('${image}', imgtxtmsg[i].image).replace('${url}', imgtxtmsg[i].url);
				}
				imgtxtmsg = imgtxtmsg0.replace('${subItems}', subItems);
			}
			var sig = this.robotDisplayName ? this.robotDisplayName : this.getSession().getRobotId();
			this.showMessage(sig, imgtxtmsg);
		} else if (exMsg.name == 'imgmsg') {
			var imgmsg = null;
			eval('imgmsg = ' + exMsg.data);
			var sig = this.robotDisplayName ? this.robotDisplayName : this.getSession().getRobotId();
			var imgId = 'img_' + new Date().getTime();
			imgmsg = '<img onload="$client.utils.resizeImg(this, 200);" id="' + imgId + '" src="' + imgmsg.url + '" />';
			this.showMessage(sig, imgmsg);
		} else if (exMsg.name == 'videomsg') {
			var objectId = null;
			var videomsg = null;
			eval('videomsg = ' + exMsg.data);
			if (videomsg.url) {
				objectId = videomsg.url.split('/');
				objectId = objectId[objectId.length - 1];
				var thumbnail = '<img name="' + objectId + '" src="' + _ROOT_PATH + 'template/images/template/playerSubmit.png" style="cursor:pointer;width:136px;height:91px;;border:6px solid #ccc;'+
					'background:url(\'' + videomsg.thumbnail + '\');background-size:135px 90px;background-repeat:no-repeat;" />';
				var sig = this.robotDisplayName ? this.robotDisplayName : this.getSession().getRobotId();
				this.showMessage(sig, thumbnail);
				var self = this;
				$('img[name=' + objectId + ']').unbind('click').click(function(){
					self.Activity.openP4(_ROOT_PATH + 'template/videoPlayer.jsp?url=' + encodeURIComponent(videomsg.url), '视频播放');
				});
			}
		} else if (exMsg.name == 'musicmsg') {
			var musicmsg = null;
			eval('musicmsg = ' + exMsg.data);
			var	objectId = musicmsg.hqUrl.split('/');
			objectId = objectId[objectId.length - 1];
			var tpl = '<div id="' + objectId + '" class="mediaBox audioBox"><div class="mediaContent"><span class="audioTxt">点击播放</span> <span class="audioIco"></span></div><span class="iconArrow"></span></div>';
			var sig = this.robotDisplayName ? this.robotDisplayName : this.getSession().getRobotId();
			this.showMessage(sig, tpl);
			$('#' + objectId).click(function() {
				var af = $('#audioFrame');
				if (!af.length) {
					af = $('<iframe></iframe>').attr('id', 'audioFrame').css('display', 'none');
					$(document.body).append(af);
				}
				af.attr('src', _ROOT_PATH + 'template/audioPlayer.jsp?objectId=' + objectId + '&url=' + encodeURIComponent(musicmsg.hqUrl));
			});
		}
	},
	showMessage : function(title, content, emoticonsObj, format, isIncoming) {
		if (typeof isIncoming == 'undefined')
			isIncoming = true;
		if (this.els.dialogDiv) {
			var newdiv = $('<div/>').attr('class', 'chat-segment');
			var className = 'user';
			if (isIncoming)
				className = 'robot';
			var nameHtml = '<span class="' + className + 'Name" >' + title
					+ '</span>';
			if (this.messageDateFormat) {
				nameHtml = nameHtml + '<span class="messageDate">'
						+ $.format.date(new Date(), this.messageDateFormat)
						+ '</span>'
			}
			var contentHtml = content;
			contentHtml = this.utils.handleFaces(contentHtml);
			if (isIncoming) {
				contentHtml = this.utils.handleLinks(contentHtml);
				if (emoticonsObj) {
					var emoticons = new Emoticons();
					for (var ekey in emoticonsObj) {
						var emVal = emoticonsObj[ekey];
						if (!ekey || !emVal)
							continue;
						emoticons.Insert(emVal, ekey);
					}
					try {
						if (emoticons != null && emoticons.Size() > 0)
							contentHtml = emoticons.Replace(contentHtml);
					} catch (e) {
					}
				}
				if (format != null)
					contentHtml = format.ApplyFormat(contentHtml);
				var _style = this.config.robotReplyStyle;
				_style = _style ? _style : '';
				contentHtml = '<div style="' + _style + '" class="' + className
						+ 'Content" >' + contentHtml + '</div>';
			} else {
				var inputBox = this.els.inputBox;
				var inputBox_style = inputBox.attr('style');
				contentHtml = '<div class="' + className + 'Content" style="'
						+ (inputBox_style ? inputBox_style : '') + '">'
						+ contentHtml + '</div>';
			}
			newdiv.html(nameHtml + contentHtml);
			this.els.dialogDiv.append(newdiv);
			var dialogEle = this.els.dialogDiv.get(0);
			dialogEle.scrollTop = dialogEle.scrollHeight;
		}
	},
	processSessionIdle : function() {
		if (this.sessionIdleReplys) {
			if (!this.sessionIdleTimeouts)
				this.sessionIdleTimeouts = {};
			for (var key in this.sessionIdleReplys) {
				var self = this;
				if (this.sessionIdleTimeouts[key])
					clearTimeout(this.sessionIdleTimeouts[key]);
				this.sendIdleReply(key);
			}
		}
	},
	sendIdleReply : function(key) {
		var self = this;
		this.sessionIdleTimeouts[key] = setTimeout(function() {
					self.showMessage(self.config.robotName,
							self.sessionIdleReplys[key]);
					delete self.sessionIdleReplys[key];
					var i = 0;
					for (var _k in self.sessionIdleReplys)
						i++;
					if (i == 0)
						self.session.sendMessage('SYS::{"handler.name":"acs", "input":"exit"}');
				}, key * 60 * 1000)
	},
	logout : function() {
		var _this = this;
		$.getJSON(_ROOT_PATH + "logout.action?ts=" + new Date().getTime()
						+ '&jsoncallback=?', {}, function(resp) {
					if (resp) {
						var logoutImg = $('#logoutImg');
						if (logoutImg.length)
							logoutImg.attr('src', resp);
						else
							$('<img/>').css('display', 'none')
									.attr('src', resp).attr('id', 'logoutImg')
									.appendTo($('body'));
					}
					var _user = _this.user;
					if (_user && _user.userId && _user.userId.length > 0) {
						_this.session.logout(_user.userId + " "
								+ JSON.toJson(_user));
					}
					if (_user && _user.userName)
						_this.config.myName = _this.config.defaultMyName;
					_this.els.userLoginBtn.html('用户登录')
							.css('cursor', 'pointer').bind('click', function() {
								_this.Activity.openP4(_ROOT_PATH
												+ 'p4pages/logon.action?ts='
												+ new Date().getTime(), '用户登录');
							});
					$('#logoutBtn').remove();
					if (_user && _user.userId)
						_this.fireEvent('userloggedout', _user.userId);
				});
	},
	utils : {
		resizeImg : function(el, size) {
			var img = $(el);
			var imgW = img.width();
			var imgH = img.height();
			if (imgH > size) {
				imgW = imgW * size / imgH;
				img.get(0).style.width = parseInt(imgW) + 'px';
				img.get(0).style.height = size + 'px';
			} else if (imgW > size) {
				imgH = imgH * size / imgW;
				img.get(0).style.height = parseInt(imgH) + 'px';
				img.get(0).style.width = size + 'px';
			}
		},
		getAbsoluteLeft : function(o) {
			oLeft = o.offsetLeft;
			while (o.offsetParent != null) {
				oParent = o.offsetParent;
				oLeft += oParent.offsetLeft;
				o = oParent;
			}
			return oLeft;
		},
		getAbsoluteTop : function(o) {
			oTop = o.offsetTop;
			while (o.offsetParent != null) {
				oParent = o.offsetParent;
				oTop += oParent.offsetTop;
				o = oParent;
			}
			return oTop;
		},
		handleLinks : function(html) {
			for (var i = 0; i < this.linkPatterns.length; i++) {
				html = html.replace(this.linkPatterns[i][0],
						this.linkPatterns[i][1]);
			}
			return html;
		},
		handleFaces : function(html) {
			var faces = $client.Face.faceKeys;
			for (var i = 0; i < faces.length; i++) {
				if (html.indexOf(faces[i]) != -1)
					html = html.replace(faces[i], '<img src="'
									+ _APP_IMAGE_PATH + 'face/' + (i + 1)
									+ '.gif"/>');
			}
			return html;
		},
		linkPatterns : [
				// defalutPattern
				[
						/\[silence\](.*?)\[\/silence\]/gi,
						'$1'],
				// submitPattern
				[
						/\[link\s+submit=[\'\"]+([^\[\]\'\"]+)[\'\"]+\s*[^\[\]]*\]([^\[\]]+)\[\/link\]/gi,
						'<a id="submitLink" href="#" onclick="$client.sendMessage(this.nextSibling.innerHTML,this);return false;" >$2</a><span style="display:none;">$1</span>'],
				[
						/\[link\s+submit=([^\s\[\]\'\"]+)\s*[^\[\]]*\]([^\[\]]+)\[\/link\]/gi,
						'<a id="submitLink" href="#" onclick="$client.sendMessage(this.nextSibling.innerHTML,this);return false;" >$2</a><span style="display:none;">$1</span>'],
				// defalutPattern
				[
						/\[link(?:\s+type=[\'\"](.*?)[\'\"])?\](.*?)\[\/link\]/gi,
						'<a id="submitLink" type="$1" href="#" onclick="$client.sendMessage(this.innerHTML,this,this.getAttribute(\'type\'));return false;" >$2</a>'],
				[
						/<a id="submitLink".*?>.*?<\/a>/gi,
						function (p) {
							return p.replace(/(http|ftp|https)/gi, '$1_');
						}],
				// closeSession
				[
						/\[link\s+close=[\'\"]+([^\[\]\'\"]+)[\'\"]+\s*[^\[\]]*\]([^\[\]]+)\[\/link\]/gi,
						'<a id="closeLink" href="#" onclick="$client.closeSession(\'$1\',this);return false;" >$2</a>'],
				[
						/\[link\s+close=([^\s\[\]\'\"]+)\s*[^\[\]]*\]([^\[\]]+)\[\/link\]/gi,
						'<a id="closeLink" href="#" onclick="$client.closeSession(\'$1\',this);return false;" >$2</a>'],
				// enterPattern
				[
						/\[link\s+enter=[\'\"]+([^\[\]\'\"]+)[\'\"]+\s*[^\[\]]*\]([^\[\]]+)\[\/link\]/gi,
						'<a href="#" onclick="$client.els.inputBox.set(\'value\', \'$1\');return false;" >$2</a>'],
				[
						/\[link\s+enter=([^\s\[\]\'\"]+)\s*[^\[\]]*\]([^\[\]]+)\[\/link\]/gi,
						'<a href="#" onclick="$client.else.inputBox.set(\'value\', \'$1\');return false;" >$2</a>'],
				// urlPattern
				[
						/\[link\s+url=[\'\"]+([^\[\]\'\"]+)[\'\"]+\s*[^\[\]]*\]([^\[\]]+)\[\/link\]/gi,
						'<a href="$1" target="_blank">$2</a>'],
				[
						/\[link\s+url=([^\s\[\]\'\"]+)\s*[^\[\]]*\]([^\[\]]+)\[\/link\]/gi,
						'<a href="$1" target="_blank">$2</a>'],
				// p4Pattern1
				[
						/\[link\s+p4=[\'\"]+([^\[\]\'\"]+)[\'\"]+\s+title=[\'\"]+([^\[\]\'\"]+)[\'\"]+\s*[^\[\]]*\]([^\[\]]+)\[\/link\]/gi,
						'<a href="#" onclick="$client.Activity.openP4(\'$1\',\'$2\');return false;" >$3</a>'],
				[
						/\[link\s+p4=([^\s\[\]\'\"]+)\s+title=([^\s\[\]\'\"]+)\s*[^\[\]]*\]([^\[\]]+)\[\/link\]/gi,
						'<a href="#" onclick="$client.Activity.openP4(\'$1\',\'$2\');return false;" >$3</a>'],
				// p4Pattern2
				[
						/\[link\s+p4=[\'\"]+([^\[\]\'\"]+)[\'\"]+\s*[^\[\]]*\]([^\[\]]+)\[\/link\]/gi,
						'<a href="#" onclick="$client.Activity.openP4(\'$1\',\'$2\');return false;" >$2</a>'],
				[
						/\[link\s+p4=([^\s\[\]\'\"]+)\s*[^\[\]]*\]([^\[\]]+)\[\/link\]/gi,
						'<a href="#" onclick="$client.Activity.openP4(\'$1\',\'$2\');return false;" >$2</a>'],
				[
				 		/(^|[^"'=])((http|https|ftp):\/\/([\w-]+\.)+[\w-]+([\w-#\.\/?=;!*%$]*)?([\w-#\.&=;!*%$]*)?)/gi,
						'$1<a href="$2" target="_new">$2</a>'],
				[
				 		/(http|ftp|https)_/gi, 
				 		'$1']
				]
	},
	Location : {
		init : function() {
			var self = this;
			var _postionDiv = $('<div/>').css({
						'position' : 'absolute',
						'height' : 0,
						'width' : 0,
						'top' : 5,
						'zIndex' : 100
					})
			$client.els.inputBox.parent().parent().prepend(_postionDiv);
			var locationPosDiv = $('<div/>').css({
						'position' : 'absolute',
						'background-color' : 'white'
					})
			var locationWrapper = $client.els.locationWrapper;
			locationWrapper.hide();
			locationPosDiv.append(locationWrapper);
			_postionDiv.append(locationPosDiv);
			$client.els.closeLocationBtn.bind('click', function() {
						self.closeLocationSelectBox();
					});
		},
		initLocation : function(locations) {
			var self = this;
			var locationSelDiv = $client.els.locationSelDiv;
			for (var key in locations) {
				var newSpan = $('<span/>').addClass('areabox');
				var value = locations[key];
				var newA = $('<a/>').attr({
							'href' : '#',
							'locationId' : key,
							'title' : '选择' + value + '为归属地'
						}).html(value);
				newA.bind('click', function(e) {
							self.selecteLocation(e, $(this));
						});
				newA.bind('mouseover', function(e) {
							e.stopPropagation();
							$(this).css('fontWeight', 'bold');
						});
				newA.bind('mouseout', function(e) {
							e.stopPropagation();
							$(this).css('fontWeight', 'normal');
						});
				newSpan.append(newA);
				locationSelDiv.append(newSpan);
			}
			var newClearDiv = $('<div/>').addClass('clear');
			locationSelDiv.append(newClearDiv);
		},
		openLocationSelectBox : function() {
			$client.els.locationWrapper.slideToggle('slow', function() {
						if ($client.els.locationWrapper.css('display') == 'none')
							$client.els.sendButton.show();
						else
							$client.els.sendButton.hide();
					});

		},
		closeLocationSelectBox : function() {
			$client.els.locationWrapper.slideToggle('slow');
			var btn = $client.els.sendButton;
			btn.show();
			// $client.els.inputBox.focus();
		},
		selecteLocation : function(e, ele) {
			e.stopPropagation();
			var locationId = ele.attr('locationId').replace(':', "：");
			$client.session.sendMessage("setloc:" + locationId);
			$client.addDim('location', locationId);
			this.closeLocationSelectBox();
			$client.els.locationVal.html(ele.html());
			$client.location = locationId;
		}
	},
	Activity : {
		init : function() {
			this.actTabs = new this.ActivityTab();
			var self = this;
			$client.els.userLoginBtn.bind('click', function() {
						self
								.openP4(_ROOT_PATH + 'p4pages/logon.action',
										'用户登录');
					});
		},
		openP4 : function(url, name) {
			var title = name;
			if (name.length > 4)
				name = name.substring(0, 4) + "...";
			this.actTabs.addTab(title, name, url);
		},
		closeP4 : function(title) {
			this.actTabs.removeTab(title);
		},
		ActivityTab : function(options) {
			var self = this;
			var idx = 1;
			this.options = $.extend({
						mouseOverClass : 'active',
						activateOnLoad : 'first'
					}, options || {});
			this.el = $client.els.appPanelDiv;
			this.titleEl = $client.els.appTabDiv;
			this.elid = $client.els.appPanelDiv.attr('id');
			this.titleid = $client.els.appTabDiv.attr('id');
			this.titles = $('li', this.titleEl);
			this.panels = $('div[isapp=true]', this.el);
			this.activeTitle = null;
			this.titles.each(function(idx, item) {
						$(item).bind('click', function() {
									self.activate(item);
								});
					});
			this.getPanelById = function(id) {
				return $('#' + id);
			}
			var clickOrder = 1;
			this.activate = function(tab) {
				if (typeof tab == 'string') {
					tab = $('li[title="' + tab + '"]', this.titleEl)[0];
				}
				if (typeof tab == 'object') {
					var $tab = $(tab);
					var leftNums = 0, _break = false;
					$('li', $tab.parent()).each(function() {
								var _t = $(this);
								if (_t.attr('idx') != $tab.attr('idx')) {
									_t.attr('active', 'false');
									$('a', _t).hide();
									if(!_break)
										leftNums ++;
								} else {
									_break = true;
								}
							});
					var tabDiv0 = $client.els.appTabDivMask.get(0);
					tabDiv0.scrollLeft = leftNums * 79;
					$tab.attr('active', 'true').attr('order', clickOrder++);
					if ($('li', $tab.parent()).length > 1)
						$('a', $tab).show();
					else
						$('a', $tab).hide();
					var newTab = $tab.attr('title');
					for (var i = 0; i < this.panels.length; i++) {
						if(this.panels[i])
							$(this.panels[i]).removeClass('active');
					}
					this.activePanel = this.getPanelById(newTab.replace(/\s|\./gi, ''));
					this.activePanel.addClass('active');
					if (this.activeTitle)
						this.activeTitle.removeClass('active');
					this.activeTitle = $tab;
					this.activeTitle.addClass('active');
				}
			}
			this.addTab = function(title, name, url) {
				title = title.replace(/["“”]/gi, '');
				title = $.trim(title);
				title4Id = title.replace(/\s|\./gi, '');
				var iftab = $('li[title="' + title + '"]', this.titleEl);
				if (iftab.length > 0) {
					$('iframe', $('#' + title4Id)).get(0).src = url;
					this.activate(title);
					return;
				}
				var newLi = $('<li/>').attr('title', title).attr('idx', idx++);
				newLi
						.html('<p id="activityTabTitle">'
								+ name
								+ '</p><p id="activityTabCloseBtn"><a href="#" onclick="$client.Activity.closeP4(\''
								+ title
								+ '\');event.cancelBubble=true;return false;"><img style="border:none;" src="images/x.gif" width="8" height="22" /></a></p>');
				newLi.bind('click', function() {
							self.activate(newLi.get(0));
						});
				newLi.bind('mouseover', function() {
							if ($('li', $(this).parent()).length > 1)
								$('a', $(this)).show();
						});
				newLi.bind('mouseout', function() {
							if ($(this).attr('active') != 'true')
								$('a', $(this)).hide();
						});
				var newPanel = $('<div/>').attr({
							'id' : title4Id,
							'isapp' : 'true'
						});
				newPanel
						.html('<iframe width="100%" heigth="100%" src="'
								+ url
								+ '"  scrolling ="auto" allowTransparency="true" frameborder="no"></iframe>');
				var rawp = newPanel.get(0);
				var ret = $client.fireEvent('beforeappopen', rawp, name, url);
				if (ret == false)
					return false;
				this.panels.push(rawp);
				this.el.append(newPanel);
				var ulEl = $('ul:first-child', this.titleEl);
				ulEl.append(newLi);
				this.activate(newLi);
				var pnode = newPanel.parent();
				newPanel
						.css('height', (pnode.attr('clientHeight') - 10) + 'px');
				pnode.bind('resize', function() {
							newPanel.css('height',
									(pnode.attr('clientHeight') - 10) + 'px')
						});
				var tabsWidth = parseInt($client.els.appTabDiv.css('width'));
				var liNum = $('li', ulEl).length;
				if (liNum * 79 > tabsWidth) {
					$client.els.tabLeftBtn.show();
					$client.els.tabRightBtn.show();
					var tabDiv0 = $client.els.appTabDivMask.get(0);
					ulEl.css('width', liNum * 79);
					tabDiv0.scrollLeft += tabDiv0.scrollWidth;
				}
				$client.fireEvent('appopened', rawp, name, url);
			}
			this.removeTab = function(title) {
				if ($('li', this.titleEl).length == 1)
					return;
				try {
					var tab = $('li[title="' + title + '"]', this.titleEl)
					if (tab.length == 0)
						return
					var p = this.getPanelById(title);
					var ret = $client.fireEvent('beforeappremove', p);
					if (ret == false)
						return false;
					$('iframe', p).attr('src', '');
					p.remove();
					var rli = $('li[title="' + title + '"]', this.titleEl);
					var lastClick = parseInt(rli.attr('order'));
					rli.remove();
					this.panels = $('div[isapp=true]', this.el);
					var _tgt, _lastOffset = null;
					$('li', this.titleEl).each(function() {
						var _offset = lastClick
								- parseInt($(this).attr('order'));
						if (_lastOffset == null) {
							_lastOffset = _offset;
							_tgt = this;
						}
						if (_offset < _lastOffset)
							_tgt = this;
					});
					this.activate(_tgt);
					var ulEl = $('ul:first-child', this.titleEl);
					var tabsWidth = parseInt($client.els.appTabDiv.css('width'));
					var liNum = $('li', ulEl).length;
					var tabDiv0 = $client.els.appTabDivMask.get(0);
					if (tabsWidth < liNum * 79) {
						$client.els.tabLeftBtn.show();
						$client.els.tabRightBtn.show();
						ulEl.css('width', liNum * 79);
						tabDiv0.scrollLeft += tabDiv0.scrollWidth;
					} else {
						$client.els.tabLeftBtn.hide();
						$client.els.tabRightBtn.hide();
						tabDiv0.scrollLeft -= tabDiv0.scrollWidth;
					}
				} catch (e) {
				}
			}
			if (this.options.activateOnLoad != 'none') {
				if (this.options.activateOnLoad == 'first')
					this.activate(this.titles[0], true);
				else
					this.activate(this.options.activateOnLoad, true);
			}
		}
	},
	Face : {
		column : 11,
		pic_width : 30,
		pic_height : 30,
		faceKeys : ["[:)]", "[:d]", "[8-)]", "[8o|]", "[;)]", "[|o]", "[(h)]",
				"[:-/]", "[:@]", "[:-#]", "[:s]", "[:|]", "[:?]", "[*-:)]",
				"[b)]", "[o:)]", "[:gg]", "[:’(]", "[^o)]", "[:&]", "[:b]",
				"[:(]", "[:-o]", "[:p]", "[:tq)]", "[(s)]", "[(st)]", "[(g)]",
				"[(i)]", "[(co)]", "[(mp)]", "[(um)]", "[(d)]", "[)-|]",
				"[(^)]", "[(@)]", "[(&)]", "[(sn)]", "[(y)]", "[(c)]", "[(r)]",
				"[(f)]", "[(w)]", "[({)]", "[(})]", "[(l)]", "[(u)]", "[(8)]",
				"[(k)]", "[(o)]", "[(#)]", "[(*)]"],
		initFaces : function() {
			var newmap = $('<Map/>').attr({
						'id' : 'faceMap',
						'name' : 'faceMap'
					});
			$client.els.faceDiv.append(newmap);
			var x = 0;
			var y = 0;
			for (i = 0; i < this.faceKeys.length; i++) {
				var coords = x * this.pic_width + ',' + y * this.pic_height
						+ ',' + (x + 1) * this.pic_width + ',' + (y + 1)
						* this.pic_height;
				var subArea = $('<area/>').attr({
					'shape' : 'rect',
					'coords' : coords,
					'href' : 'javascript:$client.Face.insertFaceKey(\"'
							+ this.faceKeys[i] + '\");'
				});
				newmap.append(subArea);
				x++;
				if (x > 10) {
					x = 0;
					y++;
				}
			}
		},
		insertFaceKey : function(key) {
			this.insert(key);
			this.toggleDisplay();
		},
		insert : function(str) {
			var obj = $client.els.inputBox;
			if (document.selection) {
				obj.focus();
				var sel = document.selection.createRange();
				document.selection.empty();
				if (obj.val() == $client.config.defaultContent)
					obj.val('');
				sel.text = str;
			} else {
				var val = obj.val();
				if (!val)
					return;
				var prefix, main, suffix;
				var obj0 = obj.get(0);
				prefix = val.substring(0, obj0.selectionStart);
				main = val.substring(obj0.selectionStart, obj0.selectionEnd);
				suffix = val.substring(obj0.selectionEnd);
				if (val == $client.config.defaultContent)
					prefix = '';
				val = prefix + str + suffix;
				if (val.indexOf($client.config.defaultContent) != -1)
					val = val.replace($client.config.defaultContent, '');
				obj.val(val);
			}
			obj.focus();
		},
		toggleDisplay : function() {
			if ($client.els.fontCfgDiv.css('display') != 'none')
				$client.Font.toggle($client.els.fontBtn);
			var el = $client.els.faceDiv;
			var state = el.css('display');
			if (state == '' || state == 'block')
				el.css('display', 'none');
			else
				el.css('display', 'block');
		},
		initPostion : function() {
			var postionDiv = $client.els.toolbarPosDiv;
			$client.els.toolBar.prepend(postionDiv);
			var faceDiv = $client.els.faceDiv;
			faceDiv.css({
						'position' : 'absolute',
						'top' : '-188px'
					});
			postionDiv.append(faceDiv);
		},
		init : function() {
			this.initPostion();
			$client.els.faceImg.attr("usemap", "#faceMap");
			$client.els.faceBtn.bind('click', function() {
						$client.Face.toggleDisplay();
					});
			$client.els.faceCloseBtn.bind('click', function() {
						$client.Face.toggleDisplay();
					});
			this.initFaces();
		}
	},
	Font : {
		toggleStyle : function(e) {
			var currentClass = e.attr('class');
			var subIndex = currentClass.indexOf('_sel');
			if (subIndex > -1)
				return currentClass.substr(0, subIndex);
			else
				return currentClass + "_sel";
		},
		swapStyle : function(e, style) {
			e.attr('class', style);
		},
		setFontSize : function(value) {
			$client.els.inputBox.css('fontSize', value);
			$client.fireEvent('fontsizechanged', value);
		},
		changeFontWeight : function(t) {
			var area = $client.els.inputBox;
			var v = 'normal';
			if (t.attr('fontWeight') !== 'bold') {
				area.css('fontWeight', 'bold');
				t.attr('fontWeight', 'bold');
				v = 'bold';
			} else {
				area.css('fontWeight', 'normal');
				t.attr('fontWeight', 'normal');
			}
			this.swapStyle(t, this.toggleStyle(t));
			$client.fireEvent('fontweightchanged', v);
		},
		changeFontStyle : function(t) {
			var area = $client.els.inputBox;
			if (t.attr('fontStyle') !== 'italic') {
				area.css('fontStyle', 'italic');
				t.attr('fontStyle', 'italic');
			} else {
				area.css('fontStyle', 'normal');
				t.attr('fontStyle', 'normal');
			}
			this.swapStyle(t, this.toggleStyle(t));
			$client.fireEvent('fontstylechanged', area.css('fontStyle'));
		},
		changeFontDecoration : function(t) {
			var area = $client.els.inputBox;
			if (t.attr('textDecoration') !== 'underline') {
				area.css('textDecoration', 'underline');
				t.attr('textDecoration', 'underline');
			} else {
				area.css('textDecoration', 'none');
				t.attr('textDecoration', 'none');
			}
			this.swapStyle(t, this.toggleStyle(t));
			$client.fireEvent('fontdecorationchanged', area
							.css('textDecoration'));
		},
		toggle : function(t) {
			if ($client.els.faceDiv.css('display') != 'none')
				$client.Face.toggleDisplay();
			$client.els.fontCfgDiv.toggle();
			this.swapStyle(t, this.toggleStyle(t));
		},
		setFontColor : function(selectedColor) {
			$client.els.inputBox.css('color', selectedColor);
			$client.fireEvent('fontcolorchanged', selectedColor);
		},
		init : function() {
			var self = this;
			$client.els.fontCfgDiv.css('top', '-28px');
			$client.els.toolbarPosDiv.append($client.els.fontCfgDiv);
			$client.els.fontBtn.bind('click', function() {
						self.toggle($(this));
					});
			$client.els.fontSizeSel.bind('change', function() {
						self.setFontSize(this.value + "pt");
					});
			$client.els.fontWeightBtn.attr('fontWeight', 'normal').bind(
					'click', function() {
						self.changeFontWeight($(this));
					});
			$client.els.fontStyleBtn.attr('fontStyle', 'normal').bind('click',
					function() {
						self.changeFontStyle($(this));
					});
			$client.els.fontDecorateBtn.attr('textDecoration', 'none').bind(
					'click', function() {
						self.changeFontDecoration($(this));
					});
			if ($client.els.fontColorBtn.length) {
				$client.els.fontColorBtn.get(0).onclick = function() {
					if (!this.value)
						this.value = "000000";
					if (!this.color) {
						this.color = new jscolor.color(this, {
									pickerMode : 'HSV',
									pickerPosition : 'top',
									pickerFaceColor : 'transparent',
									pickerFace : 3,
									pickerBorder : 0,
									pickerInsetColor : 'black',
									valueElement : this,
									styleElement : $client.els.inputBox.get(0),
									onchange : function(color) {
										$client.fireEvent('fontcolorchanged',
												color);
									}
								});
					}
					this.color.showPicker();
				}
			};
		}
	},
	Resize : function(drag) {
		var _this = this;
		this.el = drag;
		$(this.el).css('cursor', 'e-resize');
		var leftLimit = $client.resizeLeft ? $client.resizeLeft : 180;
		var rightLimit = $client.resizeRight ? $client.resizeRight : 300;
		var dragMask = $('<div style="background:url('
				+ _APP_IMAGE_PATH
				+ 's.gif);height:100%;width:100%;position:absolute;z-index:9999"></div>');
		var _dragEl;
		var _bind = function(object, fun) {
			return function() {
				return fun.apply(object, arguments);
			}
		}
		var _bindEventListener = function(object, fun) {
			return function(event) {
				return fun.call(object, (event || window.event));
			}
		}
		this.start = function(e) {
			_dragEl = $('<div/>').css({
						height : '100%',
						width : $(this.el).css('width'),
						position : 'absolute',
						backgroundColor : '#CCC',
						right : $(this.el).css('right'),
						cursor : 'e-resize',
						zIndex : 99999
					})
			$(this.el).parent().append(_dragEl);
			$client.els.rightPanel.append(dragMask);
			if (document.all) {
				this.el.setCapture();
			} else {
				$(window).bind('blur', function(e) {
							_this.stop(e);
						});
				e.preventDefault();
			}
			this._x = e.clientX - this.el.offsetLeft;
			$(document).bind('mousemove', function(e) {
						_this.move(e);
					});
			$(document).bind('mouseup', function(e) {
						if (_this.el.releaseCapture)
							_this.el.releaseCapture();
						_this.stop(e);
					});
		}
		this.move = function(e) {
			window.getSelection
					? window.getSelection().removeAllRanges()
					: document.selection.empty();
			var _r = document.documentElement.clientWidth - e.clientX - this._x;
			var _minWidth = window._MIN_P4_WIDTH ? window._MIN_P4_WIDTH : 150;
			var _minDialogWidth = window._MIN_DIALOG_WIDTH ? window._MIN_DIALOG_WIDTH : 300;
			if (_r > _minWidth && _r < document.documentElement.clientWidth - _minDialogWidth) {
				_dragEl.css('right', _r);
			}
		}
		var _dr = parseInt($(this.el).css('right'));
		var lr = parseInt($client.els.leftPanel.css('right'));
		var rr = parseInt($client.els.rightPanel.css('right'));
		var rw = parseInt($client.els.rightPanel.css('width'));
		var lw = document.documentElement.clientWidth - rw
				- parseInt($(this.el).css('width'));
		this.stop = function() {
			$(document).unbind('mousemove');
			$(document).unbind('mouseup');
			var _r = parseInt(_dragEl.css('right'));
			var _offset = _r - _dr;
			_dragEl.remove();
			dragMask.remove();
			if ($client.config.isIE6) {
				$client.els.rightPanel.css('width', rw + _offset);
				$(_this.el).css('right', _r + _this.el.offsetWidth);
				try {
					adjustLayout("mainDiv", "panelDiv", 0);
					adjustLayout("panelDiv", "leftPanelDiv", 1);
					adjustLayout("leftPanelDiv", "outputDiv", 0);
					adjustLayout("inputDiv", "inputAreaDiv", 1);
					adjustLayout("rightPanelDiv", "activityDiv", 0);
					adjustLayout("activityDiv", "activityPanelDiv", 0);
					adjustLayout("outputDiv", "outputArea", 2);
				} catch (e) {
				}
			} else {
				$client.els.leftPanel.css('right', lr + _offset);
				$client.els.rightPanel.css('width', rw + _offset);
				$(_this.el).css('right', _r);
				$client.els.rightPanel.css('right', rr);
			}
			var ulEl = $('ul:first-child', $client.els.appTabDiv);
			var tabsWidth = parseInt($client.els.appTabDiv.css('width'));
			var liNum = $('li', ulEl).length;
			if (liNum * 79 > tabsWidth) {
				$client.els.tabLeftBtn.show();
				$client.els.tabRightBtn.show();
				ulEl.css('width', liNum * 79);
			} else {
				$client.els.tabLeftBtn.hide();
				$client.els.tabRightBtn.hide();
				ulEl.css('width', tabsWidth);
			}
		}
		this._x = this._y = 0;
		$(this.el).bind('mousedown', function(e) {
			var isCollapseClicked = $(e.target).parent().attr('id') == 'collapseBtn';
			var isMinSize = $client.els.collapseBtn.attr('collapsed') == 'true';
			if (!isCollapseClicked && !isMinSize)
				_this.start(e)
		});
	},
	Collapse : {
		init : function() {
			var _this = $client;
			var _rp = _this.els.rightPanel;
			var _lp = _this.els.leftPanel;
			var _ps = _this.els.panelSpacer;
			var _rs = _this.els.rightSpacer;
			var _ap = _this.els.appDiv;
			var _ip = _this.els.introDiv;
			var _dd = _this.els.dialogDiv.get(0);
			var _btn = $('<div/>')
					.attr('id', 'collapseBtn')
					.html('<img src="images/hover_right.gif" class="collapseBtnImg" />')
					.appendTo(_ps);
			_this.els.collapseBtn = _btn;
			_this.els.collapseBtn.bind('mousedown', function(e) {
						var ele = $(this);
						var collapsed = ele.attr('collapsed');
						if (typeof collapsed == 'undefined'
								|| collapsed == 'false') {
							if (typeof _this.config.resizable == 'undefined'
									|| _this.config.resizable)
								_ps.css('cursor', 'default');
							var _rpw = parseInt(_rp.css('width'));
							ele.attr('rightWidth', _rpw);
							_rp.css('width', 0);
							_ip.hide();
							_rs.hide();
							_ap.hide();
							if (_this.config.isIE6) {
								try {
									adjustLayout("mainDiv", "panelDiv", 0);
									adjustLayout("panelDiv", "leftPanelDiv", 1);
									adjustLayout("leftPanelDiv", "outputDiv", 0);
									adjustLayout("inputDiv", "inputAreaDiv", 1);
									adjustLayout("rightPanelDiv",
											"activityDiv", 0);
									adjustLayout("activityDiv",
											"activityPanelDiv", 0);
									adjustLayout("outputDiv", "outputArea", 2);
								} catch (e) {
								}
							} else {
								_ps.css('right', parseInt(_ps.css('right'))
												- _rpw)
								_lp.css('right', parseInt(_lp.css('right'))
												- _rpw)
							}
							$('img', ele).attr('src', 'images/hover_left.gif')
							ele.attr('collapsed', 'true');
						} else {
							if (typeof _this.config.resizable == 'undefined'
									|| _this.config.resizable)
								_ps.css('cursor', 'e-resize');
							var _ow = parseInt(ele.attr('rightWidth'));
							_rp.css('width', _ow);
							_ip.show();
							_rs.show();
							_ap.show();
							if (_this.config.isIE6) {
								try {
									adjustLayout("mainDiv", "panelDiv", 0);
									adjustLayout("panelDiv", "leftPanelDiv", 1);
									adjustLayout("leftPanelDiv", "outputDiv", 0);
									adjustLayout("inputDiv", "inputAreaDiv", 1);
									adjustLayout("rightPanelDiv",
											"activityDiv", 0);
									adjustLayout("activityDiv",
											"activityPanelDiv", 0);
									adjustLayout("outputDiv", "outputArea", 2);
								} catch (e) {
								}
							} else {
								_ps.css('right', parseInt(_ps.css('right'))
												+ _ow);
								_lp.css('right', parseInt(_lp.css('right'))
												+ _ow);
							}
							$('img', ele).attr('src', 'images/hover_right.gif');
							ele.attr('collapsed', 'false');
							_dd.scrollTop = _dd.scrollHeight;
						}
					});
		}
	}
}

$client.Suggest = (function() {
	var textbox = null;
	var requestKeyNew = "";
	var table = null;
	var tableStyle = null;
	var loadCssStyleFlg = false;
	var tableRows = null;
	var mouseoverflg = 0;
	selectIndex = -1, itemonmousedownFlg = 0;
	var selectRow = null;
	var keyDownCount = 0;
	var pressKeyUpOrKeyDownFlg = 0
	var textboxCurrentValue;
	var ba = null;
	var loopHttpRequestAddress = null;
	var requestErrorCount = 0;
	var requestKeyCurrent;
	var requestCount = 0;
	var responseKey = "";
	var timeoutVisibilityHidden = null;
	var callSuggestHandlerKey = "";
	function dispose() {
		responseKey = "";
		textbox = null;
		requestKeyNew = "";
		table = null;
		tableStyle = null;
		loadCssStyleFlg = false;
		tableRows = null;
		mouseoverflg = 0;
		selectIndex = -1, itemonmousedownFlg = 0;
		selectRow = null;
		keyDownCount = 0;
		window.clearTimeout(loopHttpRequestAddress);
		window.clearInterval(ba);
		loopHttpRequestAddress = null;
		requestErrorCount = 0;
		requestCount = 0;
	}
	function init(vtextbox) {
		vtextbox = $client.els.inputBox;
		textbox = vtextbox;
		requestKeyNew = textboxCurrentValue = requestKeyCurrent = textbox.val();
		if (!vtextbox.inited) {
			textbox.attr("autocomplete", "off");
			bind(textbox, "blur", function() {
						pressKeyUpOrKeyDownFlg || visibilityHidden();
						pressKeyUpOrKeyDownFlg = 0
					});
			bind(textbox, "beforedeactivate", function(e) {
						if (itemonmousedownFlg) {
							if (window.event) {
								window.event.cancelBubble = true;
								window.event.returnValue = false
							} else {
								e.stopPropagation();
								e.proventDefault();
							}
						}
						itemonmousedownFlg = 0
					});
			bind(textbox, "keydown", function(keyArg) {
						var keyCode = keyArg.keyCode;
						if (keyCode == 27 && isVisibilityVisible()) {
							visibilityHidden();
							setTextboxValue(requestKeyNew);
							keyArg.cancelBubble = true;
							return keyArg.returnValue = false
						}
						if (keyCode == 38 || keyCode == 40) {
							keyDownCount++;
							keyDownCount % 3 == 1 && keydownupHandler(keyCode);
							return false;
						}
					});
			bind(textbox, "keyup", function(keyArg) {
						var keyCode = keyArg.keyCode;
						if (keyDownCount == 0
								&& !(keyCode == 38 || keyCode == 40))
							keydownupHandler(keyCode);
						keyDownCount = 0;
						return false;
					});
			table = document.createElement("table");
			table.cellSpacing = table.cellPadding = "0";
			tableStyle = table.style;
			table.className = "gac_m";
			document.body.appendChild(table);
			vtextbox.inited = true;
		}
		if (!loadCssStyleFlg) {
			loadCssStyle();
			loadCssStyleFlg = true;
		}
		visibilityHidden();
		setPopupTablePosition();
		requestKeyNew = textboxCurrentValue = requestKeyCurrent = textbox.val();
		ba = window.setInterval(function() {
					var l = textbox.val();
					l != textboxCurrentValue && keydownupHandler(0);
					textboxCurrentValue = l
				}, 10);
		loopHttpRequest();
	}

	function keydownupHandler(keyCode) {
		if (keyCode == 38 || keyCode == 40) {
			pressKeyUpOrKeyDownFlg = 1;
			window.setTimeout(function() {
						textbox.focus()
					}, 10)
		}
		if (textbox.val() != textboxCurrentValue) {
			requestKeyNew = $.trim(textbox.val());
		}
		keyCode == 40 && showSelectItem(selectIndex + 1);
		keyCode == 38 && showSelectItem(selectIndex - 1);
		setPopupTablePosition();
		if (responseKey != requestKeyNew && !timeoutVisibilityHidden)
			timeoutVisibilityHidden = window.setTimeout(visibilityHidden, 100);
		textboxCurrentValue = textbox.val();
		textboxCurrentValue == "" && !loopHttpRequestAddress
				&& loopHttpRequest()
	}
	function showSelectItem(a) {
		if (!responseKey && requestKeyNew) {
			requestKeyCurrent = "";
			loopHttpRequest();
			return;
		}
		if (requestKeyNew != responseKey || !loopHttpRequestAddress)
			return;
		if (!tableRows || tableRows.length <= 0)
			return;
		if (!isVisibilityVisible()) {
			visibilityVisible();
			return;
		}
		var b = tableRows.length - 1;
		if (selectRow)
			selectRow.className = "gac_a";
		if (a == b || a == -1) {
			selectIndex = -1;
			setTextboxValue(requestKeyNew);
			textbox.focus();
			return;
		} else if (a > b)
			a = 0;
		else if (a < -1)
			a = b - 1;
		selectIndex = a;
		selectRow = tableRows.item(a);
		selectRow.className = "gac_b";
		$client.lastSuggestChoice = setTextboxValue(selectRow.completeString);
	}
	function loadCssStyle() {
		var a = [];
		function b(l, r) {
			a.push(l, "{", r, "}")
		}
		b(
				".gac_m",
				"cursor:default;border:1px solid #90a8c1;z-index:10001;background:#f2f7fe;position:absolute;margin:0;font-size:12px");
		b(".gac_m td", "line-height:15px");
		b(".gac_b", "background:#355fb1;color:#fff");
		var k = "padding-left:3px;white-space:nowrap;overflow:hidden;text-align:left;padding-bottom:1px";
		b(".gac_c", k);
		b(
				".gac_d",
				"padding:0 3px; white-space:nowrap;overflow:hidden;text-align:right;color:green;font-size:0.77em");
		b(".gac_b td", "color:#fff");
		b(
				".gac_e",
				"padding:0 3px 2px;text-decoration:underline;text-align:right;color:#00c;font-size:0.77em;line-height:0.88em");
		a = a.join("");
		var f = document.createElement("style");
		f.setAttribute("type", "text/css");
		document.getElementsByTagName("head")[0].appendChild(f);
		if (f.styleSheet) {
			f.styleSheet.cssText = a;
		} else {
			f.appendChild(document.createTextNode(a));
		}
	}
	function loopHttpRequest() {
		if (requestErrorCount >= 3)
			return;
		if (requestKeyCurrent != requestKeyNew && requestKeyNew) {
			var code13 = requestKeyNew.replace(new RegExp("\n", "g"), "");
			if (code13 && requestKeyNew != $client.config.defaultContent) {
				requestCount++;
				httpRequest(code13);
				textbox.focus();
			}
		}
		requestKeyCurrent = requestKeyNew;
//		var a = 50;
//		for (var b = 1; b <= (requestCount - 2) / 2; ++b)
//			a *= 2;
//		a += 50;
		loopHttpRequestAddress = window.setTimeout(loopHttpRequest, 300);
	}
	function httpRequest(a) {
		a = $.trim(a);
		callSuggestHandlerKey = a;
		var _faces = $client.Face.faceKeys;
		for (var i = 0; i < _faces.length; i++) {
			if (_faces[i] == a)
				return false;
		}
		if ($client.session)
			$client.session.sendMessage("suggest:" + a);
	}
	function visibilityHidden() {
		if (timeoutVisibilityHidden) {
			window.clearTimeout(timeoutVisibilityHidden);
			timeoutVisibilityHidden = null
		}
		tableStyle && (tableStyle.visibility = "hidden");
	}
	function visibilityVisible() {
		tableStyle && (tableStyle.visibility = "visible");
		setPopupTablePosition();
		mouseoverflg = 1
	}
	function isVisibilityVisible() {
		return !!tableStyle && tableStyle.visibility == "visible"
	}
	function deleteAllTableRows() {
		if (table) {
			while (table.rows.length)
				table.deleteRow(-1);
		}
	}
	function setPopupTablePosition() {
		if (table) {
			var rawTxtBox = textbox.get(0);
			var tb = $client.els.toolBar.get(0);
			tableStyle.top = $client.utils.getAbsoluteTop(tb)
					- table.rows.length * 18 + 2 + 'px';
			tableStyle.left = $client.utils.getAbsoluteLeft(tb) + 1 + 'px';
			document.body.appendChild(table);
			tableStyle.width = rawTxtBox.offsetWidth + "px";
		}
	}
	function getOffsetFullValue(a, b) {
		var d = 0;
		while (a) {
			d += a[b];
			a = a.offsetParent
		}
		return d
	}
	function appendTextNode(a, b) {
		a.innerHTML = b;
	}
	function createDataRowItems(a) {
		requestCount > 0 && requestCount--;
		if (!table || callSuggestHandlerKey != requestKeyNew)
			return;
		if (timeoutVisibilityHidden) {
			window.clearTimeout(timeoutVisibilityHidden);
			timeoutVisibilityHidden = null;
		}
		responseKey = callSuggestHandlerKey;
		deleteAllTableRows();
		var showCloseFlg = false;
		for (var k = 0, f; k < a.length; k++) {
			if (f = a[k]) {
				showCloseFlg = true;
				var drNew = table.insertRow(-1);
				drNew.onclick = function() {
					setTextboxValue(this.completeString);
					visibilityHidden();
					$client.sendText('_inType_fuzzy');
				};
				drNew.onmousedown = itemonmousedown;
				drNew.onmouseover = itemonmouseover;
				drNew.onmousemove = function() {
					if (mouseoverflg) {
						mouseoverflg = 0;
						itemonmouseover.call(this)
					}
				};
				drNew.completeString = f;
				drNew.className = "gac_a";
				var tdKeyWord = document.createElement("td");
				appendTextNode(tdKeyWord, f);
				tdKeyWord.className = "gac_c";
				tdKeyWord.style.paddingTop = "2px";
				drNew.appendChild(tdKeyWord);
				var tdResult = document.createElement("td");
				appendTextNode(tdResult, "");// appendTextNode(tdResult,
				// f[1]);//显示结果条数
				tdResult.className = "gac_d";
				drNew.appendChild(tdResult);
			}
		}
		if (showCloseFlg) {
			var drNew = table.insertRow(-1);
			drNew.onmousedown = itemonmousedown;
			var td = document.createElement("td");
			td.colSpan = 2;
			td.style.cursor = "pointer";
			td.align = 'right';
			td.style.fontSize = '12px';
			td.style.paddingRight = '2px';
			td.innerHTML = '关闭';
			drNew.className = "gac_e";
			drNew.appendChild(td);
			td.onclick = function() {
				visibilityHidden();
				responseKey = "";
				window.clearTimeout(loopHttpRequestAddress);
				loopHttpRequestAddress = null;
			}
		}
		selectIndex = -1;
		tableRows = table.rows;
		(tableRows && tableRows.length > 0
				? visibilityVisible
				: visibilityHidden)();
	}
	function setTextboxValue(newValue) {
		var divEle = document.createElement("div");
		divEle.innerHTML = newValue;
		if (textbox) {
			var val = divEle.innerText || divEle.textContent;
			textbox.val(val);
			textboxCurrentValue = val;
			return val;
		}
	}
	function itemonmousedown(a) {
		itemonmousedownFlg = 1;
		return false
	}
	function itemonmouseover() {
		if (mouseoverflg)
			return;
		if (selectRow)
			selectRow.className = "gac_a";
		this.className = "gac_b";
		selectRow = this;
		for (var a = 0, b; b = tableRows[a]; a++)
			b == selectRow && (selectIndex = a)
	}
	function bind(element, event, eventhandler) {// 给元素绑定事件
		element.bind(event, eventhandler);
	};
	bind($(window), "resize", setPopupTablePosition);// 改变窗体大小时, 重新定位table
	_sendMessage = function(message) {
		$client.sendText();
	};
	return {
		init : init,
		aiTipCallback : createDataRowItems,
		visibilityHidden : visibilityHidden
	};
})();

$client.on('inputfocus', function() {
			this.els.inputBox.attr('class', 'inputbox_active_style');
			if (this.els.inputBox.val() == this.config.defaultContent)
				this.els.inputBox.val('');
		}, $client);
$client.on('inputblur', function() {
			this.els.inputBox.attr('class', 'inputbox_default_style');
			if (this.els.inputBox.val() == '')
				this.els.inputBox.val(this.config.defaultContent);
		}, $client);
$client.on('logincompleted', function(user) {
			var _this = this;
			if (user) {
				if (!this.user)
					this.user = user;
				var _btn = this.els.userLoginBtn;
				_btn.attr('title', '');
				if (user.length > 11) {
					_btn.attr('title', user);
					user = user.substring(0, 11);
				}
				_btn.html(($client.user && $client.user.userName)
						? $client.user.userName
						: user).unbind('click');
				_btn.css('cursor', 'auto');
				var logoutBtn = $('<a></a>');
				logoutBtn.css('cursor', 'pointer').attr('id', 'logoutBtn')
						.html('[退出]').appendTo(_btn.parent()).bind('click',
								function() {
									_this.logout();
									return false;
								});
				this.Activity.closeP4("用户登录");
				_this.fireEvent('userloggedin', user);
			} else {
				alert("登录失败！");
			}
		}, $client);
$client.on('beforeappopen', function(ele, name, url) {
			ele.className = 'p4tabs_panel';
		});
RobotWebClient = $client;

Channel_Login = function(user) {
	$client.user = user;
	if (user && user.userName) {
		$client.config.defaultMyName = $client.config.myName;
		$client.config.myName = user.userName;
	}
	if ($client.session) {
		if (user && user.userId && user.userId.length > 0)
			$client.session.login(user.userId + " " + JSON.toJson(user));
	}
}
Channel_SendIM = function(msg) {
	if ($client.session) {
		$client.showMessage($client.config.myName, msg, null, null, false);
		$client.sendMessage(msg)
	}
}
$(function() {
			if (window._CLIENT_INIT)
				window._CLIENT_INIT();
			$client.init();
			if (window._CLIENT_INITED)
				window._CLIENT_INITED();
		});

/** external apis */
$client.external = {
	login : function(userid, userInfo, cb) {
		var url = _ROOT_PATH + "check-login.action?ticket="
				+ encodeURIComponent(userid);
		$.getJSON(url + '&jsoncallback=?', {}, function(user) {
					if (user && user.error) {
						$(document).mask('登录后才可以访问！');
					} else {
						Channel_Login(user)
						if (userInfo) {
							if (userInfo.location) {
								$client.session.sendMessage('setloc:' + userInfo.location);
								$client.addDim('location', userInfo.location);
							}
							if (userInfo.brand) {
								$client.session.sendMessage('setbrd:' + userInfo.brand);
								$client.addDim('brand', userInfo.brand);
							}
						}
					}
					if (cb)
						cb(user);
				});
	},
	logout : function(cb) {
		$.getJSON(_ROOT_PATH + "logout.action?ts=" + new Date().getTime()
						+ '&jsoncallback=?', {}, function(resp) {
					if (resp) {
						var logoutImg = $('#logoutImg');
						if (logoutImg.length)
							logoutImg.attr('src', resp);
						else
							$('<img/>').css('display', 'none')
									.attr('src', resp).attr('id', 'logoutImg')
									.appendTo($('body'));
					}
					var _this = $client;
					var _user = _this.user;
					if (_user && _user.userId && _user.userId.length > 0) {
						_this.session.logout(_user.userId + " "
								+ JSON.toJson(_user));
					}
					if (_user && _user.userName)
						_this.config.myName = _this.config.defaultMyName;
					if (_user && _user.userId)
						_this.fireEvent('userloggedout', _user.userId);
					if (cb) {
						cb();
					}
				});
	},
	feedback : function(type, advice, reason, cb) {
		var url = _ROOT_PATH + "p4pages/feedback.action?";
		var qs = 'type=' + type + '&advice=' + encodeURIComponent(advice)
				+ '&reason=' + encodeURIComponent(reason);
		$.getJSON(url + qs + '&async=true&jsoncallback=?', {}, function(resp) {
					if (!resp) {
						alert('登录后才可以访问！');
					} else {
						if (cb)
							cb();
					}
				});
	},
	leaveword : function(type, content, addr, cb) {
		var url = _ROOT_PATH + "p4pages/leaveword.action?";
		var qs = 'type=' + type + '&content=' + encodeURIComponent(content)
				+ '&addr=' + addr;
		$.getJSON(url + qs + '&async=true&jsoncallback=?', {}, function(resp) {
					if (!resp) {
						alert('登录后才可以访问！');
					} else {
						if (cb)
							cb();
					}
				});
	},
	relatedQuestions : function(question, location, platform, maxReturn, cb) {
		var url = _ROOT_PATH + "p4pages/related-question.action?";
		var qs = 'question=' + encodeURIComponent(question) + '&location='
				+ location + '&platform=' + platform + '&maxReturn='
				+ maxReturn;
		$.getJSON(url + qs + '&format=json&jsoncallback=?', {}, function(rqs) {
					if (cb)
						cb(rqs);
				});
	},
	hotQuestions : function(location, platform, maxReturn, cb) {
		var url = _ROOT_PATH + "p4pages/hot-question.action?";
		var qs = 'location=' + location + '&platform=' + platform
				+ '&maxReturn=' + maxReturn;
		$.getJSON(url + qs + '&format=json&jsoncallback=?', {}, function(hqs) {
					if (cb)
						cb(hqs);
				});
	},
	acsInvite : function() {
		var _this = $client;
		if (_this.session)
			_this.session.sendMessage('acs');
	},
	acsBye : function() {
		var _this = $client;
		if (_this.session)
			_this.session.sendMessage('SYS::{"handler.name":"acs", "input":"exit"}');
	},
	openP4 : function(name, url) {
		$client.Activity.openP4(url, name);
	},
	closeP4 : function(name) {
		$client.Activity.closeP4(name);
	},
	setLocation : function(cityCode) {
		var _this = $client;
		if (_this.session) {
			_this.session.sendMessage("setloc:" + cityCode);
			$client.addDim('location', cityCode);
		}
	},
	setBrand : function(brand) {
		var _this = $client;
		if (_this.session) {
			_this.session.sendMessage("setbrd:" + brand);
			$client.addDim('brand', brand);
		}
	},
	setDims : function(dimCfg) {
		var _this = $client;
		if (_this.session && dimCfg) {
			_this.session.sendMessage("setdims:" + JSON.toJson(dimCfg));
			for (var k in dimCfg) {
				$client.addDim(k, dimCfg[k]);
			}
		}
	},
	setUserType : function(type) {
		var _this = $client;
		if (_this.session) {
			_this.session.sendMessage("set_ai_filter_attr:usertype " + type);
		}
	}
};

$client.FlashStore = function(swfURL, name) {
	var FlashStore = $client.FlashStore;
	var test=function() {
		if(FlashStore.oFlash) return true;
		var version=0;
		var swfHTML;
		if(navigator.plugins && navigator.mimeTypes.length){
			var x = navigator.plugins["Shockwave Flash"];
			if(x && x.description) {
				version = parseInt(x.description.replace(/([a-zA-Z]|\s)+/, "").replace(/(\s+r|\s+b[0-9]+)/, ".").split(".")[0]);
			}			
			swfHTML='<embed id="_persist_flash" height=1 width=1 type="application/x-shockwave-flash" src="'+swfURL+'" allowScriptAccess="always"/>'
		}else{
			try{
				var axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7");
			}catch(e){
				try {
						var axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6");
						version=6
					try {
						axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash");
					} catch(e) {}
				} catch(e) {}
			}
			if (axo != null) {
				version = parseInt(axo.GetVariable("$version").split(" ")[1].split(",")[0]);
			}
			swfHTML='<object id="_persist_flash" style="position:absolute;left:-1024px" height=1 width=1 classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"><param name="movie" value="'+swfURL+'"/><param name="allowScriptAccess" value="always" /></object>'
		}
		var result = (version >= 8)
		if (result) {
			if(navigator.plugins && navigator.mimeTypes.length) {
				var storeFrame=document.createElement("iframe");
				storeFrame.style.width='0px'
				storeFrame.style.height='0px'
				storeFrame.scrolling = "no";
				storeFrame.frameBorder = "no";
				storeFrame.style.position='absolute'
				storeFrame.style.left='-1024px'
				document.body.appendChild(storeFrame)
				doc=storeFrame.contentWindow.document
				doc.open();
				doc.write("<html><head><script>function FlashStore_OnLoad(){window.parent.FlashStore_OnLoad()}</script></head><body>"+swfHTML+"</body></html>")
				doc.close();
				FlashStore.oFlash = doc.getElementById('_persist_flash');
			}
			 else {
				 document.write(swfHTML);
				 FlashStore.oFlash = document.getElementById('_persist_flash');
			 }
		}
		return result;
	}
	if(!test() && name)throw new Error("not support flash store");
	
	if(!name)return;
	
	var self=this;
	var esc = function(str) {
		return 'PS' + str.replace(/_/g, '__').replace(/ /g, '_s');
	};

	this.get=function(key) {
		var val;
		try {
			key=esc(key);
			var json=FlashStore.oFlash.get(name, key);
			var entry;
			if(json)entry=JSON.fromJson(json);
			if(entry && !entry.expires || new Date().getTime()<entry.expires)
				val=entry.value
		}catch(e){
			if(window.logger)window.logger.log("get:"+name+"."+key,e.description);
		}
		return val;
	}
	this.set=function(key, val, expires) {
		try{
			key=esc(key);
			var entry={}
			if(val)entry.value=val
			if(expires)entry.expires=expires
			json=JSON.toJson(entry).replace(/\\/g,"\\\\")
			FlashStore.oFlash.set(name, key, json);
		}catch(e){
			if(window.logger)window.logger.log("set:"+name+"."+key,e.description);
		}
	}
	this.remove=function(key) {
		try {
			key = esc(key);
			FlashStore.oFlash.remove(name, key);
		}catch(e){
			if(window.logger)window.logger.log("remove:"+name+"."+key,e.description);
		}
	}
}
uinfoStore = new $client.FlashStore(_APP_SCRIPT_PATH + 'flashstore.swf', 'uinfo');