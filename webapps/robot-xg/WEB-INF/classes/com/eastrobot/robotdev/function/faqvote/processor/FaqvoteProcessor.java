package com.eastrobot.robotdev.function.faqvote.processor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.eastrobot.robotdev.function.FunctionInterface;
import com.eastrobot.robotdev.function.utils.StringUtil;
import com.eastrobot.robotnerve.Channel;
import com.eastrobot.robotnerve.Event;
import com.eastrobot.robotnerve.HandlerContext;
import com.eastrobot.robotnerve.InterceptorChain;
import com.eastrobot.robotnerve.app.HandlerHelper;
import com.eastrobot.robotnerve.events.CommandEvent;
import com.eastrobot.robotnerve.events.TextEvent;
import com.eastrobot.robotnerve.throughput.HandlerOutput;
import com.eastrobot.robotnerve.throughput.SendAction;
import com.eastrobot.robotnerve.throughput.TextSendAction;
import com.incesoft.ibotsdk.RobotSession;

/**
 * 解决未解决解决序号处理方法
 * 需要robot的拦截器Interceptor ，不能使用HandlerInterceptor
 * 
 * @author Mike.Peng
 * @time Aug 28, 2015  4:22:08 PM
 */
public class FaqvoteProcessor implements FunctionInterface {
	
	private FaqvoteProcessor() {}  
    private static final FaqvoteProcessor processor = new FaqvoteProcessor();  
    public static FaqvoteProcessor getInstance() {
        return processor;  
    }
    
	public void afterProcessor(Channel channel, Event event) {
		if (null != event && (event instanceof TextEvent || event instanceof CommandEvent)){
			HandlerContext context = HandlerContext.getContext();
			HandlerOutput aioutput = context.getHandlerOutput();
			System.out.println("进入拦截器");
			if (aioutput != null) {
				List<SendAction> actions = aioutput.getActions();
				for (SendAction act : actions) {
					if (act instanceof TextSendAction) {
						Integer answerType = (Integer) ((RobotSession) channel.getRealSession()).getAttribute("key_answer_type");
						String answer = ((TextSendAction) act).getText();
						
						if (answerType == 1) {
							if (answer != null && !"".equals(answer.trim()) && answer.contains("[faqvote.hide]") && answer.contains("[faqvote.seq]")) {
								List<String> hides = StringUtil.getTag(answer, "faqvote.hide");
								List<String> seqs = StringUtil.getTag(answer, "faqvote.seq");
								channel.setAttribute("faqvote." + seqs.get(0), hides.get(0));
								channel.setAttribute("faqvote." + seqs.get(1), hides.get(1));
								answer = StringUtil.removeTag(answer, "faqvote.hide");
								for (String seq : seqs) {
									answer = answer.replace("[faqvote.seq]" + seq + "[/faqvote.seq]", seq);
								}
								((TextSendAction) act).setText(answer);
							}
						} else {
							removeSeq(channel);
						}
					}
				}
			}
		}
	}

	@Override
	public boolean processor(Channel channel, Event event) {
		boolean isContinue = Boolean.TRUE;
		
		if (null != event && (event instanceof TextEvent || event instanceof CommandEvent)){
			HandlerHelper helper = HandlerHelper.getInstance();
			String input = helper.ctx().getInput();
			
			Map<String , Object> attrs = channel.getAttributes();
			for (String key : attrs.keySet()) {
				if (key.startsWith("faqvote")) {
					String seq = key.replace("faqvote.", "");
					if (seq.equalsIgnoreCase(input)) {
						Object faqvoteInput = channel.getAttribute(key);
						if (faqvoteInput != null) {
							helper.nav.to0("faqVoteHandler",String.valueOf(faqvoteInput));
							isContinue = Boolean.FALSE;
						}
					}
				}
			}
		}
		
		//不管哪种event，缓存必须清除
		removeSeq(channel);
		
		return isContinue;
	}
	
	public void beforeProcessor(Channel channel, Event event) {
		
	}
	
	private void removeSeq(Channel channel){
		List<String> list = new ArrayList<String>();
		Map<String , Object> attrs = channel.getAttributes();
		for (String key : attrs.keySet()) {
			if (key.startsWith("faqvote")) {
				list.add(key);
			}
		}
		
		for (String key : list) {
			channel.removeAttribute(key);
		}
	}

	@Override
	public boolean processor(Channel channel, Event event,
			InterceptorChain chain) {
		// TODO Auto-generated method stub
		return false;
	}
}
