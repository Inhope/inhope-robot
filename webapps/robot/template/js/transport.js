var appBaseURI = '';
function FlashTransport() {
	var test = function() {
		if (FlashTransport.oFlash)
			return true;
		var swfURL = appBaseURI + _APP_SCRIPT_PATH + 'transport.swf';
		var version = 0;
		var swfHTML;
		if (navigator.plugins && navigator.mimeTypes.length) {
			var x = navigator.plugins["Shockwave Flash"];
			if (x && x.description) {
				version = parseInt(x.description.replace(/([a-zA-Z]|\s)+/, "").replace(/(\s+r|\s+b[0-9]+)/, ".").split(".")[0]);
			}
			swfHTML = '<embed style="position:absolute;left:-1024px;top:-1024px;" id="_transport_flash" height=1 width=1 type="application/x-shockwave-flash" src="'
					+ swfURL + '" allowScriptAccess="always"/>'
		} else {
			try {
				var axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7");
			} catch (e) {
				try {
					var axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6");
					version = 6
					try {
						axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash");
					} catch (e) {
					}
				} catch (e) {
				}
			}
			if (axo != null) {
				version = parseInt(axo.GetVariable("$version").split(" ")[1].split(",")[0]);
			}
			swfHTML = '<object id="_transport_flash" allowScriptAccess="always" style="position:absolute;left:-1024px;top:-1024px;" height=1 width=1 classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"><param name="movie" value="'
					+ swfURL + '"/><param name="allowScriptAccess" value="always" /></object>'
		}
		var result = (version >= 8)
		if (result) {
			// if (navigator.plugins && navigator.mimeTypes.length) {
			if (false) {
				var storeFrame = document.createElement("iframe");
				storeFrame.style.width = '0px'
				storeFrame.style.height = '0px'
				storeFrame.scrolling = "no";
				storeFrame.frameBorder = "no";
				storeFrame.style.position = 'absolute'
				storeFrame.style.left = '-1024px'
				document.body.appendChild(storeFrame)
				doc = storeFrame.contentWindow.document
				doc.open();
				doc.write("<html><body>" + swfHTML + "</body></html>")
				doc.close();
				FlashTransport.oFlash = doc.getElementById('_transport_flash');
			} else {
				document.write(swfHTML)
				FlashTransport.oFlash = document.getElementById('_transport_flash');
			}
		} else {
			throw new Error("Flash player N/A.");
		}
		return result;
	}

	if (!test() && name)
		throw new Error("not support flash transport");

	if (!name)
		return;
}

JSON = {
	toJson : function(obj) {
		var m = {
			'\b' : '\\b',
			'\t' : '\\t',
			'\n' : '\\n',
			'\f' : '\\f',
			'\r' : '\\r',
			'"' : '\\"',
			'\\' : '\\\\'
		}, s = {
			array : function(x) {
				var a = [ '[' ], b, f, i, l = x.length, v;
				for (i = 0; i < l; i += 1) {
					v = x[i];
					f = s[typeof v];
					if (f) {
						v = f(v);
						if (typeof v == 'string') {
							if (b) {
								a[a.length] = ',';
							}
							a[a.length] = v;
							b = true;
						}
					}
				}
				a[a.length] = ']';
				return a.join('');
			},
			'boolean' : function(x) {
				return String(x);
			},
			'null' : function(x) {
				return null;
			},
			number : function(x) {
				return isFinite(x) ? String(x) : 'null';
			},
			object : function(x) {
				if (x) {
					if (x instanceof Array) {
						return s.array(x);
					}
					var a = [ '{' ], b, f, i, v;
					for (i in x) {
						v = x[i];
						f = s[typeof v];
						if (f) {
							v = f(v);
							if (typeof v == 'string') {
								if (b) {
									a[a.length] = ',';
								}
								a.push(s.string(i), ':', v);
								b = true;
							}
						}
					}
					a[a.length] = '}';
					return a.join('');
				}
				return null;
			},
			string : function(x) {
				if (/["\\\x00-\x1f]/.test(x)) {
					x = x.replace(/([\x00-\x1f\\"])/g, function(a, b) {
						var c = m[b];
						if (c) {
							return c;
						}
						c = b.charCodeAt();
						return '\\u00' + Math.floor(c / 16).toString(16) + (c % 16).toString(16);
					});
				}
				return '"' + x + '"';
			}
		};
		if (typeof obj == "array") {
			return s.array(obj)
		} else {
			return s.object(obj)
		}
	},
	fromJson : function(s) {
		var r
		eval("r=" + s)
		return r;
	}
}
// >>>Utils>>>
var Utils = {
	fromJson : function(jsonString, instance) {
		jsonString = jsonString.replace(/\r\n/ig, '\\n');
		jsonString = jsonString.replace(/\n/ig, '\\n');
		jsonString = jsonString.replace(/\r/ig, '\\n');
		var obj;
		try {
			obj = eval('(' + jsonString + ')');
		} catch (e) {
			alert('mal-format transport data!\r\n' + jsonString);
		}
		if (!obj)
			return;
		if (!instance)
			return obj;

		if (typeof instance == 'function') {
			instance = new instance();
		}
		for ( var key in obj) {
			instance[key] = obj[key];
		}
	},
	toJson : function(obj) {
		return JSON.toJson(obj);
	}
}
function $d(obj, des) {
	if (window.console && window.console.debug) {
		console.debug("::" + (des ? des : ''), obj);
	} else {
		// alert("::" + (des ? des : '') + "|" + obj);
	}
}
// <<<Utils<<<
// >>> protocol >>>
function Packet(type) {
	this.type = type;
	this.sequence = -1;
	this.callId = -1;
	this.payload = null;
}

Packet.TYPE_AUTH = 0;
Packet.TYPE_MESSAGE = 1;
Packet.TYPE_REQUEST = 2;
Packet.TYPE_RESPONSE = 3;
Packet.TYPE_ERROR = 4;
Packet.TYPE_KEEPALIVE = 5;
function ConnectionEventHandler() {
	this.onMessage = function(body) {
	}

	this.onError = function(body) {
	}

	this.onConnected = function() {
	}

	this.onDisconnected = function() {
	}
	this.onRequest = function(body) {
	}
	this.onResponse = function(body) {
	}
}
function Connection(transport) {
	var _conn = this;
	/**
	 * @type ConnectionEventHandler
	 */
	var handler;
	/**
	 * 
	 * @param {ConnectionEventHandler}
	 *            h
	 */
	this.setEventHandler = function(h) {
		handler = h;
	}

	this.state = null;
	var getTransport = function() {
		return transport;
	}

	this.configConnection = function(host, port) {
		getTransport().configConnection({
			host : host,
			port : port
		});
	}

	this.setCompressThreshold = function(t) {
		getTransport().setCompressThreshold(t);
	}

	this.connect = function() {
		this.disconnByClient = null;
		if (this.state != Connection.STA_CONNECTED) {
			getTransport().connect();
		}
	}
	this.disconnByClient = null;
	this.disconnect = function() {
		this.disconnByClient = 1;
		if (this.state == Connection.STA_CONNECTED) {
			getTransport().disconnect();
		}
		handler.onDisconnected();
	}

	var seq = 1;
	/**
	 * 
	 * @param p{Packet}
	 */
	var sendPacket = function(p) {
		if (p.type != Packet.TYPE_KEEPALIVE) {
			p.sequence = seq++;
		}
		p.payload = Utils.toJson(p.payload);
		// console.debug(p)
		if (_conn.state == Connection.STA_CONNECTED) {
			getTransport().sendPacket(p);
		}
	}
	this.authenticate = function(bodyData, callback) {
		if (callback == null) {
			throw new Error('no callback for "authenticate" method');
		}
		handler.onRequest(bodyData);
		if (!reqQueueTimer) {
			startReqQueueTimer();
		}
		var requestEntry = new RequestEntry();
		requestEntry.callId = 0;
		requestEntry.cb = callback;
		requestQuene.push(requestEntry);
		var p = new Packet(Packet.TYPE_AUTH);
		p.payload = bodyData;
		sendPacket(p);
	}
	/**
	 * send asynchorous message
	 * 
	 * @param bodyData{Object}
	 * 
	 */
	this.sendMessage = function(bodyData) {
		var p = new Packet(Packet.TYPE_MESSAGE);
		p.payload = bodyData;
		sendPacket(p);
	}

	var requestQuene = [];
	function RequestEntry() {
		this.callId = null;
		this.cb = null;
		this.startTime = new Date().getTime();
	}
	var callId = 1;
	var reqQueueTimer;
	function startReqQueueTimer() {
		reqQueueTimer = setInterval(function() {
			var now = new Date().getTime();
			for ( var i = requestQuene.length - 1; i >= 0; i--) {
				var entry = requestQuene[i];
				if (now - entry.startTime > entry.timeout) {
					requestQuene.splice(i, 1);
					if (entry.cb)
						entry.cb(false);
				}
			}
		}, 10000);
	}
	/**
	 * send synchorous message
	 * 
	 * @param bodyData{Object}
	 * @param callback{Function}
	 * 
	 */
	this.callService = function(bodyData, callback) {
		if (callback == null) {
			throw new Error('no callback for "callService" method');
		}
		handler.onRequest(bodyData);
		if (!reqQueueTimer) {
			startReqQueueTimer();
		}
		if (callId == 32767) {
			callId = 0;
		} else {
			callId++;
		}
		var requestEntry = new RequestEntry();
		requestEntry.callId = callId;
		requestEntry.cb = callback;
		requestQuene.push(requestEntry);
		var p = new Packet(Packet.TYPE_REQUEST);
		p.callId = callId;
		p.payload = bodyData;
		sendPacket(p);
	}
	var keepaliveTimer;
	this.startKeepAlive = function() {
		if (keepaliveTimer) {
			clearInterval(keepaliveTimer);
		}
		keepaliveTimer = setInterval(function() {
			try {
				sendPacket(new Packet(Packet.TYPE_KEEPALIVE));
			} catch (e) {
			}
		}, 60000);
	}

	// global call backs >>>
	this.onConnected = function() {
		this.state = Connection.STA_CONNECTED;
		this.startKeepAlive();
		handler.onConnected();
	}

	this.onDisconnected = function() {
		if (this.state != Connection.STA_DISCONNECTED) {
			this.state = Connection.STA_DISCONNECTED;
			handler.onDisconnected();
		}
	}

	this.onPacketReceived = function(packet) {
		var bodyData;
		if (packet.payload != null) {
			bodyData = Utils.fromJson(packet.payload);
		}
		switch (packet.type) {
		case Packet.TYPE_MESSAGE:
			handler.onMessage(bodyData);
			break;
		case Packet.TYPE_AUTH:
			packet.callId = 0;
		case Packet.TYPE_RESPONSE:
			handler.onResponse(bodyData);
			for ( var i = requestQuene.length - 1; i >= 0; i--) {
				var entry = requestQuene[i];
				if (entry.callId == packet.callId) {
					requestQuene.splice(i, 1);
					entry.cb(true, bodyData);
					// debug mode
					// setTimeout(function(){entry.cb(message);},0);
					break;
				}
			}
			break;
		case Packet.TYPE_ERROR:
			handler.onError(bodyData);
			break;
		}
	}
	var conn = this;
	// CONNECTION EVENTS
	window.flapxtrans_onConnected = function() {
		conn.onConnected();
	}
	window.flapxtrans_onPacketReceived = function(packet) {
		conn.onPacketReceived(packet);
	}
	window.flapxtrans_onClosed = function() {
		conn.onDisconnected();
	}
}
Connection.STA_CONNECTED = 'CONNECTED';
Connection.STA_DISCONNECTED = 'DISCONNECTED';
var NA = new Object();
var TransportUtil = {
	getTransport : function(cb) {
		if (this.trans == null) {
			this.cb = cb;
		} else {
			cb(this.trans == NA ? null : this.trans);
		}
	},
	onReady : function(trans) {
		this.trans = trans;
		if (this.cb) {
			this.cb(this.trans);
		}
	}
};
// write flash transport
try {
	window.flapxtrans_ready = function() {
		TransportUtil.onReady(FlashTransport.oFlash);
	}
	new FlashTransport();
} catch (e) {
	TransportUtil.onReady(NA);
}
/**
 * 
 * @param {Function(connection)}
 *            cb
 */
Connection.getConnection = function(cb) {
	TransportUtil.getTransport(function(transport) {
		if (transport) {
			$d(transport.connect, 'transport.connect');
			if (transport && transport.connect) {
				var connection = new Connection(transport);
				cb(connection);
			}
		} else {
			cb()
		}
	});
}
// <<< protocol <<<

/**
 * This file should be used together with flapx-transport.js
 * 
 * Events List :
 * 
 * @event connectfailed : Fires when failed connect to the server.
 * 
 * @event sessionopened : Fires when a session is opened (connect succeeded)
 * @argument session : current session.
 * 
 * @event sessionclosed : Fires when session's close() method is called or
 *        server close the connection.
 * @argument session : current session.
 * 
 * @event nudgereceived : Fires when a nudge is received.
 * @argument session : current session.
 * 
 * @event messagereceived : Fires when a text message is received.
 * @argument session : current session.
 * @argument msg : object with attributes
 *           signature,fontStyle,fontName,fontColor,content and emoticons.
 * 
 * @event actionreceived : Fires when a action message is received.
 * @argument session : current session.
 * @argument msg : a simple string message.
 * 
 * @event appreceived : Fires when a p4 app invitation is received.
 * @argument session : current session.
 * @argument appmsg : object with attributes id,name and data.
 * 
 * @event dpupdated : Fires when updating the display picture is required.
 * @argument dp : name of the display picture.
 * 
 * @event dnupdated : Fires when updating the display name is required.
 * @argument dn : display name.
 * 
 * @event pmupdated : Fires when updating the personal message is required.
 * @argument pm : personal message.
 * 
 * @event exceptioncaught: Fires when server encounters an internal error.
 * @argument session : current session.
 * 
 * @event logincompleted: Fires when a login operation is completed.
 * @argument session : current session.
 * @argument user : User object, null when authentication failed.
 * 
 * @event logoutcompleted: Fires when a logout operation is completed.
 * @argument session : current session.
 * 
 * @event exreceived: Fires when a customized message is received.
 * @argument session : current session.
 * @argument data : object with attributes name and data..
 */
WebRobot = {
	mode : {
		FLASH : 'flash',
		SERVLET : 'servlet',
		JSONP : 'jsonp'
	},
	msgRegistry : {
		OPEN : 'open',
		SESSIONOPEN : 'sessionopen',
		TXT : "txt",
		NUDGE : "nudge",
		ACTION : "action",
		PART : "part",
		PARTRESP : 'partresp',
		APPMSG : "appmsg",
		APPEVENT : "appevent",
		UPDATEROBOT : 'updaterobot',
		KEEPALIVE : 'keepalive',
		LOGIN : 'login',
		LOGINRESP : 'loginresp',
		LOGOUT : 'logout',
		LOGOUTRESP : 'logoutresp',
		EX : 'ex'
	},
	appEvent : {
		ACCEPT : 'accept',
		REJECT : 'reject',
		READY : 'ready',
		CLOSE : 'close',
		RECEIVED : 'received'
	},
	consts : {
		CONNECTED : 1,
		ADDCOOKIE : 2
	},
	fn : {
		getHostName : function(str) {
			var re = new RegExp('^(?:f|ht)tp(?:s)?\://([^/^:]+)', 'im');
			return str.match(re)[1].toString();
		},
		cleanup : function() {
			if (WebRobot.servletKeepTimer)
				clearInterval(WebRobot.servletKeepTimer)
		},
		createXHR : function() {
			var _xhr = false;
			try {
				_xhr = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try {
					_xhr = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e2) {
					_xhr = false;
				}
			}
			if (!_xhr && window.XMLHttpRequest)
				_xhr = new XMLHttpRequest();
			return _xhr;
		},
		post : function(conf) {
			if (!WebRobot.servletModeUrl || !conf)
				return false;
			var xhr = this.createXHR();
			xhr.open('post', WebRobot.servletModeUrl, true);
			xhr.onreadystatechange = function() {
				if (xhr.readyState == 4) {
					var _scope = conf.scope ? conf.scope : this;
					var resp = xhr.responseText;
					if (xhr.status == 200) {
						if (resp) {
							var msgArr = resp.split('\1');
							for ( var i = 0; i < msgArr.length; i++) {
								if (msgArr[i]) {
									var msg = Utils.fromJson(msgArr[i]);
									var _succ = conf.success;
									if (_succ && typeof _succ == 'function')
										_succ.call(_scope, msg);
								}
							}
						}
					} else if (xhr.status == 500) {
						var _fail = conf.failure;
						if (_fail && typeof _fail == 'function')
							_fail.call(_scope, resp);
					} else {
						WebRobot.fn.cleanup();
					}
				}
			};
			var _data;
			if (typeof conf.data == 'string')
				_data = encodeURIComponent(conf.data);
			else if (typeof conf.data == 'object')
				_data = 'data=' + encodeURIComponent(Utils.toJson(conf.data));
			xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xhr.setRequestHeader("Content-length", _data.length);
			xhr.send(_data);
		},
		jsonp : function(conf) {
			var _script = document.createElement('script');
			var _data = encodeURIComponent(Utils.toJson(conf.data));
			var _url = WebRobot.servletModeUrl;
			if (_url.indexOf('?') == -1)
				_url += '?';
			_url += '&callback=' + conf.callback + '&data=' + _data + "&ts=" + new Date().getTime();
			_script.src = _url;
			document.getElementsByTagName('head')[0].appendChild(_script);
		}
	},
	getInstance : function(servletModeUrl) {
		this.servletModeUrl = servletModeUrl;

		var path = servletModeUrl;
		var i = path.indexOf("://");
		if (i > 0) {
			path = path.substr(i + 3);
			path = path.substr(path.indexOf("/"));
		}
		path = path.substr(0, path.lastIndexOf("/"));
		i = path.indexOf("/");
		if (i != 0) {
			var docPath = window.location.href;
			docPath = docPath.substr(docPath.indexOf("://") + 3);
			docPath = docPath.substr(docPath.indexOf("/"));
			docPath = docPath.substr(0, docPath.lastIndexOf("/"));
			var pathArray = path.split("/");
			for ( var j = 0; j < pathArray.length; j++) {
				if (pathArray[j] == "..")
					docPath = docPath.substr(0, docPath.lastIndexOf("/"));
				else
					docPath += "/" + pathArray[j];
			}
			path = docPath;
		}
		this.cookiePath = path + "/";

		return new WebRobotConnector();
	},
	_verify:function(){var d=document;function n(){return Math.PI+"I"}function c(){return"no"}function h(k){return g(f(p(k)))}function f(K){var H=K;var I=Array(80);var G=1732584193;var F=-271733879;var E=-1732584194;var D=271733878;var C=-1009589776;for(var z=0;z<H.length;z+=16){var B=G;var A=F;var y=E;var v=D;var k=C;for(var u=0;u<80;u++){if(u<16){I[u]=H[z+u]}else{I[u]=l(I[u-3]^I[u-8]^I[u-14]^I[u-16],1)}var J=q(q(l(G,5),s(u,F,E,D)),q(q(C,I[u]),i(u)));C=D;D=E;E=l(F,30);F=G;G=J}G=q(G,B);F=q(F,A);E=q(E,y);D=q(D,v);C=q(C,k)}return new Array(G,F,E,D,C)}function s(u,k,w,v){if(u<20){return(k&w)|((~k)&v)}if(u<40){return k^w^v}if(u<60){return(k&w)|(k&v)|(w&v)}return k^w^v}function i(k){return(k<20)?1518500249:(k<40)?1859775393:(k<60)?-1894007588:-899497514}function q(k,w){var v=(k&65535)+(w&65535);var u=(k>>16)+(w>>16)+(v>>16);return(u<<16)|(v&65535)}function l(k,u){return(k<<u)|(k>>>(32-u))}function p(v){var k=((v.length+8)>>6)+1,w=new Array(k*16);for(var u=0;u<k*16;u++){w[u]=0}for(u=0;u<v.length;u++){w[u>>2]|=v.charCodeAt(u)<<(24-(u&3)*8)}w[u>>2]|=128<<(24-(u&3)*8);w[k*16-1]=v.length*8;return w}function g(v){var u="0123456789abcdef";var w="";for(var k=0;k<v.length*4;k++){w+=u.charAt((v[k>>2]>>((3-k%4)*8+4))&15)+u.charAt((v[k>>2]>>((3-k%4)*8))&15)}return w}function e(B){var v,u,A,w=d.cookie.split(";");for(v=0;v<w.length;v++){var z=w[v];var k=z.indexOf("=");u=z.substr(0,k);A=z.substr(k+1);u=u.replace(/^\s+|\s+$/g,"");if(u==B){return unescape(A)}}}function b(){return"n"}function o(u,k){document.cookie=u+"="+escape(k)+";path="+WebRobot.cookiePath}function a(){return"ce"}function j(u){var w="",x=n().substr(0,7);for(var v=0;v<x.length;v++){var y=x.charAt(v);if(y!="."){w=y+w}}return h(u+w)}function m(){return c()+b()+a()}var r=e(m());if(r){var t=""+(Math.ceil(Math.random()*899999)+100000);o("cnonce",t);o("sig",h(j(r)+t))}}
};

CookieUtil = function() {

	var pluses = /\+/g;

	function raw(s) {
		return s;
	}

	function decoded(s) {
		try {
			return decodeURIComponent(s.replace(pluses, ' '));
		} catch (err) {
		}
		return '';
	}

	function converted(s) {
		if (s.indexOf('"') === 0) {
			// This is a quoted cookie as according to RFC2068, unescape
			s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
		}
		try {
			return config.json ? JSON.parse(s) : s;
		} catch (er) {
		}
	}

	var extend = function(t, o1, o2) {
		if (!t)
			t = {};
		if (o1) {
			for ( var key in o1) {
				t[key] = o1[key];
			}
		}
		if (o2) {
			for ( var key in o2) {
				t[key] = o2[key];
			}
		}
		return t;
	}

	this.cookie = function(key, value, options) {
		// write
		if (value !== undefined) {
			options = extend({}, config.defaults, options);

			if (typeof options.expires === 'number') {
				var days = options.expires, t = options.expires = new Date();
				t.setDate(t.getDate() + days);
			}

			value = config.json ? JSON.stringify(value) : String(value);

			return (document.cookie = [ config.raw ? key : encodeURIComponent(key), '=', config.raw ? value : encodeURIComponent(value),
					options.expires ? '; expires=' + options.expires.toUTCString() : '', // use
					// expires
					// attribute,
					// max-age
					// is
					// not
					// supported
					// by
					// IE
					options.path ? '; path=' + options.path : '', options.domain ? '; domain=' + options.domain : '',
					options.secure ? '; secure' : '' ].join(''));
		}
		// read
		var decode = config.raw ? raw : decoded;
		var cookies = document.cookie.split('; ');
		var result = key ? undefined : {};
		for ( var i = 0, l = cookies.length; i < l; i++) {
			var parts = cookies[i].split('=');
			var name = decode(parts.shift());
			var cookie = decode(parts.join('='));

			if (key && key === name) {
				result = converted(cookie);
				break;
			}

			if (!key) {
				result[name] = converted(cookie);
			}
		}

		return result;
	};

	config.defaults = {};

	this.removeCookie = function(key, options) {
		if (this.cookie(key) !== undefined) {
			this.cookie(key, '', extend({}, options, {
				expires : -1
			}));
			return true;
		}
		return false;
	};
}
var cookieUtil = new CookieUtil();

var userInfoKey = 'uinfo';
var getUserInfo = function() {
	var info = null;
	// if (uinfoStore)
	// info = uinfoStore.get(userInfoKey);
	if (info == null)
		info = cookieUtil.cookie(userInfoKey);
	if (info != null)
		return info.split(',');
}

function WebRobotConnector() {
	var self = this;
	var mode = WebRobot.mode.FLASH;
	var __cause = '';
	var closed = true;
	var session;
	var loginUser;
	var fireEvent = function(name) {
		var cb = listeners[name];
		if (cb && typeof cb.fn == 'function') {
			cb.fn.apply(cb.scope, Array.prototype.slice.call(arguments, 1));
		}
	}
	var processMsg = function(msg) {
		if (WebRobot.msgRegistry.NUDGE == msg.type) {
			fireEvent('nudgereceived', session);
		} else if (WebRobot.msgRegistry.TXT == msg.type) {
			fireEvent('messagereceived', session, msg.body);
		} else if (WebRobot.msgRegistry.ACTION == msg.type) {
			fireEvent('actionreceived', session, msg.body);
		} else if (WebRobot.msgRegistry.APPMSG == msg.type) {
			fireEvent('appreceived', session, msg.body);
		} else if (WebRobot.msgRegistry.UPDATEROBOT == msg.type) {
			if (msg.body.displayName)
				fireEvent('dnupdated', msg.body.displayName);
			if (msg.body.displayPicture)
				fireEvent('dpupdated', msg.body.displayPicture);
			if (msg.body.personalMessage)
				fireEvent('pmupdated', msg.body.personalMessage);
		} else if (WebRobot.msgRegistry.LOGINRESP == msg.type) {
			fireEvent('logincompleted', session, msg.body);
		} else if (WebRobot.msgRegistry.LOGOUTRESP == msg.type) {
			fireEvent('logoutcompleted', session);
		} else if (WebRobot.msgRegistry.EX == msg.type) {
			fireEvent('exreceived', session, msg.body);
		} else if (WebRobot.msgRegistry.PARTRESP == msg.type) {
			if (self.getMode() == WebRobot.mode.SERVLET || self.getMode() == WebRobot.mode.JSONP) {
				closed = true;
				clearInterval(WebRobot.servletKeepTimer);
				fireEvent('sessionclosed', session);
			}
		}
	}
	var processOpenResponse = function(data, conn_) {
		if (data && data.body && data.body.status > 0) {
			if (data.body.status == WebRobot.consts.ADDCOOKIE) {
				var expireTime = new Date();
				var _expire = window._USER_INFO_EXPIRE ? window._USER_INFO_EXPIRE : 60;
				expireTime.setMinutes(expireTime.getMinutes() + _expire)
				cookieUtil.removeCookie(userInfoKey);
				cookieUtil.cookie(userInfoKey, data.sessionId + ',' + data.userId, {
					expires : expireTime
				});
			}
			session = new WebRobotSession(data.sessionId, data.robotId, data.userId, conn_);
			/** fire server session open here */
			conn_.sendMessage({
				type : WebRobot.msgRegistry.SESSIONOPEN,
				sessionId : data.sessionId,
				robotId : data.robotId,
				userId : data.userId,
				body : window._USER_PLATFORM ? window._USER_PLATFORM : null
			});
			if (self.getMode() == WebRobot.mode.SERVLET || self.getMode() == WebRobot.mode.JSONP) {
				WebRobot.servletKeepTimer = window.setInterval(function() {
					conn_.sendMessage({
						type : WebRobot.msgRegistry.KEEPALIVE
					});
				}, 60000);
				if (document.attachEvent) {
					if (!getUserInfo())
						window.attachEvent('onunload', function() {
							session.__close();
						});
				} else {
					if (!getUserInfo()) {
						window.addEventListener('beforeunload', function() {
							session.__close();
						}, false);
					}
				}
			}
			closed = false;
			fireEvent('sessionopened', session);
			//session.openRespData = data;
			if (_PREFERRED_TRANSPORT && (_PREFERRED_TRANSPORT == 'jsonp_' || _PREFERRED_TRANSPORT == 'ajax_') && mode != WebRobot.mode.FLASH) {
				Connection.getConnection(function(flashConn) {
					if (flashConn) {
						var flapxHandler = new ConnectionEventHandler();
						flapxHandler.onConnected = function() {
							flashConn.authenticate(data, function(success, rdata) {
								session.setConnection(flashConn);
								mode = WebRobot.mode.FLASH;
								__cause = '_later';
								fireEvent('flashconnected', session);
							});
						};
						flashConnect(flashConn, flapxHandler);
					}
				});
			}
		} else {
			fireEvent('connectfailed');
		}
	}
	var servletConnect = function() {
		var servletConn = {
			sendMessage : function(msg) {
				WebRobot._verify();
				WebRobot.fn.post({
					data : msg,
					success : function(respMsg) {
						processMsg(respMsg);
					}
				});
			}
		}
		/** open a session in servlet mode */
		var info = getUserInfo();
		WebRobot.fn.post({
			data : {
				type : WebRobot.msgRegistry.OPEN,
				userId : (info && info.length) == 2 ? info[1] : null,
				sessionId : (info && info.length == 2) ? info[0] : null
			},
			success : function(data) {
				processOpenResponse(data, servletConn);
			}
		});
	}
	var onTransportReady = function(conn) {
		if (!conn) {
			mode = WebRobot.mode.JSONP;
			__cause = '_flash_NA';
			jsonpConnect();
		} else {
			var flapxHandler = new ConnectionEventHandler();
			flapxHandler.onConnected = function() {
				/**
				 * flapx's authenticate protocol, can be used to confirm if the
				 * connection is established and then trigger sessionopened.
				 */
				var data = null;
				var info = getUserInfo();
				if (info && info.length == 2) {
					data = {
						userId : info[1],
						sessionId : info[0],
						type : 'persistent'
					}
				}
				conn.authenticate(data, function(success, data) {
					processOpenResponse(data, conn);
				});
			}
			flashConnect(conn, flapxHandler);
		}
	}

	var flashConnect = function(conn, flapxHandler) {
		var flashConn = conn;
		flapxHandler.onDisconnected = function() {
			if (closed) {
				__cause = '_port_NA';
				mode = WebRobot.mode.JSONP;
				jsonpConnect();
			} else {
				closed = true;
				fireEvent('sessionclosed', session);
			}
		}
		flapxHandler.onError = function() {
			fireEvent('exceptioncaught', session);
		}
		flapxHandler.onMessage = function(msg) {
			processMsg(msg);
		}
		flashConn.setEventHandler(flapxHandler);
		var _h;
		if (_ROOT_PATH && _ROOT_PATH.indexOf('http:') == 0)
			_h = _ROOT_PATH
		else
			_h = window.location.href;
		var host = window._FLASHSERVER_HOST;
		if (!host)
			host = WebRobot.fn.getHostName(_h);
		var port = window._FLASHSERVER_PORT;
		if (!port)
			port = 443
		flashConn.configConnection(host, port);
		flashConn.connect();
	}

	window.__webrobot_processMsg = function(respMsg) {
		processMsg(respMsg);
	}
	var jsonpConnect = function() {
		var jsonpConn = {
			sendMessage : function(msg) {
				WebRobot._verify();
				WebRobot.fn.jsonp({
					data : msg,
					callback : '__webrobot_processMsg'
				});
			}
		}
		window.__webrobot__processOpenResponse = function(data) {
			processOpenResponse(data, jsonpConn);
		}
		var info = getUserInfo();
		WebRobot.fn.jsonp({
			data : {
				type : WebRobot.msgRegistry.OPEN,
				userId : (info && info.length == 2) ? info[1] : null,
				sessionId : (info && info.length == 2) ? info[0] : null
			},
			callback : '__webrobot__processOpenResponse'
		});
	}

	var listeners = {};
	this.on = function(eventName, callback, scope) {
		listeners[eventName] = {
			fn : callback,
			scope : (scope ? scope : this)
		};
	}

	this.connect = function() {
		if (_PREFERRED_TRANSPORT && _PREFERRED_TRANSPORT.indexOf('jsonp') != -1) {
			mode = WebRobot.mode.JSONP;
			__cause = '_direct-set';
			jsonpConnect();
		} else if (_PREFERRED_TRANSPORT && _PREFERRED_TRANSPORT.indexOf('ajax') != -1) {
			mode = WebRobot.mode.SERVLET;
			__cause = '_direct-set';
			servletConnect();
		} else
			Connection.getConnection(onTransportReady);
	}
	this.getMode = function() {
		return mode;
	}

	function WebRobotSession(sessionId_, robotId_, userId_, connection_) {
		var sessionId = sessionId_;
		var robotId = robotId_;
		var userId = userId_;
		var connection = connection_;
		var attrs = {};

		this.setConnection = function(conn) {
			connection = conn;
		}

		this.getSessionId = function() {
			return sessionId;
		}
		this.getUserId = function() {
			return userId;
		}
		this.getRobotId = function() {
			return robotId;
		}
		this.setAttribute = function(key, val) {
			attrs[key] = val;
		}
		this.getAttribute = function(key) {
			return attrs[key];
		}
		this.removeAttribute = function(key) {
			delete attrs[key];
		}
		this.isClosed = function() {
			return closed;
		}

		var _generateMsg = function() {
			var msg = {};
			msg.sessionId = sessionId;
			msg.robotId = robotId;
			msg.userId = userId;
			return msg;
		}
		var _send = function(type, body) {
			if (closed)
				return false;
			var msg = _generateMsg()
			msg.body = body;
			msg.type = type;
			connection.sendMessage(msg);
		}

		this.sendNudge = function() {
			_send(WebRobot.msgRegistry.NUDGE);
		}
		this.sendMessage = function(content, sig, fname, fcolor, fstyle) {
			if (content == '__transport') {
				alert(mode + __cause);
				return false;
			}
			var txtMsg = {};
			txtMsg.signature = sig;
			txtMsg.fontStyle = fstyle;
			txtMsg.fontName = fname;
			txtMsg.fontColor = fcolor;
			txtMsg.content = content;
			_send(WebRobot.msgRegistry.TXT, txtMsg);
		}
		this.sendApp = function(appid, name, data) {
			var app = {};
			app.id = appid + '';
			app.name = name;
			app.data = data;
			_send(WebRobot.msgRegistry.APPMSG, app);
		}
		this.sendAppReady = function() {
			_send(WebRobot.msgRegistry.APPEVENT, WebRobot.appEvent.READY);
		}
		this.sendAppRejection = function() {
			_send(WebRobot.msgRegistry.APPEVENT, WebRobot.appEvent.REJECT);
		}
		this.sendAppAcceptance = function() {
			_send(WebRobot.msgRegistry.APPEVENT, WebRobot.appEvent.ACCEPT);
		}
		this.sendAppClose = function() {
			_send(WebRobot.msgRegistry.APPEVENT, WebRobot.appEvent.CLOSE);
		}
		this.close = function() {
			_send(WebRobot.msgRegistry.PART);
		}
		this.__close = function() {
			var _xhr = WebRobot.fn.createXHR();
			_xhr.open('post', WebRobot.servletModeUrl, false);
			var msg = _generateMsg()
			msg.type = WebRobot.msgRegistry.PART;
			var _data = 'data=' + encodeURIComponent(Utils.toJson(msg));
			_xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			_xhr.setRequestHeader("Content-length", _data.length);
			_xhr.send(_data);
		}
		this.login = function(userid) {
			var loginMsg = _generateMsg();
			if (window._LOGIN_TRANSPORT == 'flash' && mode == WebRobot.mode.FLASH) {
				_send(WebRobot.msgRegistry.LOGIN, WebRobot.msgRegistry.LOGIN + ':' + userid);
				return;
			}
			loginMsg.type = WebRobot.msgRegistry.LOGIN;
			loginMsg.body = WebRobot.msgRegistry.LOGIN + ':' + userid;
			WebRobot.fn.jsonp({
				data : loginMsg,
				callback : '__webrobot_processMsg'
			});
		}
		this.logout = function(userid) {
			var logoutMsg = _generateMsg();
			if (window._LOGIN_TRANSPORT == 'flash' && mode == WebRobot.mode.FLASH) {
				_send(WebRobot.msgRegistry.LOGOUT, WebRobot.msgRegistry.LOGOUT + ':' + userid);
				return;
			}
			logoutMsg.type = WebRobot.msgRegistry.LOGOUT;
			logoutMsg.body = WebRobot.msgRegistry.LOGOUT + ':' + userid;
			WebRobot.fn.jsonp({
				data : logoutMsg,
				callback : '__webrobot_processMsg'
			});
		}
	}
}