package com.eastrobot.robotdev.utils;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;

/**
 * Http协议访问工具类
 * 
 * @author Yale
 * 
 */
public abstract class HttpUtils {

	/**
	 * 获取HttpClient对象(默认编码utf-8)
	 * 
	 * @return HttpClient对象
	 */
	private static HttpClient getHttpClient() {
		return getHttpClient("utf-8");
	}

	/**
	 * 按encoding编码获取HttpClient对象
	 * 
	 * @param encoding
	 *            字符编码
	 * @return HttpClient对象
	 */
	private static HttpClient getHttpClient(String encoding) {
		HttpClient client = new HttpClient();
		client.getParams().setContentCharset(encoding);
		client.getHttpConnectionManager().getParams()
				.setConnectionTimeout(12000);
		client.getParams().setSoTimeout(20000);
		return client;
	}

	/**
	 * 按get方式提交url
	 * 
	 * @param url
	 *            需要访问的URL地址
	 * @return 访问url后返回的内容
	 */
	public static String get(String url) {
		GetMethod method = new GetMethod(url);
		try {
			int status = getHttpClient("utf-8").executeMethod(method);
			if (status == 200) {
				return method.getResponseBodyAsString();
			}
		} catch (Exception e) {
			return e.getMessage();
		} finally {
			method.releaseConnection();
		}
		return null;
	}

	/**
	 * 按post方式提交url
	 * 
	 * @param url
	 *            需要访问的URL地址
	 * @param params
	 *            提交参数
	 * @return 访问url后返回的内容
	 */
	public static String post(String url, Object... params) {
		if (params.length % 2 != 0) {
			throw new IllegalArgumentException(
					"Invalid number of parameters; each name must have a corresponding value!");
		}
		PostMethod method = new PostMethod(url);
		for (int i = 0; i < params.length; i += 2) {
			if (params[i] == null || params[i + 1] == null)
				continue;
			method.addParameter(params[i].toString(), params[i + 1].toString());
		}
		try {
			int status = getHttpClient().executeMethod(method);
			if (status == 200) {
				return method.getResponseBodyAsString();
			}
		} catch (Exception e) {
			return e.getMessage();
		} finally {
			method.releaseConnection();
		}
		return null;
	}

	/**
	 * 按post方式提交url
	 * 
	 * @param url
	 *            需要访问的URL地址
	 * @param account
	 *            账号
	 * @param params
	 *            提交参数
	 * @return 访问url后返回的内容
	 */
	public static String post(String url, String account, Object... params) {
		System.out.println(params.length);
		if (params.length % 2 != 0) {
			throw new IllegalArgumentException(
					"Invalid number of parameters; each name must have a corresponding value!");
		}
		PostMethod method = new PostMethod(url);

		method.addRequestHeader("account", account);

		for (int i = 0; i < params.length; i += 2) {
			if (params[i] == null || params[i + 1] == null)
				continue;
			method.addParameter(params[i].toString(), params[i + 1].toString());
		}
		try {
			int status = getHttpClient().executeMethod(method);
			if (status == 200) {
				return method.getResponseBodyAsString();
			}
		} catch (Exception e) {
			return e.getMessage();
		} finally {
			method.releaseConnection();
		}
		return null;
	}

	public static String send(String addr, String req, String encoding,
			String authName, String sha1password) {
		try {

			URL url = new URL(addr);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setUseCaches(false);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestProperty("Content-Type", "text/xml; charset="
					+ encoding);
			conn.setRequestProperty("ibot-auth-username", authName);
			conn.setRequestProperty("ibot-auth-password", sha1password);
			OutputStream os = conn.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os, encoding);
			osw.write(req);
			osw.flush();
			osw.close();
			InputStream is = conn.getInputStream();
			return DataUtils.getString(is, encoding);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static String post2Manager(String url, Map<String, String> header,
			Object... params) {
		if (params.length % 2 != 0) {
			throw new IllegalArgumentException(
					"Invalid number of parameters; each name must have a corresponding value!");
		}

		PostMethod method = new PostMethod(url);

		Set<String> keySet = header.keySet();
		Iterator<String> it = keySet.iterator();
		while (it.hasNext()) {
			String _key = it.next();
			String _value = header.get(_key);
			method.addRequestHeader(_key, _value);
		}

		for (int i = 0; i < params.length; i += 2) {
			if (params[i] == null || params[i + 1] == null)
				continue;
			method.addParameter(params[i].toString(), params[i + 1].toString());
		}

		try {
			int status = getHttpClient().executeMethod(method);
			if (status == 200) {
				return method.getResponseBodyAsString();
			}
		} catch (Exception e) {
			return e.getMessage();
		} finally {
			method.releaseConnection();
		}
		return null;
	}

	public static String get2Manager(String url, Map<String, String> header) {
		GetMethod method = new GetMethod(url);
		try {
			Set<String> keySet = header.keySet();
			Iterator<String> it = keySet.iterator();
			while (it.hasNext()) {
				String _key = it.next();
				String _value = header.get(_key);
				method.addRequestHeader(_key, _value);
			}

			int status = getHttpClient("utf-8").executeMethod(method);
			if (status == 200) {
				return method.getResponseBodyAsString();
			}
		} catch (Exception e) {
			return e.getMessage();
		} finally {
			method.releaseConnection();
		}
		return null;
	}

}
