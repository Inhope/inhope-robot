<%@page import="com.eastrobot.commonsapi.ibotcluster.ClusterConfigServiceHelper"%>
<%@ page contentType="text/html;charset=utf-8"%>
<%
	if (ClusterConfigServiceHelper.isClusterEnabled()){
		if (request.getAttribute("requestedPath") != null){
			//http://localhost/robot/app/mgrtest1/template/
			String rp = request.getAttribute("requestedPath").toString();
			response.sendRedirect((rp.endsWith("/")?rp:rp+"/")+"template/");
		}else 
			response.setStatus(404);
	}else{
		response.sendRedirect("template/");
	}
%>