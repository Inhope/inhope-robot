<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="Cache-Control" content="no-cache,   must-revalidate" />
	<meta http-equiv="expires" content="0" />
	<style>
	body {
	    background-color: transparent;
	    color: #333333;
	    font-family: Verdana,Arial,'宋体';
	    font-size: 11px;
	    line-height: 18px;
	    margin: 20px 0 0;
	    overflow-y: auto;
	}
	</style>
	<script type="text/javascript" src="../js/jquery.js"></script> 
	<script type="text/javascript" src="js/feedback.js"></script>
</head>
<body>
	<div style="color: #333333;font-size: 12px;margin: 20px;">
		<h3
			style="color: #333; font-size: 15px; border: 0; margin: 0; padding: 0; line-height: 36px;text-align: center;">
			业务导航
		</h3>
		<a href="${url}" target="_blank" style="color:#0176d0; font-size:12px;"><span style="color: red">点击</span>【${name}】打开业务办理页面</a>
	</div>
</body>
</html>