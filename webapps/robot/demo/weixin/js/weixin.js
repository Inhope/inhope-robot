(function(b, c) {
	b.fn.insertTextToInput = function(a) {
		var b = this[0];
		if (document.selection) {
			b.focus(), document.selection.createRange().text = a
		} else {
			if ("number" == typeof b.selectionStart) {
				var c = b.selectionStart, d = b.value;
				b.value = d.substr(0, b.selectionStart) + a + d.substr(b.selectionEnd);
				b.selectionStart = b.selectionEnd = c + a.length
			} else {
				b.value += a
			}
		}
		return this
	};
})(jQuery, this);

var debuggerUserName = new Date().getTime();

$(document).ready(function() {
	var btn = $("#send_btn");

	var chatScorll = $("#chatScorll");

	$("#send_btn").click(function() {
		var sendMsg = $("#textInput").val();
		if ($.trim(sendMsg))
			_sendSms(sendMsg);
	});

	$("#textInput").keyup(function(event) {
		var keyCode = event.which;
		var sendMsg = $("#textInput").val();
		if (keyCode == 13 && $.trim(sendMsg)) {
			_sendSms(sendMsg);
		}
	});

})

var istimeout = true;

var timeoutID;

var _sendSms = function(content) {
	clearTimeout(timeoutID)
	timeoutID = setTimeout(function() {
		istimeout = true;
	}, 300000);
	showSendMsg(content);
	$("#textInput").val('');
	$.ajax({
		url : "weixin-demo.action",
		type : "POST",
		dataType : "text",
		data : {
			"content" : content,
			"debuggerUserName" : debuggerUserName
		},
		success : function(data, textStatus, xhr) {
			istimeout = false;
			setTimeout(function() {
				$("#chat_chatmsglist").append(correctEmoticons(data));
				chatScorll.scrollTop = chatScorll.scrollHeight;
				$("#textInput").focus();
			}, 500);
		},
		error : function() {
			alert("服务器内部错误");
		}
	});
};

var showSendMsg = function(data) {
	var showHtml = '<div class="chatItem me" un="item_1412837404681">';
	if (istimeout) {
		var date = new Date();
		showHtml += '<div class="time">' + '<span class="timeBg left"></span>' + date.pattern("hh:mm") + '<span class="timeBg right"></span>'
				+ '</div>';
	}
	showHtml += '<div class="chatItemContent">' + '<img class="avatar" src="images/head_me.jpg" onerror="" un="avatar_wxid_jfbqg51shny022" '
			+ 'title="用户" click="showProfile" username="wxid_jfbqg51shny022">'
			+ '<div class="cloud cloudText" un="cloud_1622170361" msgid="1622170361">' + '<div class="cloudPannel" style="">'
			+ '<div class="sendStatus"></div>' + '<div class="cloudBody">' + '<div class="cloudContent">' + '<pre style="white-space:pre-wrap">'
			+ correctEmoticons(data) + '</pre>' + '</div>' + '</div><div class="cloudArrow "></div>' + '</div></div></div></div>';
	$("#chat_chatmsglist").append(showHtml);
	chatScorll.scrollTop = chatScorll.scrollHeight;
};

function correctEmoticons(content) {
	var fillEmo = false;
	var result = '';
	var emo = '';
	for ( var i = 0; i < content.length; i++) {
		var c = content.charAt(i);
		if (c == '[') {
			fillEmo = true;
		} else if (c == ']') {
			var src = emo.toString();
			fillEmo = false;
			if (this.gQQFaceMap[emo]) {
				var target = '<img src="images/emoji/' + this.gQQFaceMap[emo] + '.png">';
				result += target;
			} else {
				result += '[' + emo + ']';
			}
			emo = '';
			emo.length = 0;
		} else if (fillEmo) {
			emo += c;
		} else {
			result += c;
		}
	}
	return result;
}

Date.prototype.pattern = function(fmt) {
	var o = {
		"M+" : this.getMonth() + 1, // 月份
		"d+" : this.getDate(), // 日
		"h+" : this.getHours() == 0 ? 12 : this.getHours(), // 小时
		"H+" : this.getHours(), // 小时
		"m+" : this.getMinutes(), // 分
		"s+" : this.getSeconds(), // 秒
		"q+" : Math.floor((this.getMonth() + 3) / 3), // 季度
		"S" : this.getMilliseconds()
	// 毫秒
	};
	var week = {
		"0" : "\u65e5",
		"1" : "\u4e00",
		"2" : "\u4e8c",
		"3" : "\u4e09",
		"4" : "\u56db",
		"5" : "\u4e94",
		"6" : "\u516d"
	};
	if (/(y+)/.test(fmt)) {
		fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	}
	if (/(E+)/.test(fmt)) {
		fmt = fmt.replace(RegExp.$1, ((RegExp.$1.length > 1) ? (RegExp.$1.length > 2 ? "\u661f\u671f" : "\u5468") : "") + week[this.getDay() + ""]);
	}
	for ( var k in o) {
		if (new RegExp("(" + k + ")").test(fmt)) {
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
		}
	}
	return fmt;
}

$(function() {
	var q = window.location.search;
	if (q && q.indexOf("?q=") == 0) {
		q = q.substring(3);
		if (q) {
			q = decodeURIComponent(q);
			$('#textInput').val(q);
		}
	}
});