<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="pragma" content="no-cache"/>         
	<meta http-equiv="Cache-Control" content="no-cache,   must-revalidate"/>         
	<meta http-equiv="expires" content="0"/>  
		<style>
	body {
	    background-color: transparent;
	    color: #333333;
	    font-family: Verdana,Arial,'宋体';
	    font-size: 11px;
	    line-height: 18px;
	    margin: 20px 0 0;
	    overflow-y: auto;
	}
	</style>
	<link rel="stylesheet" type="text/css" href="../js/jquery-ui/jquery-ui.css" />
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script type="text/javascript" src="../js/jquery-ui/jquery-ui.js"></script>
	<script type="text/javascript" src="js/leaveword.js"></script>
</head>
<body>
<div style="color: #333333;font-size: 12px;margin: 20px;">
	<h3 style="color: #333; font-size: 15px; border: 0; margin: 0; padding: 0; line-height: 36px;text-align: center;">在线留言</h3>
	<div style="MARGIN: 0px auto; width: 290px; line-height: 18px; TEXT-ALIGN: left">
	<!-- <form method="post" id="leavewordForm"> -->
	<span style="color: #344d63;">联系方式：</span>
	<select name="type">
		<option style="color: #344d63; font-size: 12px;" selected="" value="1">Email</option>
		<option style="color: #344d63;" value="2">手机</option>
	</select>
	<div style="color: #344d63; margin:5px 0;" id="r1"><span id='labelAddr'>您的Email</span>: <input style="border:1px solid #8fb7da" name="addr" size="35"></div>
	<div style="color: #344d63; margin:5px 0;" id="r1"><span id='labelAddr'>您的称呼</span>: <input style="border:1px solid #8fb7da" name="username" size="35"></div>
	<div style="color: #344d63;">
		留言内容：<br>
	<textarea class="formBorder" style="border:1px solid #8fb7da;overflow: auto;" cols="35" rows="5" name="content"></textarea></div>
	<center><img id="submitButton" style="cursor:pointer" src="images/btn-submit.gif"></center>
	<!-- </form> -->
	</div>
</div>
<div style="display: none;" id="dialog" title="留言提交成功">
	<p>感谢您的留言，我们会尽快与您联系！</p>
</div>
</body>
</html>