package com.eastrobot.robotdev.utils;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ManagerService {

	public static final String CHARSET = "utf-8";

	private final static String debugkey = "abcaaaaaaaaaaaaaaaaxgxga";

	private static Log log = LogFactory.getLog(ManagerService.class);

	private static byte[] getKey() throws Exception {
		byte[] ret = null;
		ret = debugkey.getBytes(CHARSET);
		return ret;
	}

	static String transformation = "DESede/ECB/PKCS5Padding";
	static String algorithm = "DESede";

	private static String encode(String src) throws Exception {
		return encode0(src, getKey());
	}

	private static String encode0(String src, byte[] key) throws Exception {
		SecretKey secretKey = new SecretKeySpec(key, algorithm);
		Cipher cipher = Cipher.getInstance(transformation);
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		byte[] b = cipher.doFinal(src.getBytes(CHARSET));
		return new String(Hex.encodeHex(b));
	}

	public String decode(String dest) throws Exception {
		return decode0(dest, getKey());
	}

	public String decode0(String dest, byte[] key) throws Exception {
		SecretKey secretKey = new SecretKeySpec(key, algorithm);
		Cipher cipher = Cipher.getInstance(transformation);
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		byte[] b = cipher.doFinal(Hex.decodeHex(dest.toCharArray()));
		return new String(b, CHARSET);
	}

	// for 报表
	public static String getManagerUrl(String username, String userrole,
			String targetpath, String managerPath) {
		String path = "";
		try {
			String raw = "timestamp="
					+ new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())
					+ "&loginid="
					+ username
					+ "&bankcode=012&userprivilege=0&deptcode=est&divcode=t&username="
					+ username + "&userroles=" + userrole + "";
			String token = encode(raw);
			String digest = DigestUtils.md5Hex(token);
			HttpClient client = new HttpClient();
			PostMethod m = new PostMethod(managerPath
					+ "authmgr/auth!login.action");
			m.addRequestHeader("X-Requested-With", "XMLHTTPRequest");
			m.addParameter("ssotoken", token);
			m.addParameter("ssochecksum", digest);
			client.executeMethod(m);
			log.debug("登录信息验证" + "statusCode=" + m.getStatusCode()
					+ ",responseText=" + m.getResponseBodyAsString());
			m.releaseConnection();
			String success_url = managerPath + "unimgr/?initModule="
					+ targetpath + "&";
			success_url = URLEncoder.encode(success_url, "UTF-8");
			path = String
					.format(
							managerPath
									+ "authmgr/auth!login.action?ssotoken=%s&ssochecksum=%s&success_url=%s",
							token, digest, success_url);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return path;
	}

	// for 单点
	public static String getManagerUrl(String username, String userrole, String managerPath) {
		String path = "";
		try {
			String raw = "timestamp="
					+ new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())
					//+ "&username="
					+ "&loginid=52002&bankcode=012&userprivilege=0&deptcode=est&divcode=t&username="
					+ username + "&userroles=" + userrole + "";
			String token = encode(raw);
			String digest = DigestUtils.md5Hex(token);
			HttpClient client = new HttpClient();
			PostMethod m = new PostMethod(managerPath
					+ "authmgr/auth!login.action");
			m.addRequestHeader("X-Requested-With", "XMLHTTPRequest");
			m.addParameter("ssotoken", token);
			m.addParameter("ssochecksum", digest);
			client.executeMethod(m);
			log.debug("登录信息验证" + "statusCode=" + m.getStatusCode()
					+ ",responseText=" + m.getResponseBodyAsString());
			m.releaseConnection();
			path = String.format(managerPath
					+ "authmgr/auth!login.action?ssotoken=%s&ssochecksum=%s",
					token, digest);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return path;
	}

	public static void main1(String[] args) throws Exception {
		ManagerService destool = new ManagerService();
		// String raw =
		// "timestamp=20140306121311&loginid=52002&bankcode=012&deptcode=123&username=Lee
		// Siu Ming&userroles=X0002";
		String raw = "timestamp="
				+ new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())
				+ "&loginid=52002&bankcode=012&userprivilege=0&deptcode=est&divcode=t&username=iiioooo&userroles=管理员";
		String token = destool.encode(raw);
		String digest = DigestUtils.md5Hex(token);
		System.out.println("token:" + token);
		System.out.println("digest:" + digest);
		System.out.println("=====Result=====");
		HttpClient client = new HttpClient();
		PostMethod m = new PostMethod(
				"http://pbsm.demo.xiaoi.com/manager/authmgr/auth!login.action");
		m.addRequestHeader("X-Requested-With", "XMLHTTPRequest");
		m.addParameter("ssotoken", token);
		m.addParameter("ssochecksum", digest);
		client.executeMethod(m);
		m.releaseConnection();
		String path = String.format("http://172.16.1.196:8280/manager/"
				+ "authmgr/auth!login.action?ssotoken=%s&ssochecksum=%s",
				token, digest);
		System.out.println(path);
	}
	public static void main(String[] args) throws Exception {
		String url=getManagerUrl("paul","管理员","http://172.16.1.196:8280/manager/");
		System.out.println(url);
		
	}
	
}