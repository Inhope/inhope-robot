package com.eastrobot.robotdev.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import com.eastrobot.robotdev.Constants;

/**
 * 常用数据工具类
 * 
 * @author Yale
 * 
 */
public class DataUtils {

	/**
	 * 根据输入流得到字符串
	 * 
	 * @param is
	 *            输入流
	 * @return
	 */
	public static String getString(InputStream is) {
		return getString(is, Constants.ENCODING);
	}

	/**
	 * 根据输入流得到字符串
	 * 
	 * @param is
	 *            输入流
	 * @param encoding
	 *            字符集
	 * @return
	 */
	public static String getString(InputStream is, String encoding) {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(is, encoding));
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		String line = null;
		StringBuilder sb = new StringBuilder();
		try {
			while ((line = br.readLine()) != null) {
				sb.append(line + "\r\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
		return sb.toString();
	}

}
