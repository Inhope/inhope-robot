package com.eastrobot.robotdev.util;

import net.sf.json.JSONObject;

import com.eastrobot.robotface.domain.RobotResponseEx;

public class XiaoiTagUtils {

	public static String tagFilter(String answer) {
		if (answer == null)
			return null;
		// 替换link url
		answer = answer.replaceAll("\\[link url=(.*?)](.*?)\\[/link]",
				"<a target=\"_blank\" href=$1>$2</a>");
		// 替换link submit
		answer = answer.replaceAll("\\[link submit=(.*?)](.*?)\\[/link]",
				"<a onclick=\"send($1)\" href=\"javascript:void(0);\">$2</a>");
		// 替换link submit简写
		answer = answer
				.replaceAll("\\[link](.*?)\\[/link]",
						"<a onclick=\"send(\"$1\")\" href=\"javascript:void(0);\">$1</a>");
		return answer;
	}

	public static void main(String[] args) {
		RobotResponseEx rrE = new RobotResponseEx();
		String str = "[link submit=\"faqvote:4d8a761555eb45ed804744af0cb5db34 2 venJ3MG9s8fSu7zS venJ3MG9s8fSu7zS IsG9s8fSu7zSobHKx86qwvrX47/Nu6fC/tPOzajQxdDox/O2+Mzhuam1xLaotePC/tPOzai7sNPFu923/s7xoaPM17LNt9Ez1Kov1MIvyqGjrLDswO24w9K1zvG686Osyta7+sL+087Wwb/Nu6ew7MDttcS2qLXjyqG33aGi1rHPvcrQu/LX1NbOx/jKsaOsvdPM/cPit9GjrNaxvdOyprTywv7TztPFu93KobfdtbG12LXnu7C6zdaxsqa6xcLruenK9LXYKNbYx+wps6TNvrXnu7C/ydLUz+3K3DAuMjnUqi+31tbTtcTTxbvdvNu48aGj\"]未解决[/link]";
		str = str.replaceAll("\"", "'");
		str = tagFilter(str);
		rrE.setContent(str);
		JSONObject obj =  JSONObject.fromObject(rrE);
		System.out.println(obj);
	}
}


