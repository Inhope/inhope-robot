<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
	.title {
		color: #38608E;
		font: 13px 宋体;
	}
	.question_list {
		margin:5px 20px;
		padding:0;
	}
	.question {
		color: #38608E;
		font: 13px 宋体;
		cursor : pointer;
		height:auto;
		line-height:20px;
	}
</style>
<script type="text/javascript" src="../js/jquery.js"></script> 
<script type="text/javascript">
$(function(){
	$(".question").click(function(){
		if(window.parent.Channel_SendIM) {
			window.parent.Channel_SendIM(this.innerHTML)
		}
		else if(window.top.Channel_SendIM) {
			window.top.Channel_SendIM(this.innerHTML)
		}
	});
	$(".question").mouseover(function(){
		this.style.backgroundColor = "#CBDBEC"
	});
	$(".question").mouseout(function(){
		this.style.backgroundColor = "transparent";
	});
});
</script>
<title>热点问题</title> 
</head>
<body>
	<div class="title">热点问题：</div>
	<ul class="question_list">
	<c:forEach var="question" varStatus="status" items="${hotquestions}">
		<li class="question">${question.question}</li>
	</c:forEach>
	</ul>
</body>
</html>
