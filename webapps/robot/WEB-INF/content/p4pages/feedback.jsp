<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="Cache-Control" content="no-cache,   must-revalidate" />
	<meta http-equiv="expires" content="0" />
	<style>
	body {
	    background-color: transparent;
	    color: #333333;
	    font-family: Verdana,Arial,'宋体';
	    font-size: 11px;
	    line-height: 18px;
	    margin: 20px 0 0;
	    overflow-y: auto;
	}
	</style>
	<link rel="stylesheet" type="text/css" href="../js/jquery-ui/jquery-ui.css" />
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script type="text/javascript" src="../js/jquery-ui/jquery-ui.js"></script>
	<script type="text/javascript" src="js/feedback.js"></script>
	<script type="text/javascript" src="../js/verify.js"></script>
</head>
<body>
	<div style="color: #333333;font-size: 12px;margin: 20px;">
		<h3
			style="color: #333; font-size: 15px; border: 0; margin: 0; padding: 0; line-height: 36px;text-align: center;">
			意见反馈
		</h3>
		<!-- <form style="border: 0px solid #000; padding: 0; margin: 0"
			method="post" id="feedbackForm"> -->
			<p style="margin: 0">
				您好，为了今后更好的为您服务，请您给我的本次服务一个评价：
			</p>
			<p style="margin-bottom: 10px;">
				您对本次服务的评价是：
			</p>
			<p style="margin-top: 0;">
				<input type="radio" value="1" name="type">
					非常满意 
				<input type="radio" value="2" name="type">
					满意 
				<input type="radio" value="3" name="type">
					一般 
				<input type="radio" value="4" name="type">
					不满意 
				<input type="radio" value="5" name="type">
					非常不满意 
			</p>
			<p style="display: none" id="reasonDiv">
				<b>请您选择您觉得不满意的原因：</b>
				<br>
				<input type="radio" value="系统慢" name="reason">
					系统慢 
				<input type="radio" value="无回答" name="reason">
					无回答 
				<input type="radio" value="回答不知道" name="reason">
					回答不知道 
				<input type="radio" value="回答不全" name="reason">
					回答不全 
				<br>
				<input type="radio" value="回答错误" name="reason">
					回答错误 
				<input type="radio" value="其它" name="reason">
					其它 
			</p>

			<p>
				请您提出宝贵建议，我会努力学习，今后为您更好的服务:
				<br>
				<textarea style="border: 1px solid #8fb7da; width: 95%"
					name="content" cols="45" rows="4"></textarea>
			</p>
			<center>
				<img style="cursor: pointer" src="images/btn-submit.gif"
					id="submitButton">
			</center>
		<!-- </form> -->
	</div>
	<div style="display: none;" id="dialog" title="反馈提交成功">
		<p>感谢您的宝贵建议，我会努力改进！</p>
	</div>
</body>
</html>