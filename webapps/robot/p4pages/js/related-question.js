$(function(){
	$(".question").click(function(){
		var faqDiv = this.parentNode;
		$(".faq").each(function(){
			if(this==faqDiv)return;
			this.style.border = "none";
			$(".question",this).css("background-color","transparent");
			$(".answer",this).hide();
			$(".question_leftimg",this).css("background-image","url(images/arrow_l_right.gif)");
			$(".question_rightimg",this).css("background-image","url(images/arrow_r_down.gif)");
		});
		if($(".answer",faqDiv).css("display")=="none") {
			faqDiv.style.border = "1px solid #CBDBEC";
			$(".question",faqDiv).css("background-color","#CBDBEC");
			$(".answer",faqDiv).slideDown();
			$(".question_leftimg",this).css("background-image","url(images/arrow_l_down.gif)");
			$(".question_rightimg",this).css("background-image","url(images/arrow_r_up.gif)");
		}
		else {
			faqDiv.style.border = "none";
			$(".question",faqDiv).css("background-color","transparent");
			$(".answer",faqDiv).hide();
			$(".question_leftimg",faqDiv).css("background-image","url(images/arrow_l_right.gif)");
			$(".question_rightimg",faqDiv).css("background-image","url(images/arrow_r_down.gif)");
		}
		
	});
	$(".question").mouseover(function(){
		if($(".answer",this.parentNode).css("display")=="none") 
			this.style.backgroundColor = "#CBDBEC"
	});
	$(".question").mouseout(function(){
		if($(".answer",this.parentNode).css("display")=="none") 
			this.style.backgroundColor = "transparent";
	});
});