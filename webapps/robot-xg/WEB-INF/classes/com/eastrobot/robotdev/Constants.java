package com.eastrobot.robotdev;

public interface Constants {

	public static final String ENCODING = "utf-8";
	public static final int RES_SUCCESS = 0;
	
	static interface MsgResource {
		public static final String WELCOME_WORDS = "dev.welcome.words";
		public static final String FEEDBACK_SUCCESS = "dev.feedback.success.words";
		public static final String FEEDBACK_FAILURE = "dev.feedback.failure.words";
		public static final String LEAVEWORD_SUCCESS = "dev.leaveword.success.words";
		public static final String LEAVEWORD_FAILURE = "dev.leaveword.failure.words";
		public static final String FAQVOTE_SUCCESS_YES = "dev.faqvote.success.words.yes";
		public static final String FAQVOTE_SUCCESS_NO = "dev.faqvote.success.words.no";
		public static final String FAQVOTE_FAILURE = "dev.faqvote.failure.words";
	}

}
