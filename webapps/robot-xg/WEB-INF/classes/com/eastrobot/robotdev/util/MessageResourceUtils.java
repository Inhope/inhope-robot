package com.eastrobot.robotdev.util;

import org.apache.commons.lang.StringUtils;

import com.eastrobot.robotface.RobotMessageResource;
import com.eastrobot.robotface.RobotMessageResourceProxy;

/**
 * 服务参数获取
 * 
 * @author yale
 * 
 */
public class MessageResourceUtils {

	private static RobotMessageResource robotMessageResource = new RobotMessageResourceProxy();

	public static String get(String key, String platform) {
		String value = robotMessageResource.get(key, platform);
		if (StringUtils.isBlank(value))
			value = key;
		return value;
	}

	public static String get(String key, String platform, String defaultValue) {
		String value = robotMessageResource.get(key, platform);
		if (StringUtils.isBlank(value)) {
			value = defaultValue;
		}
		return value;
	}

}
