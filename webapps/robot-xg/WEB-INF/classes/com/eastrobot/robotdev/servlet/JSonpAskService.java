package com.eastrobot.robotdev.servlet;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eastrobot.robotface.RobotServiceEx;
import com.eastrobot.robotface.RobotServiceExProxy;
import com.eastrobot.robotface.domain.RobotRequestEx;
import com.eastrobot.robotface.domain.RobotResponseEx;
import com.eastrobot.robotface.domain.UserAttribute;

/**
 * 提供给客户JSONP接口
 * 
 * @author yale
 * 
 */
public class JSonpAskService extends BaseServlet {

	private static final long serialVersionUID = -6433901548830594473L;
	private static Logger log = LoggerFactory.getLogger(JSonpAskService.class);

	private RobotServiceEx robotServiceEx = new RobotServiceExProxy();

	@Override
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String jsonp = request.getParameter("jsonpcallback");
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("success", false);
		String result = jsonp + "(#content#)";
		try {

			// 设置请求参数
			RobotRequestEx robotRequestEx = new RobotRequestEx();

			// 必填参数
			String userId = request.getParameter("userId");
			String question = request.getParameter("question");
			String platform = request.getParameter("platform");

			log.debug("userId:" + userId);
			log.debug("question:" + question);
			log.debug("platform:" + platform);

			if (StringUtils.isBlank(userId) || StringUtils.isBlank(question)
					|| StringUtils.isBlank(platform)) {
				throw new Exception("缺少必填参数");
			}

			question = URLDecoder.decode(question);
			log.debug("decode question:" + question);
			
			String location = request.getParameter("location");
			String brand = request.getParameter("brand");

			robotRequestEx.setUserId(userId);
			robotRequestEx.setQuestion(question);

			// 设置UserAttribute数组
			List<UserAttribute> attrs = new ArrayList<UserAttribute>();
			attrs.add(new UserAttribute(UserAttribute.PLATFORM, platform));
			if (StringUtils.isNotBlank(location))
				attrs.add(new UserAttribute(UserAttribute.LOCATION, location));
			if (StringUtils.isNotBlank(brand))
				attrs.add(new UserAttribute(UserAttribute.BRAND, brand));

			robotRequestEx.setAttributes(attrs.toArray(new UserAttribute[] {}));

			RobotResponseEx robotResponseEx = robotServiceEx
					.deliver(robotRequestEx);

			jsonObject.put("responseEx", robotResponseEx);
			jsonObject.put("success", true);
		} catch (Exception e) {
			jsonObject.put("errorMsg", e.getMessage());
			log.error("", e);
		} finally {
			result = result.replaceAll("#content#", jsonObject.toString());
			write(response, result);
		}

	}
}
